function b = RHS_eliminate(dofs,grid,surface_nodes,p)
b = ones(dofs,1);
n = grid.X*grid.Y*grid.Z;
for mu_ind=1:p.Number_of_MUs
    ind_offset = (mu_ind-1)*n ;
    % Load INtracellular Conductivity 
    if (size(p.sigma_i,3)==1)
        sigma_i = p.sigma_i;
    else
           sigma_i = p.sigma_i(:,:,mu_ind); 
    end
    % Apply BCs
    if (sigma_i(2,2) ~= 0)
        % Front-Side
        for i=1:length(surface_nodes.front)
            ind = surface_nodes.front(i) + ind_offset;
            b(ind,:) = 0;
        end
        % Back-Side
        for i=1:length(surface_nodes.back)
            ind = surface_nodes.back(i) + ind_offset;
            b(ind,:) = 0;
        end
    end
    % Left-Side
    for i=1:length(surface_nodes.left)
        ind = surface_nodes.left(i) + ind_offset;
        b(ind,:) = 0;
    end
    % Right-Side
    for i=1:length(surface_nodes.right)
        ind = surface_nodes.right(i) + ind_offset;
        b(ind,:) = 0;
    end
    if (sigma_i(3,3) ~= 0)
        % Bottom Side
        for i=1:length(surface_nodes.bottom)
            ind = surface_nodes.bottom(i) + ind_offset;
            b(ind,:) = 0;
        end
        % Top Side
        for i=1:length(surface_nodes.top)
            ind = surface_nodes.top(i) + ind_offset;
            b(ind,:) = 0;
        end  
    end
end
%
end