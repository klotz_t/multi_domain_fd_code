function [ M, A, B, C, D] = build_mixed_fine_corse_mesh_matrices(dt,grid,p,c_factor)%,include_fat,include_skin)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
if (length(c_factor) == 1)
    c_factor = c_factor*[1,1,1];
else
    c_factor = c_factor;
end
%% Get Variables
n_of_mus = p.Number_of_MUs;
% define fine grid parameters
X_f = grid.X;
Y_f = grid.Y;
Z_f = grid.Z;
dx_f = grid.dx;
dy_f = grid.dy;
dz_f = grid.dz;
% 
dofs_f = X_f*Y_f*Z_f;
nodes_f = 1:dofs_f;
nodes_3d_f = reshape(nodes_f,X_f,Y_f,Z_f);
% define corse grid parameters 
X_c = round(grid.X/c_factor(1));
Y_c = round(grid.Y/c_factor(2));
Z_c = round(grid.Z/c_factor(3));
dx_c = grid.dx*c_factor(1);
dy_c = grid.dy*c_factor(2);
dz_c = grid.dz*c_factor(3);
%
dofs_c = X_c*Y_c*Z_c;
nodes_c = 1:dofs_c;
nodes_3d_c = reshape(nodes_c,X_c,Y_c,Z_c);



%% Get Surface and Innner Muscle Nodes of the fine mesh
fine_inner = reshape(nodes_3d_f(2:X_f-1,2:Y_f-1,2:Z_f-1),[],1)';
fine_left = reshape(nodes_3d_f(1:1,1:Y_f,2:Z_f-1),[],1)';%(X*Y+1):X:(dofs-X*Y);
fine_right = reshape(nodes_3d_f(X_f:X_f,1:Y_f,2:Z_f-1),[],1)';%(X*Y+X):X:(dofs-X*Y);
fine_front = reshape(nodes_3d_f(2:X_f-1,1:1,2:Z_f-1),[],1)';
fine_back = reshape(nodes_3d_f(2:X_f-1,Y_f:Y_f,2:Z_f-1),[],1)';
fine_bottom = 1:1:(X_f*Y_f);
fine_top_interface = reshape(nodes_3d_f(1:X_f,1:Y_f,Z_f:Z_f),[],1)';%(dofs-X*Y+1):1:dofs;

num_of_fine_bottom_nodes = length(fine_bottom);
num_of_fine_top_nodes = length(fine_top_interface);
num_of_fine_left_nodes = length(fine_left);
num_of_fine_right_nodes = length(fine_right);
num_of_fine_back_nodes = length(fine_back);
num_of_fine_front_nodes = length(fine_front);
num_of_fine_inner_nodes = length(fine_inner);

%% Get Surface and Innner Muscle Nodes of the corse mesh
corse_inner = reshape(nodes_3d_c(2:X_c-1,2:Y_c-1,2:Z_c-1),[],1)';
corse_left = reshape(nodes_3d_c(1:1,1:Y_c,2:Z_c-1),[],1)';%(X*Y+1):X:(dofs-X*Y);
corse_right = reshape(nodes_3d_c(X_c:X_c,1:Y_c,2:Z_c-1),[],1)';%(X*Y+X):X:(dofs-X*Y);
corse_front = reshape(nodes_3d_c(2:X_c-1,1:1,2:Z_c-1),[],1)';
corse_back = reshape(nodes_3d_c(2:X_c-1,Y_c:Y_c,2:Z_c-1),[],1)';
corse_bottom = 1:1:(X_c*Y_c);
corse_top_interface = reshape(nodes_3d_c(1:X_c,1:Y_c,Z_c:Z_c),[],1)';%(dofs-X*Y+1):1:dofs;

num_of_corse_bottom_nodes = length(corse_bottom);
num_of_corse_top_nodes = length(corse_top_interface);
num_of_corse_left_nodes = length(corse_left);
num_of_corse_right_nodes = length(corse_right);
num_of_corse_back_nodes = length(corse_back);
num_of_corse_front_nodes = length(corse_front);
num_of_corse_inner_nodes = length(corse_inner);

%% Setting Up Matrix A
for mu_ind=1:n_of_mus
    i = zeros(1,dofs_f*11);
    j = zeros(1,dofs_f*11);
    v = zeros(1,dofs_f*11);

    ind_count = 1;

    fact_xx = (dt/(p.A_m(mu_ind)*p.C_m(mu_ind))*p.sigma_i(1,1)/dx_f^2);
    fact_yy = (dt/(p.A_m(mu_ind)*p.C_m(mu_ind))*p.sigma_i(2,2)/dy_f^2); 
    fact_zz = (dt/(p.A_m(mu_ind)*p.C_m(mu_ind))*p.sigma_i(3,3)/dz_f^2);
    fact_xy = (dt/(p.A_m(mu_ind)*p.C_m(mu_ind))*(p.sigma_i(1,2)+p.sigma_i(2,1))/(4*dx_f*dy_f));
    w0 = (1+2*fact_xx+2*fact_yy+2*fact_zz);
    % diag 
    i(ind_count:ind_count+num_of_fine_inner_nodes-1) = fine_inner;
    j(ind_count:ind_count+num_of_fine_inner_nodes-1) = fine_inner;
    v(ind_count:ind_count+num_of_fine_inner_nodes-1) = w0;
    ind_count = ind_count+num_of_fine_inner_nodes;
    % u_xx
    i(ind_count:ind_count+num_of_fine_inner_nodes-1) = fine_inner;
    j(ind_count:ind_count+num_of_fine_inner_nodes-1) = fine_inner + 1;
    v(ind_count:ind_count+num_of_fine_inner_nodes-1) = -fact_xx;
    ind_count = ind_count+num_of_fine_inner_nodes;
    i(ind_count:ind_count+num_of_fine_inner_nodes-1) = fine_inner;
    j(ind_count:ind_count+num_of_fine_inner_nodes-1) = fine_inner - 1;
    v(ind_count:ind_count+num_of_fine_inner_nodes-1) = -fact_xx;
    ind_count = ind_count+num_of_fine_inner_nodes;
    % u_yy
    i(ind_count:ind_count+num_of_fine_inner_nodes-1) = fine_inner;
    j(ind_count:ind_count+num_of_fine_inner_nodes-1) = fine_inner + X_f;
    v(ind_count:ind_count+num_of_fine_inner_nodes-1) = -fact_yy;
    ind_count = ind_count+num_of_fine_inner_nodes;
    i(ind_count:ind_count+num_of_fine_inner_nodes-1) = fine_inner;
    j(ind_count:ind_count+num_of_fine_inner_nodes-1) = fine_inner - X_f;
    v(ind_count:ind_count+num_of_fine_inner_nodes-1) = -fact_yy;
    ind_count = ind_count+num_of_fine_inner_nodes;
    % u_zz
    i(ind_count:ind_count+num_of_fine_inner_nodes-1) = fine_inner;
    j(ind_count:ind_count+num_of_fine_inner_nodes-1) = fine_inner + X_f*Y_f;
    v(ind_count:ind_count+num_of_fine_inner_nodes-1) = -fact_zz;
    ind_count = ind_count+num_of_fine_inner_nodes;
    i(ind_count:ind_count+num_of_fine_inner_nodes-1) = fine_inner;
    j(ind_count:ind_count+num_of_fine_inner_nodes-1) = fine_inner - X_f*Y_f;
    v(ind_count:ind_count+num_of_fine_inner_nodes-1) = -fact_zz;
    ind_count = ind_count+num_of_fine_inner_nodes;
    % u_xy
    i(ind_count:ind_count+num_of_fine_inner_nodes-1) = fine_inner;
    j(ind_count:ind_count+num_of_fine_inner_nodes-1) = fine_inner + (X_f+1);
    v(ind_count:ind_count+num_of_fine_inner_nodes-1) = -fact_xy;
    ind_count = ind_count+num_of_fine_inner_nodes;
    i(ind_count:ind_count+num_of_fine_inner_nodes-1) = fine_inner;
    j(ind_count:ind_count+num_of_fine_inner_nodes-1) = fine_inner - (X_f+1);
    v(ind_count:ind_count+num_of_fine_inner_nodes-1) = -fact_xy;
    ind_count = ind_count+num_of_fine_inner_nodes;
    i(ind_count:ind_count+num_of_fine_inner_nodes-1) = fine_inner;
    j(ind_count:ind_count+num_of_fine_inner_nodes-1) = fine_inner + (X_f-1);
    v(ind_count:ind_count+num_of_fine_inner_nodes-1) = fact_xy;
    ind_count = ind_count+num_of_fine_inner_nodes;
    i(ind_count:ind_count+num_of_fine_inner_nodes-1) = fine_inner;
    j(ind_count:ind_count+num_of_fine_inner_nodes-1) = fine_inner - (X_f-1);
    v(ind_count:ind_count+num_of_fine_inner_nodes-1) = fact_xy;
    ind_count = ind_count+num_of_fine_inner_nodes;
    % BCs left
    i(ind_count:ind_count+num_of_fine_left_nodes-1) = fine_left;
    j(ind_count:ind_count+num_of_fine_left_nodes-1) = fine_left;
    v(ind_count:ind_count+num_of_fine_left_nodes-1) = 3/2;%1;
    ind_count = ind_count+num_of_fine_left_nodes;
    i(ind_count:ind_count+num_of_fine_left_nodes-1) = fine_left;
    j(ind_count:ind_count+num_of_fine_left_nodes-1) = fine_left + 1*c_factor(1);
    v(ind_count:ind_count+num_of_fine_left_nodes-1) = -2;%-1;
    ind_count = ind_count+num_of_fine_left_nodes;
    i(ind_count:ind_count+num_of_fine_left_nodes-1) = fine_left;
    j(ind_count:ind_count+num_of_fine_left_nodes-1) = fine_left + 2*c_factor(1);
    v(ind_count:ind_count+num_of_fine_left_nodes-1) = 1/2;
    ind_count = ind_count+num_of_fine_left_nodes;
    % BCs right
    i(ind_count:ind_count+num_of_fine_right_nodes-1) = fine_right;
    j(ind_count:ind_count+num_of_fine_right_nodes-1) = fine_right;
    v(ind_count:ind_count+num_of_fine_right_nodes-1) = 3/2;%1;
    ind_count = ind_count+num_of_fine_right_nodes;
    i(ind_count:ind_count+num_of_fine_right_nodes-1) = fine_right;
    j(ind_count:ind_count+num_of_fine_right_nodes-1) = fine_right - 1*c_factor(1);
    v(ind_count:ind_count+num_of_fine_right_nodes-1) = -2;%-1;
    ind_count = ind_count+num_of_fine_right_nodes;
    i(ind_count:ind_count+num_of_fine_right_nodes-1) = fine_right;
    j(ind_count:ind_count+num_of_fine_right_nodes-1) = fine_right - 2*c_factor(1);
    v(ind_count:ind_count+num_of_fine_right_nodes-1) = 1/2;
    ind_count = ind_count+num_of_fine_right_nodes;
    % BCs bottom
    i(ind_count:ind_count+num_of_fine_bottom_nodes-1) = fine_bottom;
    j(ind_count:ind_count+num_of_fine_bottom_nodes-1) = fine_bottom;
    v(ind_count:ind_count+num_of_fine_bottom_nodes-1) = 3/2;%1;
    ind_count = ind_count+num_of_fine_bottom_nodes;
    i(ind_count:ind_count+num_of_fine_bottom_nodes-1) = fine_bottom;
    j(ind_count:ind_count+num_of_fine_bottom_nodes-1) = fine_bottom + X_f*Y_f*c_factor(3);
    v(ind_count:ind_count+num_of_fine_bottom_nodes-1) = -2;%-1;
    ind_count = ind_count+num_of_fine_bottom_nodes;
    i(ind_count:ind_count+num_of_fine_bottom_nodes-1) = fine_bottom;
    j(ind_count:ind_count+num_of_fine_bottom_nodes-1) = fine_bottom + 2*X_f*Y_f*c_factor(3);
    v(ind_count:ind_count+num_of_fine_bottom_nodes-1) = 1/2;
    ind_count = ind_count+num_of_fine_bottom_nodes;
    % BCs top
    i(ind_count:ind_count+num_of_fine_top_nodes-1) = fine_top_interface;
    j(ind_count:ind_count+num_of_fine_top_nodes-1) = fine_top_interface;
    v(ind_count:ind_count+num_of_fine_top_nodes-1) = 3/2;%1;
    ind_count = ind_count+num_of_fine_top_nodes;
    i(ind_count:ind_count+num_of_fine_top_nodes-1) = fine_top_interface;
    j(ind_count:ind_count+num_of_fine_top_nodes-1) = fine_top_interface - X_f*Y_f*c_factor(3);
    v(ind_count:ind_count+num_of_fine_top_nodes-1) = -2;%-1;
    ind_count = ind_count+num_of_fine_top_nodes;
    i(ind_count:ind_count+num_of_fine_top_nodes-1) = fine_top_interface;
    j(ind_count:ind_count+num_of_fine_top_nodes-1) = fine_top_interface - 2*X_f*Y_f*c_factor(3);
    v(ind_count:ind_count+num_of_fine_top_nodes-1) = 1/2;
    ind_count = ind_count+num_of_fine_top_nodes;
    % BCs front
    i(ind_count:ind_count+num_of_fine_front_nodes-1) = fine_front;
    j(ind_count:ind_count+num_of_fine_front_nodes-1) = fine_front;
    v(ind_count:ind_count+num_of_fine_front_nodes-1) = 3/2;%1;
    ind_count = ind_count+num_of_fine_front_nodes;
    i(ind_count:ind_count+num_of_fine_front_nodes-1) = fine_front;
    j(ind_count:ind_count+num_of_fine_front_nodes-1) = fine_front + X_f*c_factor(2);
    v(ind_count:ind_count+num_of_fine_front_nodes-1) = -2;%-1;
    ind_count = ind_count+num_of_fine_front_nodes;
    i(ind_count:ind_count+num_of_fine_front_nodes-1) = fine_front;
    j(ind_count:ind_count+num_of_fine_front_nodes-1) = fine_front + 2*X_f*c_factor(2);
    v(ind_count:ind_count+num_of_fine_front_nodes-1) = 1/2;
    ind_count = ind_count+num_of_fine_front_nodes;
    % BCs back
    i(ind_count:ind_count+num_of_fine_back_nodes-1) = fine_back;
    j(ind_count:ind_count+num_of_fine_back_nodes-1) = fine_back;
    v(ind_count:ind_count+num_of_fine_back_nodes-1) = 3/2;%1;
    ind_count = ind_count+num_of_fine_back_nodes;
    i(ind_count:ind_count+num_of_fine_back_nodes-1) = fine_back;
    j(ind_count:ind_count+num_of_fine_back_nodes-1) = fine_back - X_f*c_factor(2);
    v(ind_count:ind_count+num_of_fine_back_nodes-1) = -2;%-1;
    ind_count = ind_count+num_of_fine_back_nodes;
    i(ind_count:ind_count+num_of_fine_back_nodes-1) = fine_back;
    j(ind_count:ind_count+num_of_fine_back_nodes-1) = fine_back - 2*X_f*c_factor(2);
    v(ind_count:ind_count+num_of_fine_back_nodes-1) = 1/2;
    ind_count = ind_count+num_of_fine_back_nodes;

    A{mu_ind} = sparse(i(1:ind_count-1),j(1:ind_count-1),v(1:ind_count-1),dofs_f,dofs_f);
%     A{mu_ind} = inv(A{mu_ind});
end
%% Interpolation matrix
if (sum(c_factor) == 3) 
    shared_nodes = nodes_c;
elseif(c_factor(1) > 1 && c_factor(2) == 1 && c_factor(3) ==1 )
    shared_nodes = reshape(nodes_3d_f(1:c_factor:end,:,:),[],1);
    ipp_type_1   = reshape(nodes_3d_f(4:c_factor:X_f-3,:,:),[],1);
    ipp_type_2   = reshape(nodes_3d_f(2,:,:),[],1);
    ipp_type_3   = reshape(nodes_3d_f(X_f-1,:,:),[],1);
else
    shared_nodes = reshape(nodes_3d_f(1:c_factor:end,1:c_factor:end,1:c_factor:end),[],1);
    ipp_type_1   = reshape(nodes_3d_f(c_factor:c_factor:end,1:c_factor:end,1:c_factor:end),[],1);
    ipp_type_2   = reshape(nodes_3d_f(1:c_factor:end,c_factor:c_factor:end,1:c_factor:end),[],1);
    ipp_type_3   = reshape(nodes_3d_f(1:c_factor:end,1:c_factor:end,c_factor:c_factor:end),[],1);
    ipp_type_4   = reshape(nodes_3d_f(c_factor:c_factor:end,c_factor:c_factor:end,1:c_factor:end),[],1);
    ipp_type_5   = reshape(nodes_3d_f(c_factor:c_factor:end,1:c_factor:end,c_factor:c_factor:end),[],1);
    ipp_type_6   = reshape(nodes_3d_f(1:c_factor:end,c_factor:c_factor:end,c_factor:c_factor:end),[],1);
    ipp_type_7   = reshape(nodes_3d_f(c_factor:c_factor:end,c_factor:c_factor:end,c_factor:c_factor:end),[],1);
end

if (sum(c_factor) == 3)
    M_ipp = sparse(shared_nodes,nodes_c,1,dofs_f,dofs_c);
elseif (c_factor(1) > 1 && c_factor(2) == 1 && c_factor(3) ==1 )
    M_ipp = sparse(shared_nodes,nodes_c,1,dofs_f,dofs_c)+ ...
        sparse(ipp_type_1,reshape(nodes_3d_c(2:X_c-2,:,:),[],1),1/2,dofs_f,dofs_c) + ...
        sparse(ipp_type_1,reshape(nodes_3d_c(3:X_c-1,:,:),[],1),1/2,dofs_f,dofs_c) + ...
        sparse(ipp_type_2,reshape(nodes_3d_c(2,:,:),[],1),1,dofs_f,dofs_c) + ...
        sparse(ipp_type_3,reshape(nodes_3d_c(X_c-1,:,:),[],1),1,dofs_f,dofs_c);  
else
    M_ipp = sparse(shared_nodes,nodes_c,1,dofs_f,dofs_c)+ ...
        sparse(ipp_type_1,reshape(nodes_3d_c(1:X_c-1,:,:),[],1),1/2,dofs_f,dofs_c) + ...
        sparse(ipp_type_1,reshape(nodes_3d_c(2:X_c,:,:),[],1),1/2,dofs_f,dofs_c) + ...
        sparse(ipp_type_2,reshape(nodes_3d_c(:,1:Y_c-1,:),[],1),1/2,dofs_f,dofs_c) + ...
        sparse(ipp_type_2,reshape(nodes_3d_c(:,2:Y_c,:),[],1),1/2,dofs_f,dofs_c) + ...
        sparse(ipp_type_3,reshape(nodes_3d_c(:,:,1:Z_c-1),[],1),1/2,dofs_f,dofs_c) + ...
        sparse(ipp_type_3,reshape(nodes_3d_c(:,:,2:Z_c),[],1),1/2,dofs_f,dofs_c) + ...
        sparse(ipp_type_4,reshape(nodes_3d_c(1:X_c-1,1:Y_c-1,:),[],1),1/4,dofs_f,dofs_c) + ...
        sparse(ipp_type_4,reshape(nodes_3d_c(2:X_c,1:Y_c-1,:),[],1),1/4,dofs_f,dofs_c) + ...
        sparse(ipp_type_4,reshape(nodes_3d_c(1:X_c-1,2:Y_c,:),[],1),1/4,dofs_f,dofs_c) + ...
        sparse(ipp_type_4,reshape(nodes_3d_c(2:X_c,2:Y_c,:),[],1),1/4,dofs_f,dofs_c) + ...
        sparse(ipp_type_5,reshape(nodes_3d_c(1:X_c-1,:,1:Z_c-1),[],1),1/4,dofs_f,dofs_c) + ...
        sparse(ipp_type_5,reshape(nodes_3d_c(2:X_c,:,1:Z_c-1),[],1),1/4,dofs_f,dofs_c) + ...
        sparse(ipp_type_5,reshape(nodes_3d_c(1:X_c-1,:,2:Z_c),[],1),1/4,dofs_f,dofs_c) + ...
        sparse(ipp_type_5,reshape(nodes_3d_c(2:X_c,:,2:Z_c),[],1),1/4,dofs_f,dofs_c) + ...
        sparse(ipp_type_6,reshape(nodes_3d_c(:,1:Y_c-1,1:Z_c-1),[],1),1/4,dofs_f,dofs_c) + ...
        sparse(ipp_type_6,reshape(nodes_3d_c(:,2:Y_c,1:Z_c-1),[],1),1/4,dofs_f,dofs_c) + ...
        sparse(ipp_type_6,reshape(nodes_3d_c(:,1:Y_c-1,2:Z_c),[],1),1/4,dofs_f,dofs_c) + ...
        sparse(ipp_type_6,reshape(nodes_3d_c(:,2:Y_c,2:Z_c),[],1),1/4,dofs_f,dofs_c) + ...
        sparse(ipp_type_7,reshape(nodes_3d_c(1:X_c-1,1:Y_c-1,1:Z_c-1),[],1),1/8,dofs_f,dofs_c) + ...
        sparse(ipp_type_7,reshape(nodes_3d_c(2:X_c,1:Y_c-1,1:Z_c-1),[],1),1/8,dofs_f,dofs_c) + ...
        sparse(ipp_type_7,reshape(nodes_3d_c(1:X_c-1,2:Y_c,1:Z_c-1),[],1),1/8,dofs_f,dofs_c) + ...
        sparse(ipp_type_7,reshape(nodes_3d_c(2:X_c,2:Y_c,1:Z_c-1),[],1),1/8,dofs_f,dofs_c) + ...
        sparse(ipp_type_7,reshape(nodes_3d_c(1:X_c-1,1:Y_c-1,2:Z_c),[],1),1/8,dofs_f,dofs_c) + ...
        sparse(ipp_type_7,reshape(nodes_3d_c(2:X_c,1:Y_c-1,2:Z_c),[],1),1/8,dofs_f,dofs_c) + ...
        sparse(ipp_type_7,reshape(nodes_3d_c(1:X_c-1,2:Y_c,2:Z_c),[],1),1/8,dofs_f,dofs_c) + ...
        sparse(ipp_type_7,reshape(nodes_3d_c(2:X_c,2:Y_c,2:Z_c),[],1),1/8,dofs_f,dofs_c);
end

%% Setting Up Matrix B
for mu_ind=1:n_of_mus
    i = zeros(1,dofs_c*11);
    j = zeros(1,dofs_c*11);
    v = zeros(1,dofs_c*11);

    ind_count = 1; 


    fact_xx = (dt/(p.A_m(mu_ind)*p.C_m(mu_ind))*p.sigma_i(1,1)/dx_c^2);
    fact_yy = (dt/(p.A_m(mu_ind)*p.C_m(mu_ind))*p.sigma_i(2,2)/dy_c^2);
    fact_zz = (dt/(p.A_m(mu_ind)*p.C_m(mu_ind))*p.sigma_i(3,3)/dz_c^2);
    fact_xy = (dt/(p.A_m(mu_ind)*p.C_m(mu_ind))*(p.sigma_i(1,2)+p.sigma_i(2,1))/(4*dx_c*dy_c));
    w0 = 2*(fact_xx+fact_yy+fact_zz);
    % diag 
    i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
    j(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
    v(ind_count:ind_count+num_of_corse_inner_nodes-1) = w0;
    ind_count = ind_count+num_of_corse_inner_nodes;
    % u_xx
    i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
    j(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner + 1;
    v(ind_count:ind_count+num_of_corse_inner_nodes-1) = -fact_xx;
    ind_count = ind_count+num_of_corse_inner_nodes;
    i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
    j(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner - 1;
    v(ind_count:ind_count+num_of_corse_inner_nodes-1) = -fact_xx;
    ind_count = ind_count+num_of_corse_inner_nodes;
    % u_yy
    i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
    j(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner + X_c;
    v(ind_count:ind_count+num_of_corse_inner_nodes-1) = -fact_yy;
    ind_count = ind_count+num_of_corse_inner_nodes;
    i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
    j(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner - X_c;
    v(ind_count:ind_count+num_of_corse_inner_nodes-1) = -fact_yy;
    ind_count = ind_count+num_of_corse_inner_nodes;
    % u_zz
    i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
    j(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner + X_c*Y_c;
    v(ind_count:ind_count+num_of_corse_inner_nodes-1) = -fact_zz;
    ind_count = ind_count+num_of_corse_inner_nodes;
    i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
    j(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner - X_c*Y_c;
    v(ind_count:ind_count+num_of_corse_inner_nodes-1) = -fact_zz;
    ind_count = ind_count+num_of_corse_inner_nodes;
    % u_xy
    i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
    j(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner + (X_c+1);
    v(ind_count:ind_count+num_of_corse_inner_nodes-1) = -fact_xy;
    ind_count = ind_count+num_of_corse_inner_nodes;
    i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
    j(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner - (X_c+1);
    v(ind_count:ind_count+num_of_corse_inner_nodes-1) = -fact_xy;
    ind_count = ind_count+num_of_corse_inner_nodes;
    i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
    j(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner + (X_c-1);
    v(ind_count:ind_count+num_of_corse_inner_nodes-1) = fact_xy;
    ind_count = ind_count+num_of_corse_inner_nodes;
    i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
    j(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner - (X_c-1);
    v(ind_count:ind_count+num_of_corse_inner_nodes-1) = fact_xy;
    ind_count = ind_count+num_of_corse_inner_nodes;
    % BCs left
    i(ind_count:ind_count+num_of_corse_left_nodes-1) = corse_left;
    j(ind_count:ind_count+num_of_corse_left_nodes-1) = corse_left;
    v(ind_count:ind_count+num_of_corse_left_nodes-1) = -3/2;%-1;
    ind_count = ind_count+num_of_corse_left_nodes;
    i(ind_count:ind_count+num_of_corse_left_nodes-1) = corse_left;
    j(ind_count:ind_count+num_of_corse_left_nodes-1) = corse_left + 1;
    v(ind_count:ind_count+num_of_corse_left_nodes-1) = 2;%1;
    ind_count = ind_count+num_of_corse_left_nodes;
    i(ind_count:ind_count+num_of_corse_left_nodes-1) = corse_left;
    j(ind_count:ind_count+num_of_corse_left_nodes-1) = corse_left + 2;
    v(ind_count:ind_count+num_of_corse_left_nodes-1) = -1/2;
    ind_count = ind_count+num_of_corse_left_nodes;
    % BCs right
    i(ind_count:ind_count+num_of_corse_right_nodes-1) = corse_right;
    j(ind_count:ind_count+num_of_corse_right_nodes-1) = corse_right;
    v(ind_count:ind_count+num_of_corse_right_nodes-1) = -3/2;%-1;
    ind_count = ind_count+num_of_corse_right_nodes;
    i(ind_count:ind_count+num_of_corse_right_nodes-1) = corse_right;
    j(ind_count:ind_count+num_of_corse_right_nodes-1) = corse_right - 1;
    v(ind_count:ind_count+num_of_corse_right_nodes-1) = 2;%1;
    ind_count = ind_count+num_of_corse_right_nodes;
    i(ind_count:ind_count+num_of_corse_right_nodes-1) = corse_right;
    j(ind_count:ind_count+num_of_corse_right_nodes-1) = corse_right - 2;
    v(ind_count:ind_count+num_of_corse_right_nodes-1) = -1/2;
    ind_count = ind_count+num_of_corse_right_nodes;
    % BCs bottom
    i(ind_count:ind_count+num_of_corse_bottom_nodes-1) = corse_bottom;
    j(ind_count:ind_count+num_of_corse_bottom_nodes-1) = corse_bottom;
    v(ind_count:ind_count+num_of_corse_bottom_nodes-1) = -3/2;%-1;
    ind_count = ind_count+num_of_corse_bottom_nodes;
    i(ind_count:ind_count+num_of_corse_bottom_nodes-1) = corse_bottom;
    j(ind_count:ind_count+num_of_corse_bottom_nodes-1) = corse_bottom + X_c*Y_c;
    v(ind_count:ind_count+num_of_corse_bottom_nodes-1) = 2;%1;
    ind_count = ind_count+num_of_corse_bottom_nodes;
    i(ind_count:ind_count+num_of_corse_bottom_nodes-1) = corse_bottom;
    j(ind_count:ind_count+num_of_corse_bottom_nodes-1) = corse_bottom + 2*X_c*Y_c;
    v(ind_count:ind_count+num_of_corse_bottom_nodes-1) = -1/2;
    ind_count = ind_count+num_of_corse_bottom_nodes;
    % BCs top
    i(ind_count:ind_count+num_of_corse_top_nodes-1) = corse_top_interface;
    j(ind_count:ind_count+num_of_corse_top_nodes-1) = corse_top_interface;
    v(ind_count:ind_count+num_of_corse_top_nodes-1) = -3/2;%-1;
    ind_count = ind_count+num_of_corse_top_nodes;
    i(ind_count:ind_count+num_of_corse_top_nodes-1) = corse_top_interface;
    j(ind_count:ind_count+num_of_corse_top_nodes-1) = corse_top_interface - X_c*Y_c;
    v(ind_count:ind_count+num_of_corse_top_nodes-1) = 2;%1;
    ind_count = ind_count+num_of_corse_top_nodes;
    i(ind_count:ind_count+num_of_corse_top_nodes-1) = corse_top_interface;
    j(ind_count:ind_count+num_of_corse_top_nodes-1) = corse_top_interface - 2*X_c*Y_c;
    v(ind_count:ind_count+num_of_corse_top_nodes-1) = -1/2;
    ind_count = ind_count+num_of_corse_top_nodes;
    % BCs front
    i(ind_count:ind_count+num_of_corse_front_nodes-1) = corse_front;
    j(ind_count:ind_count+num_of_corse_front_nodes-1) = corse_front;
    v(ind_count:ind_count+num_of_corse_front_nodes-1) = -3/2;%-1;
    ind_count = ind_count+num_of_corse_front_nodes;
    i(ind_count:ind_count+num_of_corse_front_nodes-1) = corse_front;
    j(ind_count:ind_count+num_of_corse_front_nodes-1) = corse_front + X_c;
    v(ind_count:ind_count+num_of_corse_front_nodes-1) = 2;%1;
    ind_count = ind_count+num_of_corse_front_nodes;
    i(ind_count:ind_count+num_of_corse_front_nodes-1) = corse_front;
    j(ind_count:ind_count+num_of_corse_front_nodes-1) = corse_front + 2*X_c;
    v(ind_count:ind_count+num_of_corse_front_nodes-1) = -1/2;%1;
    ind_count = ind_count+num_of_corse_front_nodes;
    % BCs back
    i(ind_count:ind_count+num_of_corse_back_nodes-1) = corse_back;
    j(ind_count:ind_count+num_of_corse_back_nodes-1) = corse_back;
    v(ind_count:ind_count+num_of_corse_back_nodes-1) = -3/2;%-1;
    ind_count = ind_count+num_of_corse_back_nodes;
    i(ind_count:ind_count+num_of_corse_back_nodes-1) = corse_back;
    j(ind_count:ind_count+num_of_corse_back_nodes-1) = corse_back - X_c;
    v(ind_count:ind_count+num_of_corse_back_nodes-1) = 2;%1;
    ind_count = ind_count+num_of_corse_back_nodes;
    i(ind_count:ind_count+num_of_corse_back_nodes-1) = corse_back;
    j(ind_count:ind_count+num_of_corse_back_nodes-1) = corse_back - 2*X_c;
    v(ind_count:ind_count+num_of_corse_back_nodes-1) = -1/2;
    ind_count = ind_count+num_of_corse_back_nodes;

    B{mu_ind} = sparse(i(1:ind_count-1),j(1:ind_count-1),v(1:ind_count-1),dofs_c,dofs_c);
end



%% Building homogenization matrix


if (sum(c_factor) == 3)
    shared_inner_nodes = corse_inner;
elseif (c_factor(1) > 1 && c_factor(2) == 1 && c_factor(3) ==1 )
    shared_inner_nodes = reshape(nodes_3d_f(1+c_factor(1):c_factor(1):X_f-c_factor(1),2:Y_f-1,2:Z_f-1),[],1)';
else
    shared_inner_nodes = reshape(nodes_3d_f(1+c_factor(1):c_factor(1):X_f-c_factor(1)...
        ,1+c_factor(2):c_factor(2):Y_f-c_factor(2),1+c_factor(3):c_factor(3):Z_f-c_factor(3)),[],1)';
end


%% Setting Up Matrices C

for mu_ind=1:n_of_mus
    i = zeros(1,dofs_f*11);
    j = zeros(1,dofs_f*11);
    v = zeros(1,dofs_f*11);

    ind_count = 1; 
    
    if (length(p.f_k{mu_ind}) == 1)
        f_r = p.f_k{mu_ind}*ones(num_of_corse_inner_nodes,1);
    else
        f_r = reshape(p.f_k{mu_ind},[],1)';
    end
%     gg = isfinite(1./f_r);
   
    fact_xx = f_r.*p.sigma_i(1,1)./dx_c^2;
%     fact_yy = f_r.*p.sigma_i(2,2)./dy_f^2;
%     fact_zz = f_r.*p.sigma_i(3,3)./dz_f^2;
%     fact_xy = f_r.*(p.sigma_i(1,2)+p.sigma_i(2,1))/(4*dx_f*dy_f);
%     w0 = -2*(fact_xx+fact_yy+fact_zz);
    % diag 
    i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
    j(ind_count:ind_count+num_of_corse_inner_nodes-1) = shared_inner_nodes;
    v(ind_count:ind_count+num_of_corse_inner_nodes-1) = -2*fact_xx;
    ind_count = ind_count+num_of_corse_inner_nodes;
    % u_xx
    i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
    j(ind_count:ind_count+num_of_corse_inner_nodes-1) = shared_inner_nodes + c_factor(1);
    v(ind_count:ind_count+num_of_corse_inner_nodes-1) = 1*fact_xx;
    ind_count = ind_count+num_of_corse_inner_nodes;
    i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
    j(ind_count:ind_count+num_of_corse_inner_nodes-1) = shared_inner_nodes - c_factor(1);
    v(ind_count:ind_count+num_of_corse_inner_nodes-1) = 1*fact_xx;
    ind_count = ind_count+num_of_corse_inner_nodes;
    
    C{mu_ind} = sparse(i(1:ind_count-1),j(1:ind_count-1),v(1:ind_count-1),dofs_c,dofs_f);
   
end



%% Setting Up Matrix D

i = zeros(1,dofs_c*11);
j = zeros(1,dofs_c*11);
v = zeros(1,dofs_c*11);

ind_count = 1;

fact_xx = (p.sigma_e(1,1)+p.sigma_i(1,1))/dx_c^2;
fact_yy = (p.sigma_e(2,2)+p.sigma_i(2,2))/dy_c^2;
fact_zz = (p.sigma_e(3,3)+p.sigma_i(3,3))/dz_c^2;
fact_xy = (p.sigma_e(1,2)+p.sigma_i(1,2)+p.sigma_e(2,1)+p.sigma_i(2,1))/(4*dx_c*dy_c);
w0 = -2*(fact_xx+fact_yy+fact_zz);
% diag 
i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
j(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
v(ind_count:ind_count+num_of_corse_inner_nodes-1) = w0;
ind_count = ind_count+num_of_corse_inner_nodes;
% u_xx
i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
j(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner + 1;
v(ind_count:ind_count+num_of_corse_inner_nodes-1) = fact_xx;
ind_count = ind_count+num_of_corse_inner_nodes;
i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
j(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner - 1;
v(ind_count:ind_count+num_of_corse_inner_nodes-1) = fact_xx;
ind_count = ind_count+num_of_corse_inner_nodes;
% u_yy
i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
j(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner + X_c;
v(ind_count:ind_count+num_of_corse_inner_nodes-1) = fact_yy;
ind_count = ind_count+num_of_corse_inner_nodes;
i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
j(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner - X_c;
v(ind_count:ind_count+num_of_corse_inner_nodes-1) = fact_yy;
ind_count = ind_count+num_of_corse_inner_nodes;
% u_zz
i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
j(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner + X_c*Y_c;
v(ind_count:ind_count+num_of_corse_inner_nodes-1) = fact_zz;
ind_count = ind_count+num_of_corse_inner_nodes;
i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
j(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner - X_c*Y_c;
v(ind_count:ind_count+num_of_corse_inner_nodes-1) = fact_zz;
ind_count = ind_count+num_of_corse_inner_nodes;
% u_xy
i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
j(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner + (X_c+1);
v(ind_count:ind_count+num_of_corse_inner_nodes-1) = fact_xy;
ind_count = ind_count+num_of_corse_inner_nodes;
i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
j(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner - (X_c+1);
v(ind_count:ind_count+num_of_corse_inner_nodes-1) = fact_xy;
ind_count = ind_count+num_of_corse_inner_nodes;
i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
j(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner + (X_c-1);
v(ind_count:ind_count+num_of_corse_inner_nodes-1) = -fact_xy;
ind_count = ind_count+num_of_corse_inner_nodes;
i(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner;
j(ind_count:ind_count+num_of_corse_inner_nodes-1) = corse_inner - (X_c-1);
v(ind_count:ind_count+num_of_corse_inner_nodes-1) = -fact_xy;
ind_count = ind_count+num_of_corse_inner_nodes;
% BCs left
i(ind_count:ind_count+num_of_corse_left_nodes-1) = corse_left;
j(ind_count:ind_count+num_of_corse_left_nodes-1) = corse_left;
v(ind_count:ind_count+num_of_corse_left_nodes-1) = 3/2;%1;
ind_count = ind_count+num_of_corse_left_nodes;
i(ind_count:ind_count+num_of_corse_left_nodes-1) = corse_left;
j(ind_count:ind_count+num_of_corse_left_nodes-1) = corse_left + 1;
v(ind_count:ind_count+num_of_corse_left_nodes-1) = -2;%-1;
ind_count = ind_count+num_of_corse_left_nodes;
i(ind_count:ind_count+num_of_corse_left_nodes-1) = corse_left;
j(ind_count:ind_count+num_of_corse_left_nodes-1) = corse_left + 2;
v(ind_count:ind_count+num_of_corse_left_nodes-1) = 1/2;
ind_count = ind_count+num_of_corse_left_nodes;
% BCs right
i(ind_count:ind_count+num_of_corse_right_nodes-1) = corse_right;
j(ind_count:ind_count+num_of_corse_right_nodes-1) = corse_right;
v(ind_count:ind_count+num_of_corse_right_nodes-1) = 3/2;%1;
ind_count = ind_count+num_of_corse_right_nodes;
i(ind_count:ind_count+num_of_corse_right_nodes-1) = corse_right;
j(ind_count:ind_count+num_of_corse_right_nodes-1) = corse_right - 1;
v(ind_count:ind_count+num_of_corse_right_nodes-1) = -2;%-1;
ind_count = ind_count+num_of_corse_right_nodes;
i(ind_count:ind_count+num_of_corse_right_nodes-1) = corse_right;
j(ind_count:ind_count+num_of_corse_right_nodes-1) = corse_right - 2;
v(ind_count:ind_count+num_of_corse_right_nodes-1) = 1/2;%-1;
ind_count = ind_count+num_of_corse_right_nodes;
% BCs bottom
i(ind_count:ind_count+num_of_corse_bottom_nodes-1) = corse_bottom;
j(ind_count:ind_count+num_of_corse_bottom_nodes-1) = corse_bottom;
v(ind_count:ind_count+num_of_corse_bottom_nodes-1) = 1;
ind_count = ind_count+num_of_corse_bottom_nodes;
i(ind_count:ind_count+num_of_corse_bottom_nodes-1) = corse_bottom;
j(ind_count:ind_count+num_of_corse_bottom_nodes-1) = corse_bottom + X_c*Y_c;
v(ind_count:ind_count+num_of_corse_bottom_nodes-1) = 0;%-1;
ind_count = ind_count+num_of_corse_bottom_nodes;
% BCs top
i(ind_count:ind_count+num_of_corse_top_nodes-1) = corse_top_interface;
j(ind_count:ind_count+num_of_corse_top_nodes-1) = corse_top_interface;
v(ind_count:ind_count+num_of_corse_top_nodes-1) = 3/2;%1;
ind_count = ind_count+num_of_corse_top_nodes;
i(ind_count:ind_count+num_of_corse_top_nodes-1) = corse_top_interface;
j(ind_count:ind_count+num_of_corse_top_nodes-1) = corse_top_interface - X_c*Y_c;
v(ind_count:ind_count+num_of_corse_top_nodes-1) = -2;%-1;
ind_count = ind_count+num_of_corse_top_nodes;
i(ind_count:ind_count+num_of_corse_top_nodes-1) = corse_top_interface;
j(ind_count:ind_count+num_of_corse_top_nodes-1) = corse_top_interface - 2*X_c*Y_c;
v(ind_count:ind_count+num_of_corse_top_nodes-1) = 1/2;%-1;
ind_count = ind_count+num_of_corse_top_nodes;
% BCs front
i(ind_count:ind_count+num_of_corse_front_nodes-1) = corse_front;
j(ind_count:ind_count+num_of_corse_front_nodes-1) = corse_front;
v(ind_count:ind_count+num_of_corse_front_nodes-1) = 3/2;%1;
ind_count = ind_count+num_of_corse_front_nodes;
i(ind_count:ind_count+num_of_corse_front_nodes-1) = corse_front;
j(ind_count:ind_count+num_of_corse_front_nodes-1) = corse_front + X_c;
v(ind_count:ind_count+num_of_corse_front_nodes-1) = -2;%-1;
ind_count = ind_count+num_of_corse_front_nodes;
i(ind_count:ind_count+num_of_corse_front_nodes-1) = corse_front;
j(ind_count:ind_count+num_of_corse_front_nodes-1) = corse_front + 2*X_c;
v(ind_count:ind_count+num_of_corse_front_nodes-1) = 1/2;%-1;
ind_count = ind_count+num_of_corse_front_nodes;
% BCs back
i(ind_count:ind_count+num_of_corse_back_nodes-1) = corse_back;
j(ind_count:ind_count+num_of_corse_back_nodes-1) = corse_back;
v(ind_count:ind_count+num_of_corse_back_nodes-1) = 3/2;%1;
ind_count = ind_count+num_of_corse_back_nodes;
i(ind_count:ind_count+num_of_corse_back_nodes-1) = corse_back;
j(ind_count:ind_count+num_of_corse_back_nodes-1) = corse_back - X_c;
v(ind_count:ind_count+num_of_corse_back_nodes-1) = -2;%-1;
ind_count = ind_count+num_of_corse_back_nodes;
i(ind_count:ind_count+num_of_corse_back_nodes-1) = corse_back;
j(ind_count:ind_count+num_of_corse_back_nodes-1) = corse_back - 2*X_c;
v(ind_count:ind_count+num_of_corse_back_nodes-1) = 1/2;%-1;
ind_count = ind_count+num_of_corse_back_nodes;

D = sparse(i(1:ind_count-1),j(1:ind_count-1),v(1:ind_count-1),dofs_c,dofs_c);
%%
M1 = [];
M2 = [];
M3 = [];
M_z = [];

for mu_idx=1:p.Number_of_MUs
    M1 = blkdiag(M1,A{mu_idx});
    M2 = vertcat(M2,M_ipp*B{mu_idx});
    M3 = horzcat(M3,C{mu_idx});
end
M = [M1,M2;M3,D];

end

