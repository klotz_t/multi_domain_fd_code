function [surface_nodes] = get_surface_nodes(X,Y,Z)
% Get the surface nodes for a cube like domain 
dofs = X*Y*Z;
nodes = 1:dofs;
nodes_3d = reshape(nodes,X,Y,Z);

surface_nodes.left = reshape(nodes_3d(1:1,1:Y,1:Z),[],1)';%(X*Y+1):X:(dofs-X*Y);
surface_nodes.right = reshape(nodes_3d(X:X,1:Y,1:Z),[],1)';%(X*Y+X):X:(dofs-X*Y);
surface_nodes.front = reshape(nodes_3d(2:X-1,1:1,1:Z),[],1)';
surface_nodes.back = reshape(nodes_3d(2:X-1,Y:Y,1:Z),[],1)';
surface_nodes.bottom = reshape(nodes_3d(2:X-1,2:Y-1,1:1),[],1)';%1:1:(X*Y);
surface_nodes.top = reshape(nodes_3d(2:X-1,2:Y-1,Z:Z),[],1)';%(dofs-X*Y+1):1:dofs;
end