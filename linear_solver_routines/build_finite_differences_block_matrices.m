function [M,A,B,C,D,F,S] = build_finite_differences_block_matrices(dt,grid,p,include_fat,include_skin,BC)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
F=[];
S=[];
%% Get Variables
X = grid.X;
Y = grid.Y;
Z = grid.Z;
dx = grid.dx;
dy = grid.dy;
dz = grid.dz;

n_of_mus = p.Number_of_MUs;
dofs = X*Y*Z;
nodes = 1:dofs;
nodes_3d = reshape(nodes,X,Y,Z);

%% Get Surface and Innner Muscle Nodes
muscle_inner = reshape(nodes_3d(2:X-1,2:Y-1,2:Z-1),[],1)';
muscle_left = reshape(nodes_3d(1:1,1:Y,1:Z),[],1)';%(X*Y+1):X:(dofs-X*Y);
muscle_right = reshape(nodes_3d(X:X,1:Y,1:Z),[],1)';%(X*Y+X):X:(dofs-X*Y);
muscle_front = reshape(nodes_3d(2:X-1,1:1,1:Z),[],1)';
muscle_back = reshape(nodes_3d(2:X-1,Y:Y,1:Z),[],1)';
muscle_bottom = reshape(nodes_3d(2:X-1,2:Y-1,1:1),[],1)';%1:1:(X*Y);
muscle_top_interface = reshape(nodes_3d(2:X-1,2:Y-1,Z:Z),[],1)';%(dofs-X*Y+1):1:dofs;

num_of_muscle_bottom_nodes = length(muscle_bottom);
num_of_muscle_top_nodes = length(muscle_top_interface);
num_of_muscle_left_nodes = length(muscle_left);
num_of_muscle_right_nodes = length(muscle_right);
num_of_muscle_back_nodes = length(muscle_back);
num_of_muscle_front_nodes = length(muscle_front);
num_of_muscle_inner_nodes = length(muscle_inner);

%% Setting Up Matrix A
for mu_ind=1:n_of_mus
    i = zeros(1,dofs*11);
    j = zeros(1,dofs*11);
    v = zeros(1,dofs*11);

    ind_count = 1;

    if (size(p.sigma_i,3)==1)
        sigma_i = p.sigma_i;
    else
           sigma_i = p.sigma_i(:,:,mu_ind); 
    end
    fact_xx = (dt/(p.A_m(mu_ind)*p.C_m(mu_ind))*sigma_i(1,1)/dx^2);
    fact_yy = (dt/(p.A_m(mu_ind)*p.C_m(mu_ind))*sigma_i(2,2)/dy^2); 
    fact_zz = (dt/(p.A_m(mu_ind)*p.C_m(mu_ind))*sigma_i(3,3)/dz^2);
    fact_xy = (dt/(p.A_m(mu_ind)*p.C_m(mu_ind))*(sigma_i(1,2)+sigma_i(2,1))/(4*dx*dy));
    fact_xz = (dt/(p.A_m(mu_ind)*p.C_m(mu_ind))*(sigma_i(1,3)+sigma_i(3,1))/(4*dx*dz));
    w0 = (1+2*fact_xx+2*fact_yy+2*fact_zz);
    % diag 
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = w0;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    % u_xx
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + 1;
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_xx;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - 1;
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_xx;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    % u_yy
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + X;
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_yy;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - X;
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_yy;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    % u_zz
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + X*Y;
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_zz;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - X*Y;
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_zz;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    % u_xy
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + (X+1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_xy;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - (X+1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_xy;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + (X-1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_xy;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - (X-1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_xy;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    % u_xz
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + (X*Y+1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_xz;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - (X*Y+1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_xz;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + (X*Y-1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_xz;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - (X*Y-1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_xz;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    % BCs left
    i(ind_count:ind_count+num_of_muscle_left_nodes-1) = muscle_left;
    j(ind_count:ind_count+num_of_muscle_left_nodes-1) = muscle_left;
    v(ind_count:ind_count+num_of_muscle_left_nodes-1) = 3/2*sigma_i(1,1)/dx;
    ind_count = ind_count+num_of_muscle_left_nodes;
    i(ind_count:ind_count+num_of_muscle_left_nodes-1) = muscle_left;
    j(ind_count:ind_count+num_of_muscle_left_nodes-1) = muscle_left + 1;
    v(ind_count:ind_count+num_of_muscle_left_nodes-1) = -2*sigma_i(1,1)/dx;
    ind_count = ind_count+num_of_muscle_left_nodes;
    i(ind_count:ind_count+num_of_muscle_left_nodes-1) = muscle_left;
    j(ind_count:ind_count+num_of_muscle_left_nodes-1) = muscle_left + 2;
    v(ind_count:ind_count+num_of_muscle_left_nodes-1) = 1/2*sigma_i(1,1)/dx;
    ind_count = ind_count+num_of_muscle_left_nodes;
    % BCs right
    i(ind_count:ind_count+num_of_muscle_right_nodes-1) = muscle_right;
    j(ind_count:ind_count+num_of_muscle_right_nodes-1) = muscle_right;
    v(ind_count:ind_count+num_of_muscle_right_nodes-1) = 3/2*sigma_i(1,1)/dx;
    ind_count = ind_count+num_of_muscle_right_nodes;
    i(ind_count:ind_count+num_of_muscle_right_nodes-1) = muscle_right;
    j(ind_count:ind_count+num_of_muscle_right_nodes-1) = muscle_right - 1;
    v(ind_count:ind_count+num_of_muscle_right_nodes-1) = -2*sigma_i(1,1)/dx;
    ind_count = ind_count+num_of_muscle_right_nodes;
    i(ind_count:ind_count+num_of_muscle_right_nodes-1) = muscle_right;
    j(ind_count:ind_count+num_of_muscle_right_nodes-1) = muscle_right - 2;
    v(ind_count:ind_count+num_of_muscle_right_nodes-1) = 1/2*sigma_i(1,1)/dx;
    ind_count = ind_count+num_of_muscle_right_nodes;
    % BCs bottom
    if (sigma_i(3,3) == 0)
        i(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
        j(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
        v(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = w0;
        ind_count = ind_count+num_of_muscle_bottom_nodes;
        i(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
        j(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom + 1;
        v(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = -fact_xx;
        ind_count = ind_count+num_of_muscle_bottom_nodes;
        i(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
        j(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom - 1;
        v(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = -fact_xx;
        ind_count = ind_count+num_of_muscle_bottom_nodes;
    else
        i(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
        j(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
        v(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = 3/2*sigma_i(3,3)/dz;
        ind_count = ind_count+num_of_muscle_bottom_nodes;
        i(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
        j(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom + X*Y;
        v(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = -2*sigma_i(3,3)/dz;
        ind_count = ind_count+num_of_muscle_bottom_nodes;
        i(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
        j(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom + 2*X*Y;
        v(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = 1/2*sigma_i(3,3)/dz;
        ind_count = ind_count+num_of_muscle_bottom_nodes;
    end
    % BCs top
    if (sigma_i(3,3) == 0)
        i(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface;
        j(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface;
        v(ind_count:ind_count+num_of_muscle_top_nodes-1) = w0;
        ind_count = ind_count+num_of_muscle_top_nodes;
        i(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface;
        j(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface + 1;
        v(ind_count:ind_count+num_of_muscle_top_nodes-1) = -fact_xx;
        ind_count = ind_count+num_of_muscle_top_nodes;
        i(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface;
        j(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface - 1;
        v(ind_count:ind_count+num_of_muscle_top_nodes-1) = -fact_xx;
        ind_count = ind_count+num_of_muscle_top_nodes;
    else
        i(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface;
        j(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface;
        v(ind_count:ind_count+num_of_muscle_top_nodes-1) = 3/2*sigma_i(3,3)/dz;
        ind_count = ind_count+num_of_muscle_top_nodes;
        i(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface;
        j(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface - X*Y;
        v(ind_count:ind_count+num_of_muscle_top_nodes-1) = -2*sigma_i(3,3)/dz;
        ind_count = ind_count+num_of_muscle_top_nodes;
        i(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface;
        j(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface - 2*X*Y;
        v(ind_count:ind_count+num_of_muscle_top_nodes-1) = 1/2*sigma_i(3,3)/dz;
        ind_count = ind_count+num_of_muscle_top_nodes;
    end
    % BCs front
    if (sigma_i(2,2) == 0)
        i(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front;
        j(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front;
        v(ind_count:ind_count+num_of_muscle_front_nodes-1) = w0;
        ind_count = ind_count+num_of_muscle_front_nodes;
        i(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front;
        j(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front + 1;
        v(ind_count:ind_count+num_of_muscle_front_nodes-1) = -fact_xx;
        ind_count = ind_count+num_of_muscle_front_nodes;
        i(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front;
        j(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front - 1;
        v(ind_count:ind_count+num_of_muscle_front_nodes-1) = -fact_xx;
        ind_count = ind_count+num_of_muscle_front_nodes;    
    else
        i(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front;
        j(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front;
        v(ind_count:ind_count+num_of_muscle_front_nodes-1) = 3/2*sigma_i(2,2)/dy;
        ind_count = ind_count+num_of_muscle_front_nodes;
        i(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front;
        j(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front + X;
        v(ind_count:ind_count+num_of_muscle_front_nodes-1) = -2*sigma_i(2,2)/dy;
        ind_count = ind_count+num_of_muscle_front_nodes;
        i(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front;
        j(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front + 2*X;
        v(ind_count:ind_count+num_of_muscle_front_nodes-1) = 1/2*sigma_i(2,2)/dy;
        ind_count = ind_count+num_of_muscle_front_nodes; 
    end
    % BCs back
    if (sigma_i(2,2) == 0)
        i(ind_count:ind_count+num_of_muscle_back_nodes-1) = muscle_back;
        j(ind_count:ind_count+num_of_muscle_back_nodes-1) = muscle_back;
        v(ind_count:ind_count+num_of_muscle_back_nodes-1) = w0;
        ind_count = ind_count+num_of_muscle_back_nodes;
        i(ind_count:ind_count+num_of_muscle_back_nodes-1) = muscle_back;
        j(ind_count:ind_count+num_of_muscle_back_nodes-1) = muscle_back + 1;
        v(ind_count:ind_count+num_of_muscle_back_nodes-1) = -fact_xx;
        ind_count = ind_count+num_of_muscle_back_nodes;
        i(ind_count:ind_count+num_of_muscle_back_nodes-1) = muscle_back;
        j(ind_count:ind_count+num_of_muscle_back_nodes-1) = muscle_back - 1;
        v(ind_count:ind_count+num_of_muscle_back_nodes-1) = -fact_xx;
        ind_count = ind_count+num_of_muscle_back_nodes;
    else
        i(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_back;
        j(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_back;
        v(ind_count:ind_count+num_of_muscle_front_nodes-1) = 3/2*sigma_i(2,2)/dy;
        ind_count = ind_count+num_of_muscle_front_nodes;
        i(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_back;
        j(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_back - X;
        v(ind_count:ind_count+num_of_muscle_front_nodes-1) = -2*sigma_i(2,2)/dy;
        ind_count = ind_count+num_of_muscle_front_nodes;
        i(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_back;
        j(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_back - 2*X;
        v(ind_count:ind_count+num_of_muscle_front_nodes-1) = 1/2*sigma_i(2,2)/dy;
        ind_count = ind_count+num_of_muscle_front_nodes; 
    end

    A{mu_ind} = sparse(i(1:ind_count-1),j(1:ind_count-1),v(1:ind_count-1),dofs,dofs);
%     A{mu_ind} = inv(A{mu_ind});
end

%% Setting Up Matrix B
for mu_ind=1:n_of_mus
    i = zeros(1,dofs*11);
    j = zeros(1,dofs*11);
    v = zeros(1,dofs*11);

    ind_count = 1; 

    if (size(p.sigma_i,3)==1)
        sigma_i = p.sigma_i;
    else
        sigma_i = p.sigma_i(:,:,mu_ind); 
    end
    fact_xx = (dt/(p.A_m(mu_ind)*p.C_m(mu_ind))*sigma_i(1,1)/dx^2);
    fact_yy = (dt/(p.A_m(mu_ind)*p.C_m(mu_ind))*sigma_i(2,2)/dy^2);
    fact_zz = (dt/(p.A_m(mu_ind)*p.C_m(mu_ind))*sigma_i(3,3)/dz^2);
    fact_xy = (dt/(p.A_m(mu_ind)*p.C_m(mu_ind))*(sigma_i(1,2)+sigma_i(2,1))/(4*dx*dy));
    fact_xz = (dt/(p.A_m(mu_ind)*p.C_m(mu_ind))*(sigma_i(1,3)+sigma_i(3,1))/(4*dx*dz));
    w0 = 2*(fact_xx+fact_yy+fact_zz);
    % diag 
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = w0;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    % u_xx
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + 1;
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_xx;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - 1;
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_xx;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    % u_yy
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + X;
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_yy;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - X;
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_yy;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    % u_zz
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + X*Y;
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_zz;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - X*Y;
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_zz;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    % u_xy
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + (X+1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_xy;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - (X+1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_xy;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + (X-1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_xy;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - (X-1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_xy;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    % u_xz
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + (X*Y+1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_xz;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - (X*Y+1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_xz;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + (X*Y-1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_xz;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - (X*Y-1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_xz;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    % BCs left
    i(ind_count:ind_count+num_of_muscle_left_nodes-1) = muscle_left;
    j(ind_count:ind_count+num_of_muscle_left_nodes-1) = muscle_left;
    v(ind_count:ind_count+num_of_muscle_left_nodes-1) = 3/2*sigma_i(1,1)/dx;
    ind_count = ind_count+num_of_muscle_left_nodes;
    i(ind_count:ind_count+num_of_muscle_left_nodes-1) = muscle_left;
    j(ind_count:ind_count+num_of_muscle_left_nodes-1) = muscle_left + 1;
    v(ind_count:ind_count+num_of_muscle_left_nodes-1) = -2*sigma_i(1,1)/dx;
    ind_count = ind_count+num_of_muscle_left_nodes;
    i(ind_count:ind_count+num_of_muscle_left_nodes-1) = muscle_left;
    j(ind_count:ind_count+num_of_muscle_left_nodes-1) = muscle_left + 2;
    v(ind_count:ind_count+num_of_muscle_left_nodes-1) = 1/2*sigma_i(1,1)/dx;
    ind_count = ind_count+num_of_muscle_left_nodes;
    % BCs right
    i(ind_count:ind_count+num_of_muscle_right_nodes-1) = muscle_right;
    j(ind_count:ind_count+num_of_muscle_right_nodes-1) = muscle_right;
    v(ind_count:ind_count+num_of_muscle_right_nodes-1) = 3/2*sigma_i(1,1)/dx;
    ind_count = ind_count+num_of_muscle_right_nodes;
    i(ind_count:ind_count+num_of_muscle_right_nodes-1) = muscle_right;
    j(ind_count:ind_count+num_of_muscle_right_nodes-1) = muscle_right - 1;
    v(ind_count:ind_count+num_of_muscle_right_nodes-1) = -2*sigma_i(1,1)/dx;
    ind_count = ind_count+num_of_muscle_right_nodes;
    i(ind_count:ind_count+num_of_muscle_right_nodes-1) = muscle_right;
    j(ind_count:ind_count+num_of_muscle_right_nodes-1) = muscle_right - 2;
    v(ind_count:ind_count+num_of_muscle_right_nodes-1) = 1/2*sigma_i(1,1)/dx;
    ind_count = ind_count+num_of_muscle_right_nodes;
    % BCs bottom
    if (sigma_i(3,3) == 0)
        i(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
        j(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
        v(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = w0;
        ind_count = ind_count+num_of_muscle_bottom_nodes;
        i(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
        j(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom + 1;
        v(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = -fact_xx;
        ind_count = ind_count+num_of_muscle_bottom_nodes;
        i(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
        j(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom - 1;
        v(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = -fact_xx;
        ind_count = ind_count+num_of_muscle_bottom_nodes;
    else
        i(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
        j(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
        v(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = 3/2*sigma_i(3,3)/dz;
        ind_count = ind_count+num_of_muscle_bottom_nodes;
        i(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
        j(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom + X*Y;
        v(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = -2*sigma_i(3,3)/dz;
        ind_count = ind_count+num_of_muscle_bottom_nodes;
        i(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
        j(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom + 2*X*Y;
        v(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = 1/2*sigma_i(3,3)/dz;
        ind_count = ind_count+num_of_muscle_bottom_nodes;
    end
    % BCs top
    if (sigma_i(3,3) == 0)
        i(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface;
        j(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface;
        v(ind_count:ind_count+num_of_muscle_top_nodes-1) = w0;
        ind_count = ind_count+num_of_muscle_top_nodes;
        i(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface;
        j(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface + 1;
        v(ind_count:ind_count+num_of_muscle_top_nodes-1) = -fact_xx;
        ind_count = ind_count+num_of_muscle_top_nodes;
        i(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface;
        j(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface - 1;
        v(ind_count:ind_count+num_of_muscle_top_nodes-1) = -fact_xx;
        ind_count = ind_count+num_of_muscle_top_nodes;
    else
        i(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface;
        j(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface;
        v(ind_count:ind_count+num_of_muscle_top_nodes-1) = 3/2*sigma_i(3,3)/dz;
        ind_count = ind_count+num_of_muscle_top_nodes;
        i(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface;
        j(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface - X*Y;
        v(ind_count:ind_count+num_of_muscle_top_nodes-1) = -2*sigma_i(3,3)/dz;
        ind_count = ind_count+num_of_muscle_top_nodes;
        i(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface;
        j(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface - 2*X*Y;
        v(ind_count:ind_count+num_of_muscle_top_nodes-1) = 1/2*sigma_i(3,3)/dz;
        ind_count = ind_count+num_of_muscle_top_nodes;
    end
    % BCs front
    if (sigma_i(2,2) == 0)
        i(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front;
        j(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front;
        v(ind_count:ind_count+num_of_muscle_front_nodes-1) = w0;
        ind_count = ind_count+num_of_muscle_front_nodes;
        i(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front;
        j(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front + 1;
        v(ind_count:ind_count+num_of_muscle_front_nodes-1) = -fact_xx;
        ind_count = ind_count+num_of_muscle_front_nodes;
        i(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front;
        j(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front - 1;
        v(ind_count:ind_count+num_of_muscle_front_nodes-1) = -fact_xx;
        ind_count = ind_count+num_of_muscle_front_nodes;
    else
        i(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front;
        j(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front;
        v(ind_count:ind_count+num_of_muscle_front_nodes-1) = 3/2*sigma_i(2,2)/dy;
        ind_count = ind_count+num_of_muscle_front_nodes;
        i(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front;
        j(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front + X;
        v(ind_count:ind_count+num_of_muscle_front_nodes-1) = -2*sigma_i(2,2)/dy;
        ind_count = ind_count+num_of_muscle_front_nodes;
        i(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front;
        j(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front + 2*X;
        v(ind_count:ind_count+num_of_muscle_front_nodes-1) = 1/2*sigma_i(2,2)/dy;
        ind_count = ind_count+num_of_muscle_front_nodes;
    end
    % BCs back
    if (sigma_i(2,2) == 0)
        i(ind_count:ind_count+num_of_muscle_back_nodes-1) = muscle_back;
        j(ind_count:ind_count+num_of_muscle_back_nodes-1) = muscle_back;
        v(ind_count:ind_count+num_of_muscle_back_nodes-1) = w0;
        ind_count = ind_count+num_of_muscle_back_nodes;
        i(ind_count:ind_count+num_of_muscle_back_nodes-1) = muscle_back;
        j(ind_count:ind_count+num_of_muscle_back_nodes-1) = muscle_back + 1;
        v(ind_count:ind_count+num_of_muscle_back_nodes-1) = -fact_xx;
        ind_count = ind_count+num_of_muscle_back_nodes;
        i(ind_count:ind_count+num_of_muscle_back_nodes-1) = muscle_back;
        j(ind_count:ind_count+num_of_muscle_back_nodes-1) = muscle_back - 1;
        v(ind_count:ind_count+num_of_muscle_back_nodes-1) = -fact_xx;
        ind_count = ind_count+num_of_muscle_back_nodes;
    else 
        i(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_back;
        j(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_back;
        v(ind_count:ind_count+num_of_muscle_front_nodes-1) = 3/2*sigma_i(2,2)/dy;
        ind_count = ind_count+num_of_muscle_front_nodes;
        i(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_back;
        j(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_back - X;
        v(ind_count:ind_count+num_of_muscle_front_nodes-1) = -2*sigma_i(2,2)/dy;
        ind_count = ind_count+num_of_muscle_front_nodes;
        i(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_back;
        j(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_back - 2*X;
        v(ind_count:ind_count+num_of_muscle_front_nodes-1) = 1/2*sigma_i(2,2)/dy;
        ind_count = ind_count+num_of_muscle_front_nodes; 
    end

    B{mu_ind} = sparse(i(1:ind_count-1),j(1:ind_count-1),v(1:ind_count-1),dofs,dofs);
end
   

%% Setting Up Matrices C

for mu_ind=1:n_of_mus
    i = zeros(1,dofs*11);
    j = zeros(1,dofs*11);
    v = zeros(1,dofs*11);

    ind_count = 1; 
    
    if (length(p.f_k{mu_ind}) == 1)
        f_r = p.f_k{mu_ind}*ones(num_of_muscle_inner_nodes,1);
    else
        f_r = reshape(p.f_k{mu_ind}(2:X-1,2:Y-1,2:Z-1),[],1)';
    end
%     gg = isfinite(1./f_r);
    
    if (size(p.sigma_i,3)==1)
        sigma_i = p.sigma_i;
    else
        sigma_i = p.sigma_i(:,:,mu_ind); 
    end
    fact_xx = f_r.*sigma_i(1,1)./dx^2;
    fact_yy = f_r.*sigma_i(2,2)./dy^2;
    fact_zz = f_r.*sigma_i(3,3)./dz^2;
    fact_xy = f_r.*(sigma_i(1,2)+sigma_i(2,1))/(4*dx*dy);
    fact_xz = f_r.*(sigma_i(1,3)+sigma_i(3,1))/(4*dx*dz);
    w0 = -2*(fact_xx+fact_yy+fact_zz);
    % diag 
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = w0;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    % u_xx
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + 1;
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_xx;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - 1;
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_xx;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    % u_yy
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + X;
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_yy;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - X;
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_yy;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    % u_zz
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + X*Y;
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_zz;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - X*Y;
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_zz;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    % u_xy
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + (X+1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_xy;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - (X+1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_xy;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + (X-1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_xy;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - (X-1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_xy;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    % u_xz
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + (X*Y+1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_xz;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - (X*Y+1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_xz;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + (X*Y-1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_xz;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
    j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - (X*Y-1);
    v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_xz;
    ind_count = ind_count+num_of_muscle_inner_nodes;
    
    C{mu_ind} = sparse(i(1:ind_count-1),j(1:ind_count-1),v(1:ind_count-1),dofs,dofs);
   
end

%% Setting Up Matrix D

i = zeros(1,dofs*11);
j = zeros(1,dofs*11);
v = zeros(1,dofs*11);

ind_count = 1;

if (size(p.sigma_i,3)==1)
    sigma_i = p.sigma_i;
else
    sigma_i = zeros(3,3);
    for mu_ind=1:n_of_mus
       sigma_i = sigma_i + p.sigma_i(:,:,mu_ind)*p.f_k{mu_ind}; 
    end
end
fact_xx = (p.sigma_e(1,1)+sigma_i(1,1))/dx^2;
fact_yy = (p.sigma_e(2,2)+sigma_i(2,2))/dy^2;
fact_zz = (p.sigma_e(3,3)+sigma_i(3,3))/dz^2;
fact_xy = (p.sigma_e(1,2)+sigma_i(1,2)+p.sigma_e(2,1)+sigma_i(2,1))/(4*dx*dy);
fact_xz = (p.sigma_e(1,3)+sigma_i(1,3)+p.sigma_e(3,1)+sigma_i(3,1))/(4*dx*dz);
w0 = -2*(fact_xx+fact_yy+fact_zz);
% diag 
i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = w0;
ind_count = ind_count+num_of_muscle_inner_nodes;
% u_xx
i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + 1;
v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_xx;
ind_count = ind_count+num_of_muscle_inner_nodes;
i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - 1;
v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_xx;
ind_count = ind_count+num_of_muscle_inner_nodes;
% u_yy
i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + X;
v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_yy;
ind_count = ind_count+num_of_muscle_inner_nodes;
i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - X;
v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_yy;
ind_count = ind_count+num_of_muscle_inner_nodes;
% u_zz
i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + X*Y;
v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_zz;
ind_count = ind_count+num_of_muscle_inner_nodes;
i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - X*Y;
v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_zz;
ind_count = ind_count+num_of_muscle_inner_nodes;
% u_xy
i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + (X+1);
v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_xy;
ind_count = ind_count+num_of_muscle_inner_nodes;
i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - (X+1);
v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_xy;
ind_count = ind_count+num_of_muscle_inner_nodes;
i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + (X-1);
v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_xy;
ind_count = ind_count+num_of_muscle_inner_nodes;
i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - (X-1);
v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_xy;
ind_count = ind_count+num_of_muscle_inner_nodes;
i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + (X*Y+1);
v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_xz;
ind_count = ind_count+num_of_muscle_inner_nodes;
i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - (X*Y+1);
v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = -fact_xz;
ind_count = ind_count+num_of_muscle_inner_nodes;
i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner + (X*Y-1);
v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_xz;
ind_count = ind_count+num_of_muscle_inner_nodes;
i(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner;
j(ind_count:ind_count+num_of_muscle_inner_nodes-1) = muscle_inner - (X*Y-1);
v(ind_count:ind_count+num_of_muscle_inner_nodes-1) = fact_xz;
ind_count = ind_count+num_of_muscle_inner_nodes;
% BCs left
i(ind_count:ind_count+num_of_muscle_left_nodes-1) = muscle_left;
j(ind_count:ind_count+num_of_muscle_left_nodes-1) = muscle_left;
v(ind_count:ind_count+num_of_muscle_left_nodes-1) = 3/2*p.sigma_e(1,1)/dx;
ind_count = ind_count+num_of_muscle_left_nodes;
i(ind_count:ind_count+num_of_muscle_left_nodes-1) = muscle_left;
j(ind_count:ind_count+num_of_muscle_left_nodes-1) = muscle_left + 1;
v(ind_count:ind_count+num_of_muscle_left_nodes-1) = -2*p.sigma_e(1,1)/dx;
ind_count = ind_count+num_of_muscle_left_nodes;
i(ind_count:ind_count+num_of_muscle_left_nodes-1) = muscle_left;
j(ind_count:ind_count+num_of_muscle_left_nodes-1) = muscle_left + 2;
v(ind_count:ind_count+num_of_muscle_left_nodes-1) = 1/2*p.sigma_e(1,1)/dx;
ind_count = ind_count+num_of_muscle_left_nodes;
% BCs right
i(ind_count:ind_count+num_of_muscle_right_nodes-1) = muscle_right;
j(ind_count:ind_count+num_of_muscle_right_nodes-1) = muscle_right;
v(ind_count:ind_count+num_of_muscle_right_nodes-1) = 3/2*p.sigma_e(1,1)/dx;
ind_count = ind_count+num_of_muscle_right_nodes;
i(ind_count:ind_count+num_of_muscle_right_nodes-1) = muscle_right;
j(ind_count:ind_count+num_of_muscle_right_nodes-1) = muscle_right - 1;
v(ind_count:ind_count+num_of_muscle_right_nodes-1) = -2*p.sigma_e(1,1)/dx;
ind_count = ind_count+num_of_muscle_right_nodes;
i(ind_count:ind_count+num_of_muscle_right_nodes-1) = muscle_right;
j(ind_count:ind_count+num_of_muscle_right_nodes-1) = muscle_right - 2;
v(ind_count:ind_count+num_of_muscle_right_nodes-1) = 1/2*p.sigma_e(1,1)/dx;
ind_count = ind_count+num_of_muscle_right_nodes;
% BCs bottom
if (BC == 1) % Neumann BC
    i(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
    j(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
    v(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = 3/2*p.sigma_e(3,3)/dz;
    ind_count = ind_count+num_of_muscle_bottom_nodes;
    i(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
    j(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom + X*Y;
    v(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = -2*p.sigma_e(3,3)/dz;
    ind_count = ind_count+num_of_muscle_bottom_nodes;
    i(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
    j(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom + 2*X*Y;
    v(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = 1/2*p.sigma_e(3,3)/dz;
    ind_count = ind_count+num_of_muscle_bottom_nodes;
elseif (BC == 2) % Mixed Neumann/Dirichlet BC
    dc_points = reshape(nodes_3d(75:81,1:21,1:1),[],1)';
    muscle_bottom = setdiff(muscle_bottom,dc_points);
    num_of_muscle_bottom_nodes = length(muscle_bottom);
    % Apply Neumann BC
    i(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
    j(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
    v(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = 3/2*p.sigma_e(3,3)/dz;
    ind_count = ind_count+num_of_muscle_bottom_nodes;
    i(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
    j(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom + X*Y;
    v(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = -2*p.sigma_e(3,3)/dz;
    ind_count = ind_count+num_of_muscle_bottom_nodes;
    i(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
    j(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom + 2*X*Y;
    v(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = 1/2*p.sigma_e(3,3)/dz;
    ind_count = ind_count+num_of_muscle_bottom_nodes;
    % Apply Dirichlet BC
    muscle_bottom = dc_points;
    num_of_muscle_bottom_nodes = length(muscle_bottom);
    i(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
    j(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
    v(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = 1;
    ind_count = ind_count+num_of_muscle_bottom_nodes;
else % Dirichlet BC
    i(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
    j(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
    v(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = 1;
    ind_count = ind_count+num_of_muscle_bottom_nodes;
end
% i(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom;
% j(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = muscle_bottom + X*Y;
% v(ind_count:ind_count+num_of_muscle_bottom_nodes-1) = -1;
% ind_count = ind_count+num_of_muscle_bottom_nodes;
% BCs top
i(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface;
j(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface;
v(ind_count:ind_count+num_of_muscle_top_nodes-1) = 3/2*p.sigma_e(3,3)/dz;
ind_count = ind_count+num_of_muscle_top_nodes;
i(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface;
j(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface - X*Y;
v(ind_count:ind_count+num_of_muscle_top_nodes-1) = -2*p.sigma_e(3,3)/dz;
ind_count = ind_count+num_of_muscle_top_nodes;
i(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface;
j(ind_count:ind_count+num_of_muscle_top_nodes-1) = muscle_top_interface - 2*X*Y;
v(ind_count:ind_count+num_of_muscle_top_nodes-1) = 1/2*p.sigma_e(3,3)/dz;
ind_count = ind_count+num_of_muscle_top_nodes;
% BCs front
i(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front;
j(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front;
v(ind_count:ind_count+num_of_muscle_front_nodes-1) = 3/2*p.sigma_e(2,2)/dy;
ind_count = ind_count+num_of_muscle_front_nodes;
i(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front;
j(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front + X;
v(ind_count:ind_count+num_of_muscle_front_nodes-1) = -2*p.sigma_e(2,2)/dy;
ind_count = ind_count+num_of_muscle_front_nodes;
i(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front;
j(ind_count:ind_count+num_of_muscle_front_nodes-1) = muscle_front + 2*X;
v(ind_count:ind_count+num_of_muscle_front_nodes-1) = 1/2*p.sigma_e(2,2)/dy;
ind_count = ind_count+num_of_muscle_front_nodes;
% BCs back
i(ind_count:ind_count+num_of_muscle_back_nodes-1) = muscle_back;
j(ind_count:ind_count+num_of_muscle_back_nodes-1) = muscle_back;
v(ind_count:ind_count+num_of_muscle_back_nodes-1) = 3/2*p.sigma_e(2,2)/dy;
ind_count = ind_count+num_of_muscle_back_nodes;
i(ind_count:ind_count+num_of_muscle_back_nodes-1) = muscle_back;
j(ind_count:ind_count+num_of_muscle_back_nodes-1) = muscle_back - X;
v(ind_count:ind_count+num_of_muscle_back_nodes-1) = -2*p.sigma_e(2,2)/dy;
ind_count = ind_count+num_of_muscle_back_nodes;
i(ind_count:ind_count+num_of_muscle_back_nodes-1) = muscle_back;
j(ind_count:ind_count+num_of_muscle_back_nodes-1) = muscle_back - 2*X;
v(ind_count:ind_count+num_of_muscle_back_nodes-1) = 1/2*p.sigma_e(2,2)/dy;
ind_count = ind_count+num_of_muscle_back_nodes;

D = sparse(i(1:ind_count-1),j(1:ind_count-1),v(1:ind_count-1),dofs,dofs);

% for mu_ind=1:n_of_mus 
%     D = D - C{mu_ind}*A{mu_ind}*B{mu_ind};
% end

%% Assemble the overall System Matrix
M1 = [];
M2 = [];
M3 = [];

for mu_idx=1:p.Number_of_MUs
    M1 = blkdiag(M1,A{mu_idx});
    M2 = vertcat(M2,B{mu_idx});
    M3 = horzcat(M3,C{mu_idx});
end
M = [M1,M2;M3,D];

%% Body Region (Fat)

if (include_fat)
    Z_fat = grid.Z_fat;
    dz_fat = grid.dz_fat;
    nodes_fat = (1:X*Y*Z_fat);
    nodes_fat_3d = reshape(nodes_fat,X,Y,Z_fat);
    dofs_fat = X*Y*Z_fat;
    
    fat_bottom_interface = reshape(nodes_fat_3d(2:X-1,2:Y-1,1:1),[],1)';
    fat_top_interface = reshape(nodes_fat_3d(2:X-1,2:Y-1,Z_fat),[],1)';
    fat_left = reshape(nodes_fat_3d(1:1,1:Y,1:Z_fat),[],1)';
    fat_right = reshape(nodes_fat_3d(X:X,1:Y,1:Z_fat),[],1)';
    fat_front = reshape(nodes_fat_3d(2:X-1,1:1,1:Z_fat),[],1)';
    fat_back = reshape(nodes_fat_3d(2:X-1,Y:Y,1:Z_fat),[],1)';
    fat_inner = reshape(nodes_fat_3d(2:X-1,2:Y-1,2:Z_fat-1),[],1)';
    
    num_of_fat_bottom_nodes = length(fat_bottom_interface);
    num_of_fat_top_nodes = length(fat_top_interface);
    num_of_fat_left_nodes = length(fat_left);
    num_of_fat_right_nodes = length(fat_right);
    num_of_fat_front_nodes = length(fat_front);
    num_of_fat_back_nodes = length(fat_back);
    num_of_fat_inner_nodes = length(fat_inner); 
    
    i = zeros(1,length(nodes_fat)*11);
    j = zeros(1,length(nodes_fat)*11);
    v = zeros(1,length(nodes_fat)*11);

    ind_count = 1;
    
    %% generalised laplace in the body region
    fact_xx = p.sigma_o(1,1)/dx^2;
    fact_yy = p.sigma_o(2,2)/dy^2;
    fact_zz = p.sigma_o(3,3)/dz_fat^2;
    w0 = -2*(fact_xx+fact_yy+fact_zz);
    i(ind_count:ind_count+num_of_fat_inner_nodes-1) = fat_inner;
    j(ind_count:ind_count+num_of_fat_inner_nodes-1) = fat_inner;
    v(ind_count:ind_count+num_of_fat_inner_nodes-1) = w0;
    ind_count = ind_count+num_of_fat_inner_nodes;
    % u_xx
    i(ind_count:ind_count+num_of_fat_inner_nodes-1) = fat_inner;
    j(ind_count:ind_count+num_of_fat_inner_nodes-1) = fat_inner + 1;
    v(ind_count:ind_count+num_of_fat_inner_nodes-1) = fact_xx;
    ind_count = ind_count+num_of_fat_inner_nodes;
    i(ind_count:ind_count+num_of_fat_inner_nodes-1) = fat_inner;
    j(ind_count:ind_count+num_of_fat_inner_nodes-1) = fat_inner - 1;
    v(ind_count:ind_count+num_of_fat_inner_nodes-1) = fact_xx;
    ind_count = ind_count+num_of_fat_inner_nodes;
    % u_yy
    i(ind_count:ind_count+num_of_fat_inner_nodes-1) = fat_inner;
    j(ind_count:ind_count+num_of_fat_inner_nodes-1) = fat_inner + X;
    v(ind_count:ind_count+num_of_fat_inner_nodes-1) = fact_yy;
    ind_count = ind_count+num_of_fat_inner_nodes;
    i(ind_count:ind_count+num_of_fat_inner_nodes-1) = fat_inner;
    j(ind_count:ind_count+num_of_fat_inner_nodes-1) = fat_inner - X;
    v(ind_count:ind_count+num_of_fat_inner_nodes-1) = fact_yy;
    ind_count = ind_count+num_of_fat_inner_nodes;
    % u_zz
    i(ind_count:ind_count+num_of_fat_inner_nodes-1) = fat_inner;
    j(ind_count:ind_count+num_of_fat_inner_nodes-1) = fat_inner + X*Y;
    v(ind_count:ind_count+num_of_fat_inner_nodes-1) = fact_zz;
    ind_count = ind_count+num_of_fat_inner_nodes;
    i(ind_count:ind_count+num_of_fat_inner_nodes-1) = fat_inner;
    j(ind_count:ind_count+num_of_fat_inner_nodes-1) = fat_inner - X*Y;
    v(ind_count:ind_count+num_of_fat_inner_nodes-1) = fact_zz;
    ind_count = ind_count+num_of_fat_inner_nodes;
    % BCs left
    i(ind_count:ind_count+num_of_fat_left_nodes-1) = fat_left;
    j(ind_count:ind_count+num_of_fat_left_nodes-1) = fat_left;
    v(ind_count:ind_count+num_of_fat_left_nodes-1) = 3/2*p.sigma_o(1,1)/dx;
    ind_count = ind_count+num_of_fat_left_nodes;
    i(ind_count:ind_count+num_of_fat_left_nodes-1) = fat_left;
    j(ind_count:ind_count+num_of_fat_left_nodes-1) = fat_left + 1;
    v(ind_count:ind_count+num_of_fat_left_nodes-1) = -2*p.sigma_o(1,1)/dx;
    ind_count = ind_count+num_of_fat_left_nodes;
    i(ind_count:ind_count+num_of_fat_left_nodes-1) = fat_left;
    j(ind_count:ind_count+num_of_fat_left_nodes-1) = fat_left + 2;
    v(ind_count:ind_count+num_of_fat_left_nodes-1) = 1/2*p.sigma_o(1,1)/dx;
    ind_count = ind_count+num_of_fat_left_nodes;
    % BCs right
    i(ind_count:ind_count+num_of_fat_right_nodes-1) = fat_right;
    j(ind_count:ind_count+num_of_fat_right_nodes-1) = fat_right;
    v(ind_count:ind_count+num_of_fat_right_nodes-1) = 3/2*p.sigma_o(1,1)/dx;
    ind_count = ind_count+num_of_fat_right_nodes;
    i(ind_count:ind_count+num_of_fat_right_nodes-1) = fat_right;
    j(ind_count:ind_count+num_of_fat_right_nodes-1) = fat_right - 1;
    v(ind_count:ind_count+num_of_fat_right_nodes-1) = -2*p.sigma_o(1,1)/dx;
    ind_count = ind_count+num_of_fat_right_nodes;
    i(ind_count:ind_count+num_of_fat_right_nodes-1) = fat_right;
    j(ind_count:ind_count+num_of_fat_right_nodes-1) = fat_right - 2;
    v(ind_count:ind_count+num_of_fat_right_nodes-1) = 1/2*p.sigma_o(1,1)/dx;
    ind_count = ind_count+num_of_fat_right_nodes;
    % Interface Condition bottom (phi_e = phi_b)
    i(ind_count:ind_count+num_of_fat_bottom_nodes-1) = fat_bottom_interface;
    j(ind_count:ind_count+num_of_fat_bottom_nodes-1) = fat_bottom_interface;
    v(ind_count:ind_count+num_of_fat_bottom_nodes-1) = -1;
    ind_count = ind_count+num_of_fat_bottom_nodes;
    % BCs top/or interface conition (with skin)
    i(ind_count:ind_count+num_of_fat_top_nodes-1) = fat_top_interface;
    j(ind_count:ind_count+num_of_fat_top_nodes-1) = fat_top_interface;
    v(ind_count:ind_count+num_of_fat_top_nodes-1) = 3/2*p.sigma_o(3,3)/dz_fat;
    ind_count = ind_count+num_of_fat_top_nodes;
    i(ind_count:ind_count+num_of_fat_top_nodes-1) = fat_top_interface;
    j(ind_count:ind_count+num_of_fat_top_nodes-1) = fat_top_interface - X*Y;
    v(ind_count:ind_count+num_of_fat_top_nodes-1) = -2*p.sigma_o(3,3)/dz_fat;
    ind_count = ind_count+num_of_fat_top_nodes;
    i(ind_count:ind_count+num_of_fat_top_nodes-1) = fat_top_interface;
    j(ind_count:ind_count+num_of_fat_top_nodes-1) = fat_top_interface - 2*X*Y;
    v(ind_count:ind_count+num_of_fat_top_nodes-1) = 1/2*p.sigma_o(3,3)/dz_fat;
    ind_count = ind_count+num_of_fat_top_nodes;
    % BCs front
    i(ind_count:ind_count+num_of_fat_front_nodes-1) = fat_front;
    j(ind_count:ind_count+num_of_fat_front_nodes-1) = fat_front;
    v(ind_count:ind_count+num_of_fat_front_nodes-1) = 3/2*p.sigma_o(2,2)/dy;
    ind_count = ind_count+num_of_fat_front_nodes;
    i(ind_count:ind_count+num_of_fat_front_nodes-1) = fat_front;
    j(ind_count:ind_count+num_of_fat_front_nodes-1) = fat_front + X;
    v(ind_count:ind_count+num_of_fat_front_nodes-1) = -2*p.sigma_o(2,2)/dy;
    ind_count = ind_count+num_of_fat_front_nodes;
    i(ind_count:ind_count+num_of_fat_front_nodes-1) = fat_front;
    j(ind_count:ind_count+num_of_fat_front_nodes-1) = fat_front + 2*X;
    v(ind_count:ind_count+num_of_fat_front_nodes-1) = 1/2*p.sigma_o(2,2)/dy;
    ind_count = ind_count+num_of_fat_front_nodes;
    % BCs back
    i(ind_count:ind_count+num_of_fat_back_nodes-1) = fat_back;
    j(ind_count:ind_count+num_of_fat_back_nodes-1) = fat_back;
    v(ind_count:ind_count+num_of_fat_back_nodes-1) = 3/2*p.sigma_o(2,2)/dy;
    ind_count = ind_count+num_of_fat_back_nodes;
    i(ind_count:ind_count+num_of_fat_back_nodes-1) = fat_back;
    j(ind_count:ind_count+num_of_fat_back_nodes-1) = fat_back - X;
    v(ind_count:ind_count+num_of_fat_back_nodes-1) = -2*p.sigma_o(2,2)/dy;
    ind_count = ind_count+num_of_fat_back_nodes;
    i(ind_count:ind_count+num_of_fat_back_nodes-1) = fat_back;
    j(ind_count:ind_count+num_of_fat_back_nodes-1) = fat_back - 2*X;
    v(ind_count:ind_count+num_of_fat_back_nodes-1) = 1/2*p.sigma_o(2,2)/dy;
    ind_count = ind_count+num_of_fat_back_nodes;
    
    F{1} = sparse(i(1:ind_count-1),j(1:ind_count-1),v(1:ind_count-1),dofs_fat,dofs_fat);
    
    %% Coupling Matrices
    F{2} = sparse(fat_bottom_interface,muscle_top_interface,1,dofs_fat,dofs);
    F{3} = sparse(muscle_top_interface,fat_bottom_interface,3/2*p.sigma_o(3,3)/dz_fat,dofs,dofs_fat) + ...
        sparse(muscle_top_interface,fat_bottom_interface + X*Y ,-2*p.sigma_o(3,3)/dz_fat,dofs,dofs_fat) + ...
        sparse(muscle_top_interface,fat_bottom_interface + 2*X*Y,1/2*p.sigma_o(3,3)/dz_fat,dofs,dofs_fat);
    
    %% Assemble the overall System Matrix
    M = horzcat(M,vertcat(sparse(n_of_mus*dofs,dofs_fat),F{3}));
    M = vertcat(M,horzcat(sparse(dofs_fat,n_of_mus*dofs),F{2},F{1}));     
    
end


%% Skin Region
if (include_skin)
    Z_skin = grid.Z_skin;
    dz_skin = grid.dz_skin;
    nodes_skin = (1:X*Y*Z_skin);
    nodes_skin_3d = reshape(nodes_skin,X,Y,Z_skin);
    dofs_skin = X*Y*Z_skin;
    
    skin_bottom_interface = reshape(nodes_skin_3d(2:X-1,2:Y-1,1:1),[],1)';
    skin_top = reshape(nodes_skin_3d(2:X-1,2:Y-1,Z_skin),[],1)';
    skin_left = reshape(nodes_skin_3d(1:1,1:Y,1:Z_skin),[],1)';
    skin_right = reshape(nodes_skin_3d(X:X,1:Y,1:Z_skin),[],1)';
    skin_front = reshape(nodes_skin_3d(2:X-1,1:1,1:Z_skin),[],1)';
    skin_back = reshape(nodes_skin_3d(2:X-1,Y:Y,1:Z_skin),[],1)';
    skin_inner = reshape(nodes_skin_3d(2:X-1,2:Y-1,2:Z_skin-1),[],1)';
    
    num_of_skin_bottom_nodes = length(skin_bottom_interface);
    num_of_skin_top_nodes = length(skin_top);
    num_of_skin_left_nodes = length(skin_left);
    num_of_skin_right_nodes = length(skin_right);
    num_of_skin_front_nodes = length(skin_front);
    num_of_skin_back_nodes = length(skin_back);
    num_of_skin_inner_nodes = length(skin_inner); 
    
    i = zeros(1,length(nodes_skin)*11);
    j = zeros(1,length(nodes_skin)*11);
    v = zeros(1,length(nodes_skin)*11);

    ind_count = 1;
    
    %% generalised laplace in the body region
    fact_xx = p.sigma_s(1,1)/dx^2;
    fact_yy = p.sigma_s(2,2)/dy^2;
    fact_zz = p.sigma_s(3,3)/dz_skin^2;
    w0 = -2*(fact_xx+fact_yy+fact_zz);
    i(ind_count:ind_count+num_of_skin_inner_nodes-1) = skin_inner;
    j(ind_count:ind_count+num_of_skin_inner_nodes-1) = skin_inner;
    v(ind_count:ind_count+num_of_skin_inner_nodes-1) = w0;
    ind_count = ind_count+num_of_skin_inner_nodes;
    % u_xx
    i(ind_count:ind_count+num_of_skin_inner_nodes-1) = skin_inner;
    j(ind_count:ind_count+num_of_skin_inner_nodes-1) = skin_inner + 1;
    v(ind_count:ind_count+num_of_skin_inner_nodes-1) = fact_xx;
    ind_count = ind_count+num_of_skin_inner_nodes;
    i(ind_count:ind_count+num_of_skin_inner_nodes-1) = skin_inner;
    j(ind_count:ind_count+num_of_skin_inner_nodes-1) = skin_inner - 1;
    v(ind_count:ind_count+num_of_skin_inner_nodes-1) = fact_xx;
    ind_count = ind_count+num_of_skin_inner_nodes;
    % u_yy
    i(ind_count:ind_count+num_of_skin_inner_nodes-1) = skin_inner;
    j(ind_count:ind_count+num_of_skin_inner_nodes-1) = skin_inner + X;
    v(ind_count:ind_count+num_of_skin_inner_nodes-1) = fact_yy;
    ind_count = ind_count+num_of_skin_inner_nodes;
    i(ind_count:ind_count+num_of_skin_inner_nodes-1) = skin_inner;
    j(ind_count:ind_count+num_of_skin_inner_nodes-1) = skin_inner - X;
    v(ind_count:ind_count+num_of_skin_inner_nodes-1) = fact_yy;
    ind_count = ind_count+num_of_skin_inner_nodes;
    % u_zz
    i(ind_count:ind_count+num_of_skin_inner_nodes-1) = skin_inner;
    j(ind_count:ind_count+num_of_skin_inner_nodes-1) = skin_inner + X*Y;
    v(ind_count:ind_count+num_of_skin_inner_nodes-1) = fact_zz;
    ind_count = ind_count+num_of_skin_inner_nodes;
    i(ind_count:ind_count+num_of_skin_inner_nodes-1) = skin_inner;
    j(ind_count:ind_count+num_of_skin_inner_nodes-1) = skin_inner - X*Y;
    v(ind_count:ind_count+num_of_skin_inner_nodes-1) = fact_zz;
    ind_count = ind_count+num_of_skin_inner_nodes;
    % BCs left
    i(ind_count:ind_count+num_of_skin_left_nodes-1) = skin_left;
    j(ind_count:ind_count+num_of_skin_left_nodes-1) = skin_left;
    v(ind_count:ind_count+num_of_skin_left_nodes-1) = 3/2*p.sigma_s(1,1)/dx;
    ind_count = ind_count+num_of_skin_left_nodes;
    i(ind_count:ind_count+num_of_skin_left_nodes-1) = skin_left;
    j(ind_count:ind_count+num_of_skin_left_nodes-1) = skin_left + 1;
    v(ind_count:ind_count+num_of_skin_left_nodes-1) = -2*p.sigma_s(1,1)/dx;
    ind_count = ind_count+num_of_skin_left_nodes;
    i(ind_count:ind_count+num_of_skin_left_nodes-1) = skin_left;
    j(ind_count:ind_count+num_of_skin_left_nodes-1) = skin_left + 2;
    v(ind_count:ind_count+num_of_skin_left_nodes-1) = 1/2*p.sigma_s(1,1)/dx;
    ind_count = ind_count+num_of_skin_left_nodes;
    % BCs right
    i(ind_count:ind_count+num_of_skin_right_nodes-1) = skin_right;
    j(ind_count:ind_count+num_of_skin_right_nodes-1) = skin_right;
    v(ind_count:ind_count+num_of_skin_right_nodes-1) = 3/2*p.sigma_s(1,1)/dx;
    ind_count = ind_count+num_of_skin_right_nodes;
    i(ind_count:ind_count+num_of_skin_right_nodes-1) = skin_right;
    j(ind_count:ind_count+num_of_skin_right_nodes-1) = skin_right - 1;
    v(ind_count:ind_count+num_of_skin_right_nodes-1) = -2*p.sigma_s(1,1)/dx;
    ind_count = ind_count+num_of_skin_right_nodes;
    i(ind_count:ind_count+num_of_skin_right_nodes-1) = skin_right;
    j(ind_count:ind_count+num_of_skin_right_nodes-1) = skin_right - 2;
    v(ind_count:ind_count+num_of_skin_right_nodes-1) = 1/2*p.sigma_s(1,1)/dx;
    ind_count = ind_count+num_of_skin_right_nodes;
    % Interface Condition bottom (phi_e = phi_b)
    i(ind_count:ind_count+num_of_skin_bottom_nodes-1) = skin_bottom_interface;
    j(ind_count:ind_count+num_of_skin_bottom_nodes-1) = skin_bottom_interface;
    v(ind_count:ind_count+num_of_skin_bottom_nodes-1) = -1;
    ind_count = ind_count+num_of_skin_bottom_nodes;
    % BCs top
    i(ind_count:ind_count+num_of_skin_top_nodes-1) = skin_top;
    j(ind_count:ind_count+num_of_skin_top_nodes-1) = skin_top;
    v(ind_count:ind_count+num_of_skin_top_nodes-1) = 3/2*p.sigma_s(3,3)/dz_skin;
    ind_count = ind_count+num_of_skin_top_nodes;
    i(ind_count:ind_count+num_of_skin_top_nodes-1) = skin_top;
    j(ind_count:ind_count+num_of_skin_top_nodes-1) = skin_top - X*Y;
    v(ind_count:ind_count+num_of_skin_top_nodes-1) = -2*p.sigma_s(3,3)/dz_skin;
    ind_count = ind_count+num_of_skin_top_nodes;
    i(ind_count:ind_count+num_of_skin_top_nodes-1) = skin_top;
    j(ind_count:ind_count+num_of_skin_top_nodes-1) = skin_top - 2*X*Y;
    v(ind_count:ind_count+num_of_skin_top_nodes-1) = 1/2*p.sigma_s(3,3)/dz_skin;
    ind_count = ind_count+num_of_skin_top_nodes;
    % BCs front
    i(ind_count:ind_count+num_of_skin_front_nodes-1) = skin_front;
    j(ind_count:ind_count+num_of_skin_front_nodes-1) = skin_front;
    v(ind_count:ind_count+num_of_skin_front_nodes-1) = 3/2*p.sigma_s(2,2)/dy;
    ind_count = ind_count+num_of_skin_front_nodes;
    i(ind_count:ind_count+num_of_skin_front_nodes-1) = skin_front;
    j(ind_count:ind_count+num_of_skin_front_nodes-1) = skin_front + X;
    v(ind_count:ind_count+num_of_skin_front_nodes-1) = -2*p.sigma_s(2,2)/dy;
    ind_count = ind_count+num_of_skin_front_nodes;
    i(ind_count:ind_count+num_of_skin_front_nodes-1) = skin_front;
    j(ind_count:ind_count+num_of_skin_front_nodes-1) = skin_front + 2*X;
    v(ind_count:ind_count+num_of_skin_front_nodes-1) = 1/2*p.sigma_s(2,2)/dy;
    ind_count = ind_count+num_of_skin_front_nodes;
    % BCs back
    i(ind_count:ind_count+num_of_skin_back_nodes-1) = skin_back;
    j(ind_count:ind_count+num_of_skin_back_nodes-1) = skin_back;
    v(ind_count:ind_count+num_of_skin_back_nodes-1) = 3/2*p.sigma_s(2,2)/dy;
    ind_count = ind_count+num_of_skin_back_nodes;
    i(ind_count:ind_count+num_of_skin_back_nodes-1) = skin_back;
    j(ind_count:ind_count+num_of_skin_back_nodes-1) = skin_back - X;
    v(ind_count:ind_count+num_of_skin_back_nodes-1) = -2*p.sigma_s(2,2)/dy;
    ind_count = ind_count+num_of_skin_back_nodes;
    i(ind_count:ind_count+num_of_skin_back_nodes-1) = skin_back;
    j(ind_count:ind_count+num_of_skin_back_nodes-1) = skin_back - 2*X;
    v(ind_count:ind_count+num_of_skin_back_nodes-1) = 1/2*p.sigma_s(2,2)/dy;
    ind_count = ind_count+num_of_skin_back_nodes;
    
    S{1} = sparse(i(1:ind_count-1),j(1:ind_count-1),v(1:ind_count-1),dofs_skin,dofs_skin);
    
    %% Coupling Matrices
    S{2} = sparse(skin_bottom_interface,fat_top_interface,1,dofs_skin,dofs_fat);
    S{3} = sparse(fat_top_interface,skin_bottom_interface,3/2*p.sigma_s(3,3)/dz_skin,dofs_fat,dofs_skin) + ...
        sparse(fat_top_interface,skin_bottom_interface + X*Y ,-2*p.sigma_s(3,3)/dz_skin,dofs_fat,dofs_skin) + ...
        sparse(fat_top_interface,skin_bottom_interface + 2*X*Y,1/2*p.sigma_s(3,3)/dz_skin,dofs_fat,dofs_skin);
    %% Assemble the overall System Matrix
    M = horzcat(M,vertcat(sparse((n_of_mus+1)*dofs,dofs_skin),S{3}));
    M = vertcat(M,horzcat(sparse(dofs_skin,(n_of_mus+1)*dofs),S{2},S{1})); 
end


end

