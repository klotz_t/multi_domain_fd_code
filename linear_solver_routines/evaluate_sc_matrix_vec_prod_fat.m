function [ y ] = evaluate_sc_matrix_vec_prod_fat(x)
% Evaluates the matrix vector product of the Schur complement of the system
% matrix (for the case that a stack of muscle and fat tissue is considered) 
% and an arbitray vector x. 
global  B C D l u F
x1 = x(1:length(B{1}));
x2 = x(length(B{1})+1:end);
gg = zeros(length(x1),length(B));
for i=1:length(B)
    gg(:,i) = C{i}*(u{i}\(l{i}\(B{i}*x1)));
end
y1 = D*x1 - sum(gg,2) + F{3}*x2;
y2 = F{2}*x1 + F{1}*x2;
y = [y1;y2];
end

