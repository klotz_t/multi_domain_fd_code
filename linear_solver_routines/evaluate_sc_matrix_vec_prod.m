function [ y ] = evaluate_sc_matrix_vec_prod(x)
% Evaluates the matrix vector product of the Schur complement of the system
% matrix (for the case that only muscle tissue is considered) and an 
% arbitray vector x. 
global  B C D l u 
gg = zeros(length(x),length(B));
for i=1:length(B)
    gg(:,i) = C{i}*(u{i}\(l{i}\(B{i}*x)));
end
%y = D*x1 - sum(cell2mat(gg),2);
y = D*x - sum(gg,2);
end

