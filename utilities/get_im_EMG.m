function [] = get_im_EMG(xy,filepath,diff,name_id)
%% Extract the intramuscular EMG signal from Multi-Domain Model
% Arguments: - xy: x and y position of electrode (parallel to the z-axes)
%            - filepath: Folder, containing the results   
% output: extracellular potential in a line (x,y) from bottom to top.

%% Check number of input arguments
if nargin == 2
    diff = 0;
    name_id = 0;
elseif nargin == 3
    name_id = 0;
end
%% load Grid Data and Parameters
load (sprintf('%s/simple_example_info.mat',filepath)); 

n = grid.X*grid.Y*grid.Z;
number_of_mus = p.Number_of_MUs;
% dt = delta_t;

xyz = [xy(1), xy(2), 0];

node = coordinate2node(xyz,[grid.X,grid.Y,grid.Z],[input.length,input.width,input.height]);

%%
num_of_dt = round(input.time_end / input.dt)/input.output_frequency;
imEMG = NaN(grid.Z, num_of_dt);

start_ind = n*number_of_mus + node;

for i=1:num_of_dt %length(times)
    load (sprintf('%s/simple_example_%d.mat',filepath,i-1));
    im_pot = xx(start_ind:grid.X*grid.Y:start_ind+grid.X*grid.Y*(grid.Z-1));
    % Convert data from Vector to matrix
    if diff == 0
        imEMG(:,i) = im_pot;
    elseif diff == 1
	im_pot2 = xx(start_ind+1:grid.X*grid.Y:start_ind+1+grid.X*grid.Y*(grid.Z-1));    
        imEMG(:,i) = im_pot2 - im_pot;
    else
        disp('invalid diff configuration')
        return
    end	
end

save(sprintf('%s/imEMG_%d.mat',filepath,name_id),'imEMG','xy')

end

function [n] = coordinate2node(coordinate,grid_dim,N)
% Coordinate: [x,y,z] coordinate of Point 
% grid_dim: [length,width,height] of muscle sample
% N: [nx,ny,nz] discretization points in muscle 


dx = grid_dim(1)/(N(1)-1);
dy = grid_dim(2)/(N(2)-1);
dz = grid_dim(3)/(N(3)-1);


if (coordinate(1)<=0)
    xn = 0;
elseif (coordinate(1)>=grid_dim(1))
    xn = grid_dim(1);
else
    xn = floor(coordinate(1)/dx);
end
    
if (coordinate(2)<=0)
    yn = 0;
elseif (coordinate(2)>=grid_dim(2))
    yn = (N(2)-1)*N(1);
else
    yn = floor(coordinate(2)/dy)*N(1);
end

if (coordinate(3)<=0)
    zn = 0;
elseif (coordinate(3)>=grid_dim(3))
    zn = (N(3)-1)*N(1)*N(2);
else
    zn = floor(coordinate(3)/dz)*N(1)*N(2);
end

n = 1 + xn + yn + zn;
end
