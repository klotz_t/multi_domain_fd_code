function plotdisp(data,normal, dist,newfig,color,strict_order)
%plotdisp(data,normal, dist,newfig,color,strict_order)
% data = data to plot
% normal = normalize each data channel
% dist = distance of plots
% newfig = 1 create a new figure, 0 do not create a new figure
% color = [r g b] specify RGB components
% strict_order = maintain the order of the channels as they arrive,
% otherwise check if transposition is necessary

if isa(data,'int')
    data = double(data);
end

switch nargin
    case 0
        help plotdisp
        return
    case 1
        normal = 1;
        dist = 1;
        newfig = 1;
        strict_order = 0;
        color = [];
    case 2
        dist = max(data(:));
        newfig = 1;
        strict_order = 0;
        color = [];
    case 3
        newfig = 1;
        strict_order = 0;
        color = [];
    case 4
        strict_order = 0;
        color = [];
    case 5
        strict_order = 0;
end
%==========================================================================
    if iscell(data)
        data = EMG_matrix_cell_nocell(data);
    end 
%==========================================================================      
if normal == 1
    data = normalizza (data,[],1);
    dist = 1;
end


    if newfig ==1
    figure
    end

if size(size(data),2)==3 %it is a 3D matrix (rows, columns samples)
        for ncol = 1:size(data,2)
        eval([ 'Ax(',int2str(ncol),')=subplot(1,size(data,2), ncol);']);
        temp = squeeze(data(:,ncol,:)); 
            if isempty(color)
            plotdisp(temp,normal,dist,0), box('off');
            set(gca,'ytick',[])
            else
            plotdisp(temp,normal,dist,0, color), box('off');
            set(gca,'ytick',[])
            end
        end

        linkaxes(Ax, 'xy');
else
            if strict_order ==0
                if size(data,1)>size(data,2) %transpose if appropriate
                data = data';
                end
            end
%__________________________________________________________________________ 
            hold on
            
            if isempty(color)
            for n = 1:size(data,1)              
                plot(data(n,:)+n*dist);
                set(gca,'ytick',[])
            end
            else
            for n = 1:size(data,1)              
                plot(data(n,:)+n*dist, 'Color', color);
                set(gca,'ytick',[])
            end
            end
end


% 
% -- 
% Leonardo Gizzi
% M.S. Bioeng., Ph.D.
% 
% Institute of Applied Mechanics (Civil Engineering), Chair II
% Continuum Biomechanics and Mechanobiology Research Group
% University of Stuttgart
% 
% Pfaffenwaldring 5a
% 70569 Stuttgart
% Germany
% 
% Phone   : +49 (0)711 / 685-60044
% E-Mail  : leonardo.gizzi@mechbau.uni-stuttgart.de
    
