function  EMG_plot_matrix_heatmap(EMG,animate,frame, interpolate, differential,surfaceplot)
%EMG_plot_matrix_heatmap(EMG,animate,frame, interpolate, differential,surfaceplot)
%plots the map of a EMG matrix once its channels have been reordered.
%works natively with 3D arrays (rows, columns, samples) but can accept also
%cells {rows, columns}
%animate = 1 create an animation with the heatmap
%frame = starting and ending frames, scalar value implies the animation to
%be played until the end
%interpolate =  number of interpolation passages, normally 2

switch nargin
    case 0
        help EMG_plot_matrix_heatmap
        return
    case 1
        animate = 1;
        frame = [1 length(EMG)];
        interpolate = 4; 
        differential = 2;
        surfaceplot = 0;
    case 2
        frame = [1 length(EMG)];
        interpolate = 4; 
        differential = 0;
        surfaceplot = 0;
    case 3
        interpolate = 4; 
        differential = 0;
        surfaceplot = 0;
    case 4
        differential = 0;
        surfaceplot = 0;
    case 5
        surfaceplot = 0;
end


if iscell(EMG)
    EMG = EMG_matrix_cell_nocell(EMG);
end


% switch nargin
%     case 1
%         animate = 1;
%         frame = [1 length(EMG)];
%         interpolate = 4; 
%         differential = 0;
%         surfaceplot = 0;
% end

switch differential 
    case 0
        
    case 1
    EMG = diff(EMG);
    case 2
        EMG = diff(EMG,1,2);
    otherwise 
        fprintf('sbagliato! diffenrential can only be 1 or 2 ...doing nothing')
        pause
end


if size(frame,1) == 1 %end frame not specified
    frame(2) = length(EMG);
end

if animate == 0
    image(interp2(EMG(:,:,frame(1))));
else %animate    
    gg = squeeze(sum(sum(abs(EMG),1,'omitnan'))); %EMG channel to be displayed alone
    m1 = max(gg); m2 = -max(gg); %ranges of the line to mark frame number in the EMG single channel plot
    subplot(2,1,1), P = plot(gg); L = line([0 0], [m1,m2]); %plot the EMG channel and one line to indicate the current frame
    if surfaceplot == 0
        clim = [min(EMG(:)),max(EMG(:))];tEMG = interp2(EMG(:,:,1),interpolate);
	subplot(2,1,2), IM = imagesc(1:size(tEMG,1),1:size(tEMG,2),tEMG, clim); pause(0.01); %prepare imagesc plot for heatmap
        for n  = frame(1):frame(2)       
        L.XData = [n n]; %update line data
        IM.CData = (interp2(EMG(:,:,n),interpolate)); pause(0.05); %update color data
        end
    else
%        limits= [min(min(EMG)),max(max(EMG))]; %Z limits for surf plot
        EMG = EMG/max(max(max(EMG))); %normalize signal
        subplot(2,1,2), SU = surf(interp2(EMG(:,:,1),interpolate),'EdgeColor','none'); zlim([-1 1]);  pause(0.01); %prepare surf plot
        colormap jet, caxis([-0.5 0.5])
%         pause

        
        for n  = frame(1):frame(2)
        L.XData = [n n]; %update line data
        SU.CData = interp2(EMG(:,:,n),interpolate); %update line data
        SU.ZData = interp2(EMG(:,:,n),interpolate);
        pause(0.01);
        file_name = sprintf('movies/texfiles/simple_example_%d.tex', n);
%         matlab2tikz(file_name);
        
        end
    end
        
end
        
% EMG_array_to_matrix