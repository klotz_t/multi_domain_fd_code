function [] = get_EMG(filepath,ndt)
%% Extract the surface EMG signal from Multi-Domain Model
% Arguments: Path to the folder containing the results 
%% load Grid Data and Parameters
load (sprintf('%s/simple_example_info.mat',filepath)); 

% n = grid.X*grid.Y*grid.Z;
number_of_mus = p.Number_of_MUs;
% dt = delta_t;

%%
switch nargin
    case 2
        num_of_dt = ndt;
    otherwise
        num_of_dt = round(input.time_end / input.dt)/input.output_frequency;
 end
%num_of_dt = round(input.time_end / input.dt)/input.output_frequency;
% EMG = NaN(grid.X,grid.Y, num_of_dt);
% if (input.fat)
%     start_ind = n*(number_of_mus+1) + grid.X*grid.Y*(grid.Z_fat-1) + 1;
% else
%     start_ind = n*number_of_mus + n-grid.X*grid.Y + 1;
% end
load (sprintf('%s/simple_example_0.mat',filepath));
if input.output_spec == 0 || input.output_spec == 1
	start_ind = length(xx) - grid.X*grid.Y + 1;
end

for i=1:num_of_dt %length(times)
   load(sprintf('%s/simple_example_%d.mat',filepath,i-1));
   if input.output_spec == 2
   	EMG(:,:,i) = emg;
   else
   	surface_pot = xx(start_ind:1:length(xx));
   	% Convert data from Vector to matrix
   	EMG(:,:,i) = reshape(surface_pot,[grid.X, grid.Y]); 
   end   
end

% save('results/sEMG.mat','EMG')
% save('sEMG.mat','EMG')
save(sprintf('%s/sEMG.mat',filepath),'EMG')
end

