%-------------------------------------------------%
%                                                 %
% ----------- The MultiDomain equations --------- %
%                                                 %
% ---- Institute for Modelling and Simulation --- %
% ---------- of Biomechanical Systems ----------- %
%                                                 % 
% ----------- University of Stuttgart ----------- % 
%                                                 %  
%                   Thomas Klotz                  %
%-------------------------------------------------%
%%
function [] = MultiDomain_FEM(num_of_mus,t_end,phi_mesh,...
    vm_mesh,IPP,solvers,output,bc_phi,bc_vm,cell_model)
%
fprintf('\n Start Programm \n')
%% Time Settings
fprintf('\n Read in time settings \n')
% time step size for PDE
dt = solvers.dt; % [ms]
% stop time
% number of time steps for dynamic PDE solver (k)
num_of_dt = round(t_end / dt);
%% Build the waek forms of the spatially discretized multi-domain equations
fprintf('\n Build the spatially discretized multi-domain equations \n')
[M11,K11,K12,K21,K22] = assemble_FE_multi_domain_matrices(phi_mesh,vm_mesh,IPP,num_of_mus);
%% Apply additional boundary condition (default / natural BCs are zero Neumann BCs)
fprintf('\n Apply boundary conditions \n')
% Dirichlet BCs on the extracellular potential
if (isfield(bc_phi,'dirichlet'))
    [K21,K22] = apply_Dirichlet_BC_to_phi_e(K21,K22,bc_phi.dirichlet.phi_e_dofs);
end
% Dirichlet BCs on the transmembrane potential
for mu_idx=1:num_of_mus
    if (isfield(bc_vm(mu_idx),'dirichlet'))
        [M11{mu_idx},K11{mu_idx},K12{mu_idx}] = apply_Dirichlet_BC_to_vm(M11{mu_idx},K11{mu_idx},K12{mu_idx},bc_vm(mu_idx).dirichlet.points);
    end
end
% non-zero Neumann BCs on the extracellular potential
if (isfield(bc_phi,'neumann'))
    C = apply_Neumann_BC_to_phi_e(phi_mesh,bc_phi.neumann.elements,...
        bc_phi.neumann.surface_vec,sum([vm_mesh(:).dofs]));
end
%% Apply a temporal discretization scheme
fprintf('\n Build time discretized matrix system \n')
[A,B] = get_time_discretisized_multi_domain_equations(M11,K11,K12,K21,K22,dt,solvers.fem_time_discretization);
% Incomplete LU-Factorisation of the System Matrix A (preconditioner for gmres solver) 
if solvers.linear_solver.type == 1
    [L,U] = ilu(A,solvers.linear_solver.options);
end
%% Set up the membrane Models
fprintf('\n Set up the membrane models \n')
% Initialise constants and state variables for cellular model
p = cell(num_of_mus,1);
for mu_idx=1:num_of_mus
    p{mu_idx} = initConsts(cell_model(mu_idx).ID);
    if cell_model(mu_idx).ID == 1
        if (isfield(input.cell_model_specs,'fatigue'))
            p{mu_idx}(:,57) = input.cell_model_specs.fatigue;
        end
    end 
end
% Initalise cell models at each point and for each MU
y = cell(num_of_mus,1);
Cm = cell(num_of_mus,1);
for mu_idx=1:num_of_mus
    y0 = initStates(cell_model(mu_idx).ID);
    for dof_idx=1:vm_mesh(mu_idx).dofs
        y{mu_idx}(dof_idx,:) = y0;
        Cm{mu_idx}(dof_idx,:) = cell_model(mu_idx).Cm;
    end
end
%% Initalise the state vector
fprintf('\n Initalise the state vector \n')
xx = zeros(sum([vm_mesh(:).dofs])+phi_mesh.dofs,1);
%% Set up the stimulus at the neuromuscular juntions
% Prescribe the firing times from the motor nerves
fprintf('\n Prescribe the stimuli of the membrane models (neuromuscular junctions) \n')
I_stim = cell(num_of_mus,1);
for mu_idx=1:num_of_mus
    I_stim{mu_idx} = sparse(vm_mesh(mu_idx).dofs,num_of_dt);
    if  (isfield(bc_vm,'nmj'))
        for jun_ind=1:length(bc_vm(mu_idx).nmj.points)
            dof = bc_vm(mu_idx).nmj.points(jun_ind);
            if(~isempty(bc_vm(mu_idx).nmj.times))
                I_stim{mu_idx}(dof,:) = stimulus(bc_vm(mu_idx).nmj.times,...
                    bc_vm(mu_idx).nmj.amplitude,bc_vm(mu_idx).nmj.dt,dt,num_of_dt); 
            end
        end
    end
end
%% Set up ODE solvers
fprintf('\n Set up the ODE solver \n')
ode_solver = solvers.ode_solver.type;
if ode_solver == 0 
    dt_ode = solvers.ode_solver.options;
else
    options = solvers.ode_solver.options;
end    
%% Output
fprintf('\n Set up output options \n')
mkdir(output.filepath)
file_name = sprintf('%s/%s_info.mat', output.filepath,output.filename);
save(file_name,'num_of_mus','phi_mesh','vm_mesh','IPP','solvers','output',...
    'bc_phi','bc_vm','cell_model');
output_count = 0;
file_name = sprintf('%s/%s_%d.mat', output.filepath,output.filename,output_count);
switch output.output_spec  
    case 0 % Full state vector
        t = 0;
        save(file_name,'xx','y','t');%,'rhs')
    case 1 % Only the electrical potential fields
        save(file_name,'xx');
end
%%
fprintf('\n Starting to solve the model \n')
for time = 1:num_of_dt-1
    %% Print simulation progress
    fprintf('step #   : %d \t', time);
    fprintf('time [ms]: %f \n', (time-1)*dt);    
    %% Solve the membrane models (reaction terms of the multi-domain model)
    %  
    if solvers.splitting_scheme == 1
        tspan = [(time-1)*dt, (time-0.5)*dt];
    else
        tspan = [(time-1)*dt, time*dt];
    end    
    tic
    switch ode_solver
        case 0
            parfor mu_idx=1:num_of_mus
                y{mu_idx} = heun_method(y{mu_idx},p{mu_idx},I_stim{mu_idx}(:,time),...
                    Cm{mu_idx},cell_model(mu_idx).ID,tspan,dt_ode);
            end
        case 1
            for mu_idx=1:num_of_mus
                tmp = zeros(vm_mesh(mu_idx).dofs,length(y{mu_idx}(1,:)));
                parfor i = 1:vm_mesh(mu_idx).dofs
                    LAST_STATES = y{mu_idx}(i,:);
                    % Solve model with ODE solver
                    [~, STATES] = ode15s(@(TIME, STATES)computeRatesAndAlgebraic(...
                        TIME, STATES, p{mu_idx},I_stim{mu_idx}(i,time),Cm{mu_idx}(i),cell_model(mu_idx).ID),... 
                        tspan, LAST_STATES, options);
                    % update ALL_STATES
%                     ALL_STATES{mu_ind}(i,:) = STATES(end,:);
                    tmp(i,:) = STATES(end,:);
                end
                y{mu_idx} = tmp;
            end
    end
    toc
    %  Update Vm at the intermediate timestep of the splitting scheme.
    idx = 0;
    for mu_idx=1:num_of_mus
        xx(idx+1:idx+vm_mesh(mu_idx).dofs) = y{mu_idx}(:,1);
        idx = idx + vm_mesh(mu_idx).dofs;
    end 
    %% Set up the linear system representing the PDE part of the multi-domain equations
    %  Calculate the right hand side vector
    b = B*xx;
    %  Add (non-zero) Dirchichlet BCs to the RHS vector.
    if (isfield(bc_phi,'dirichlet'))
        if (isfield(bc_phi.dirichlet,'myfunction'))
            for i = 1:length(bc_phi.dirichlet.phi_e_dofs)
                node_idx =  bc_phi.dirichlet.phi_e_dofs(i);   
                b(sum([vm_mesh(:).dofs]) + node_idx) = bc_phi.dirichlet.myfunction(node_idx,time); 
            end
        end
    end    
    %  Add (non-zero) Dirchichlet BCs to the RHS vector.
    if (isfield(bc_vm,'dirichlet'))
        dof_offset=0;
        for mu_idx=1:num_of_mus
            if (isfield(bc_vm(mu_idx).dirichlet,'myfunction'))
                for i = 1:length(bc_vm(mu_idx).dirichlet.points)
                    node_idx =  bc_vm(mu_idx).dirichlet.points(i);   
                    b(dof_offset+ node_idx) = bc_vm(mu_idx).dirichlet.myfunction(time,dt); 
                end
            end
            dof_offset=dof_offset+vm_mesh(mu_idx).dofs ;
        end
    end    
    %  Add (non-zero) Neumann BCs to the RHS Vector
    if (isfield(bc_phi,'neumann'))
        if (isfield(bc_phi.neumann,'myfunction')) 
            v = zeros(length(bc_phi.neumann.phi_e_dofs),1);
            for i = 1:length(bc_phi.neumann.phi_e_dofs)
                v(i) = bc_phi.neumann.myfunction(bc_phi.neumann.phi_e_dofs(i),time);
            end
            stim_vec = sparse(bc_phi.neumann.phi_e_dofs+sum([vm_mesh(:).dofs]),1,v,length(C),1);
            b = b + C*stim_vec;
        end
    end
    %  Solve for the linear system
    tic
    if solvers.linear_solver.type == 1
        xx = gmres(A,b,20,1e-10,20,L,U,xx);
    else
        xx = A\b;
    end
    toc
    %  Update the transmembrane models for the membrane models.
    idx = 0;
    for mu_idx=1:num_of_mus
        y{mu_idx}(:,1) = xx(idx+1:idx+vm_mesh(mu_idx).dofs);
        idx = idx + vm_mesh(mu_idx).dofs;
    end
    
    %% If a second order splitting is applied solve the membrane models for a second intermediate timestep.
    if solvers.splitting_scheme == 1
        tspan = [(time-0.5)*dt, time*dt];
        tic
        switch ode_solver
            case 0
                parfor mu_idx=1:num_of_mus
                    y{mu_idx} = heun_method(y{mu_idx},p{mu_idx},I_stim{mu_idx}(:,time),...
                        Cm{mu_idx},cell_model(mu_idx).ID,tspan,dt_ode);
                end
            case 1
                for mu_idx=1:num_of_mus
                    tmp = zeros(vm_mesh(mu_idx).dofs,length(y{mu_idx}(1,:)));
                    parfor i = 1:vm_mesh(mu_idx).dofs
                        LAST_STATES = y{mu_idx}(i,:);
                        % Solve model with ODE solver
                        [~, STATES] = ode15s(@(TIME, STATES)computeRatesAndAlgebraic(...
                            TIME, STATES, p{mu_idx},I_stim{mu_idx}(i,time),Cm{mu_idx}(i),cell_model(mu_idx).ID),... 
                            tspan, LAST_STATES, options);
                        % update ALL_STATES
    %                     ALL_STATES{mu_ind}(i,:) = STATES(end,:);
                        tmp(i,:) = STATES(end,:);
                    end
                    y{mu_idx} = tmp;
                end
        end
        toc
        % Update Vm at the end of the global timestep.
        idx = 0;
        for mu_idx=1:num_of_mus
            xx(idx+1:idx+vm_mesh(mu_idx).dofs) = y{mu_idx}(:,1);
            idx = idx + vm_mesh(mu_idx).dofs;
        end  
    
    end 
    %% Save the Solution
    if (mod(time,output.output_frequency)==0)
        output_count = output_count + 1;
        file_name = sprintf('%s/%s_%d.mat', output.filepath,output.filename,output_count);
        %
        switch output.output_spec  
            case 0 % Full state vector
                t = time*dt;
                save(file_name,'xx','y','t');%,'rhs')
            case 1 % Only the electrical potential fields
                save(file_name,'xx');
        end
     end
       
    
end
fprintf('\n The simulation has been succesfully completed :) \n')
end


