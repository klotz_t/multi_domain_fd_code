function [i,j,v] = elem_neumann_bc_matrix(node_idx,node_values,surface)
% 
%% Integration Points
x = sqrt(1/3);
if strcmp(surface,'left')
    GPs = [-1 -x -x; -1 x -x; -1 -x x; -1 x x];
elseif strcmp(surface,'right')
    GPs = [1 -x -x; 1 x -x; 1 -x x; 1 x x];
elseif strcmp(surface,'front')
    GPs =  [-x -1 -x; x -1 -x; -x -1 x; x -1 x];
elseif strcmp(surface,'back')
    GPs =  [-x 1 -x; x 1 -x; -x 1 x; x 1 x];
elseif strcmp(surface,'bottom')
    GPs =  [-x -x -1; x -x -1; -x x -1; x x -1];
elseif strcmp(surface,'top')
    GPs =  [-x -x 1; x -x 1; -x x 1; x x 1];
end
        
%
J = zeros(4,1); % Jacobean at each Gauß point
N = zeros(8,4); % Values of the basis functions (first index) at eacht Gauß point (second index)
for gp=1:4
    [~,~,J(gp)] = getJacobean(GPs(gp,:),node_values);
    N(:,gp) = triLinBasis(GPs(gp,:));
end
%% 
i = zeros(1,64);
j = zeros(1,64);
v = zeros(1,64);

idx = 1;
for n=1:8
    for m=1:8
        gg = 0;
        for gp=1:4
            gg = gg + N(n,gp).*N(m,gp).*J(gp);
        end
        i(idx) = node_idx(m);
        j(idx) = node_idx(n);
        v(idx) = gg;
        idx = idx + 1;
    end
end

end

