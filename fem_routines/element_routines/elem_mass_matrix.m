function [i,j,v] = elem_mass_matrix(node_idx,node_pos)
%  Calculate the Galerkin finite element mass matrix Int[w*w]dx, where w 
%  are the basis functions.
%  Further node_idx denotes the local node numbering and node_pos are the 
%  respective coordinates.
%% Integration Points
GPs = [-1 -1 -1; 1 -1 -1; 1 1 -1; -1 1 -1; ...
       -1 -1 1; 1 -1 1; 1 1 1; -1 1 1].*sqrt(1/3);
%
J = zeros(8,1); % Jacobean at each Gauß point
N = zeros(8,8); % Values of the basis functions (first index) at eacht Gauß point (second index)
for gp=1:8
    [~,~,J(gp)] = getJacobean(GPs(gp,:),node_pos);
    N(:,gp) = triLinBasis(GPs(gp,:));
end
%% 
i = zeros(1,64);
j = zeros(1,64);
v = zeros(1,64);

idx = 1;
for n=1:8
    for m=1:8
        gg = 0;
        for gp=1:8
            gg = gg + N(n,gp).*N(m,gp).*J(gp);
        end
        i(idx) = node_idx(m);
        j(idx) = node_idx(n);
        v(idx) = gg;
        idx = idx + 1;
    end
end

end

