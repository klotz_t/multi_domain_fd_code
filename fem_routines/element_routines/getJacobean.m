function [dXdXi,dXidX,J] = getJacobean(x,nodes)
% Approximates the Jacobean Matrix (element coordinates with respect to physical) 
% coordinates) at a random point within an element via the basis functions.
dN = dtriLinBasis(x);

dXdXi = [sum(dN(:,1).*nodes); ...
         sum(dN(:,2).*nodes); ...
         sum(dN(:,3).*nodes)]';
J     = det(dXdXi);     
dXidX = inv(dXdXi);

end

