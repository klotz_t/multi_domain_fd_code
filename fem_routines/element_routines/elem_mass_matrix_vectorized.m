function [i,j,v] = elem_mass_matrix_vectorized(node_idx,node_pos)
%  Calculate the Galerkin finite element mass matrix Int[w*w]dx, where w 
%  are the basis functions.
%  Further node_idx denotes the local node numbering and node_pos are the 
%  respective coordinates. 
%% Integration Points
GPs = [-1 -1 -1; 1 -1 -1; 1 1 -1; -1 1 -1; ...
       -1 -1 1; 1 -1 1; 1 1 1; -1 1 1].*sqrt(1/3);
%
J = zeros(8,1); % Jacobean at each Gauß point
N = zeros(8,8); % Values of the basis functions (first index) at eacht Gauß point (second index)
for gp=1:8
    [~,~,J(gp)] = getJacobean(GPs(gp,:),node_pos);
    N(:,gp) = triLinBasis(GPs(gp,:));
end
%% 
i = zeros(1,64);
j = zeros(1,64);
v = zeros(1,64);

gg = zeros(8,8);
for gp=1:8
    gg = gg + J(gp).*N(:,gp)*N(:,gp)';
end
% node_idx must be a row vector
if size(node_idx,2) == 1
    node_idx = node_idx';
end
i = repmat(node_idx,1,8);
j = reshape(repmat(node_idx,8,1),[],1)';
v = reshape(gg,[],1)';

end

