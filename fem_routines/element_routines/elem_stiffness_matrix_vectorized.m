function [i,j,v] = elem_stiffness_matrix_vectorized(k,sigma,node_idx,node_pos,IPP)
%  Claculate the finite element stiffness matrix for the weak form of a 
%  diffusion term, i.e. Int[k*sigma*grad(phi)*grad(w)]dx. Therein k is a 
%  constant factor, sigma is the diffusion tensor, phi represents a 
%  scalar potential and w denotes the test function (which is equal to the
%  basis functions in the employed Galerkin approach). 
%  Further node_idx denotes the local node numbering and node_pos are the 
%  respective coordinates. 
%  IPP denotes the method which is applied to get the parameters k and
%  sigma at each integration point. IPP = 0: Parameters are constant across
%  the element. IPP = 1: The paramters are given at the nodes and are
%  interpolated via the basis functions. IPP = 2: The parameters are
%  directly specified for each Gauss-point.
%% Interpolation to obtain the parameters at each GP
if nargin == 4
    IPP = 0; % element based IPP is the default
end
%% Integration Points (GPs)
GPs = [-1 -1 -1; 1 -1 -1; 1 1 -1; -1 1 -1; ...
       -1 -1 1; 1 -1 1; 1 1 1; -1 1 1].*sqrt(1/3);
%
dXidX = zeros(3,3,8); % Derivative of element coordinates with respect to global coordinates (dXi_i/dX_j, idx 1 = i, idx 2 = j) at each GP (idx 3).
J = zeros(8,1); % Jacobean at each GP
dN = zeros(8,3,8); % Values of the basis functions derivatives at each GP (idx 1: basis function, idx 2: element coordinate, idx 3: GP)
for gp=1:8
    [~,dXidX(:,:,gp),J(gp)] = getJacobean(GPs(gp,:),node_pos);
    dN(:,:,gp) = dtriLinBasis(GPs(gp,:));
    if IPP == 0
        sigmas(:,:,gp) = sigma;
        k_values(gp) = k;
    elseif IPP == 1
        N = triLinBasis(GPs(gp,:));
        sigmas(:,:,gp) = zeros(3,3);
        k_values(gp) = 0;
        for i=1:8
            sigmas(:,:,gp) = sigmas(:,:,gp) + N(i).*sigma(:,:,i);
            k_values(gp) = k_values(gp) + N(i).*k(i);
        end
    elseif IPP ==2
        sigma(:,:,gp) = sigma;
        k_values(gp) = k;
    end
end
%% 
i = zeros(1,64);
j = zeros(1,64);
v = zeros(1,64);

gg = zeros(8,8);
for gp=1:8
    gg = gg + J(gp).*(...
       k_values(gp).*sigmas(1,1,gp).*dN(:,:,gp)*dXidX(:,1,gp)*(dN(:,:,gp)*dXidX(:,1,gp))' + ...
       k_values(gp).*sigmas(2,2,gp).*dN(:,:,gp)*dXidX(:,2,gp)*(dN(:,:,gp)*dXidX(:,2,gp))' + ...
       k_values(gp).*sigmas(3,3,gp).*dN(:,:,gp)*dXidX(:,3,gp)*(dN(:,:,gp)*dXidX(:,3,gp))' + ...
       k_values(gp).*sigmas(1,2,gp).*dN(:,:,gp)*dXidX(:,1,gp)*(dN(:,:,gp)*dXidX(:,2,gp))' + ...
       k_values(gp).*sigmas(1,3,gp).*dN(:,:,gp)*dXidX(:,1,gp)*(dN(:,:,gp)*dXidX(:,3,gp))' + ...
       k_values(gp).*sigmas(2,1,gp).*dN(:,:,gp)*dXidX(:,2,gp)*(dN(:,:,gp)*dXidX(:,1,gp))' + ...
       k_values(gp).*sigmas(2,3,gp).*dN(:,:,gp)*dXidX(:,2,gp)*(dN(:,:,gp)*dXidX(:,3,gp))' + ...
       k_values(gp).*sigmas(3,1,gp).*dN(:,:,gp)*dXidX(:,3,gp)*(dN(:,:,gp)*dXidX(:,1,gp))' + ...
       k_values(gp).*sigmas(3,2,gp).*dN(:,:,gp)*dXidX(:,3,gp)*(dN(:,:,gp)*dXidX(:,2,gp))');
end

% node_idx must be a row vector
if size(node_idx,2) == 1
    node_idx = node_idx';
end
i = repmat(node_idx,1,8);
j = reshape(repmat(node_idx,8,1),[],1)';
v = reshape(gg,[],1)';


end

