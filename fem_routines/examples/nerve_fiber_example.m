%% Set up script for a finite element multi-domain problem
clear all
<<<<<<< HEAD:fem_routines/run_test.m

addpath utilities/
addpath matrices_routines/
=======
%% Set paths
addpath ../basis_functions/
addpath ../boundary_condition_routines/
addpath ../matrices_routines/
addpath ../mesh_routines/
addpath ../simulation_routines/
addpath ../element_routines/
addpath ../utilities/
addpath ../../cell_model_routines/
>>>>>>> 19473aec368ad647b6eb3084096cd4c8691ea565:fem_routines/examples/nerve_fiber_example.m
%% Set the total simulation time
t_end = 6; % [ms] 
%% Specify the requested number of motor units
num_of_mus = 1;
%% Define the FE mesh for the overall spatial domain of interest, i.e. solving for phi_e
%  This yields a cubic domain
<<<<<<< HEAD:fem_routines/run_test.m
lengthx = 100; % cm
width  = 0.1; % cm
height = 0.1; % cm
dx     = 0.1; % cm
=======
lengthx = 10; % cm
width  = 0.4; % cm
height = 0.4; % cm
dx     = 0.025; % cm
>>>>>>> 19473aec368ad647b6eb3084096cd4c8691ea565:fem_routines/examples/nerve_fiber_example.m
dy     = 0.1;  % cm
dz     = 0.1;  % cm
phi_mesh = generate_regular_cubic_mesh(lengthx,width,height,dx,dy,dz);

<<<<<<< HEAD:fem_routines/run_test.m
num_dx=round(lengthx/dx,0); 
num_dy=round(width/dy,0);
num_dz=round(height/dz,0);

%% Set up a mesh for each motor unit / intracellular domain, i.e. solving for Vm
active_elements.length=lengthx; %cm
active_elements.height=width; %cm
active_elements.width=height; %cm
active_elements.elements = 1:size(phi_mesh.elements.phi_e_dofs,2);% define_active_area(lengthx,width,height,dx,dy,dz,active_elements.length,active_elements.width,active_elements.height);% Specify the phi_e elements which contain electrical active cells
=======
num_dx=round(lengthx/dx,0)+1;
num_dy=round(width/dy,0)+1;
num_dz=round(height/dz,0)+1;

%% Set up a mesh for each motor unit / intracellular domain, i.e. solving for Vm
elements = reshape(1:(num_dx-1)*(num_dy-1)*(num_dz-1),[num_dx-1,num_dy-1,num_dz-1]);
% active_elements.length=lengthx; %cm
% active_elements.height=0.1; %cm
% active_elements.width=0.1; %cm
% active_elements.elements = define_active_area(lengthx,width,height,dx,dy,dz,active_elements.length,active_elements.width,active_elements.height);% Specify the phi_e elements which contain electrical active cells
active_elements.elements = reshape(elements(:,2:3,2:3),[],1);
>>>>>>> 19473aec368ad647b6eb3084096cd4c8691ea565:fem_routines/examples/nerve_fiber_example.m
for mu_idx = 1:num_of_mus
    dof_mapping = globalDof2muDof(phi_mesh,active_elements.elements);
    vm_mesh(mu_idx) = get_mu_mesh(phi_mesh,active_elements.elements,dof_mapping);
end
%% Specify the model parameters in each element / at each node / gauss point

IPP = 0; % All parameter are defined per element 

axondiameter=15; %Axon diamter [micrometer]; fiber diameter is 0.66*axondiameter (Kim vs. Sweeney 0.6)
internodallength=2*10^3; %length between two nodes (myelinised part) [micrometer]
nodalgaplength=1.5; %[micrometer] (see Sweeney)
lnlm=nodalgaplength/internodallength; % ratio of intranodal length to nodal gap length [-]
Am = linspace(500,250,num_of_mus)*lnlm; %multiply A_m with ratio lnlm as the surface area is only th non-myelinated part

sigma_i = (1/0.1) * [1 0 0; 0 0 0; 0 0 0]; % Intracellular conductivity 
sigma_e = (1/0.3) * [1 0 0; 0 0.5 0; 0 0 0.5]; % Extracellular conductivity
sigma_o = 0.4 * eye(3); % Conductivity in fat

Cm  = ones(1,num_of_mus).*2.5; % membrane capacity [microF/cm^2] (for each MU)
f_r = [1/3, 2/3];

phi_mesh.parameters.sigma = sigma_o.*ones(3,3,size(phi_mesh.elements.phi_e_dofs,2));
for i=1:length(active_elements.elements)
    idx = active_elements.elements(i);
    phi_mesh.parameters.sigma(:,:,idx) = (sigma_i + sigma_e);
end

for mu_idx=1:num_of_mus
    vm_mesh(mu_idx).parameters.sigma_i = sigma_i.*ones(3,3,size(vm_mesh(mu_idx).elements.vm_dofs,2));
    vm_mesh(mu_idx).parameters.k_am_cm = 1/(Cm(mu_idx)*Am(mu_idx)).*ones(1,size(vm_mesh(mu_idx).elements.vm_dofs,2));
    vm_mesh(mu_idx).parameters.f_r = f_r(mu_idx).*ones(1,size(vm_mesh(mu_idx).elements.vm_dofs,2));
end
%% Set up the membrane models
for mu_idx=1:num_of_mus
    cell_model(mu_idx).ID = 5; % Hodgkin-Huxley 1952 Model
    cell_model(mu_idx).Cm = Cm(mu_idx);
end
%% Specify the time settings and the numerical solvers
solvers.dt =0.026;%set_dt(dx);%sqrt((dx^2)/sqrt(80)); % [ms] Global time step
solvers.splitting_scheme = 0; % 0: Gudonov Splitting, 1: Strang Splitting
solvers.fem_time_discretization = 1; % Implicit euler method to solve for the coupled PDE system 
solvers.linear_solver.type = 0; % 0: Matlab mldivide, 1: GMRES
solvers.linear_solver.options = struct('type','crout','droptol',1e-6);
solvers.ode_solver.type = 1; % 0: Heun method, 1: ODE15s
if solvers.ode_solver.type == 0
    solvers.ode_solver.options = 0.01; % [ms] Time Step width for the Heun solver
else
    solvers.ode_solver.options = odeset('RelTol', 1e-06, 'AbsTol', 1e-06, 'MaxStep', 0.1); % Settings for ODE15s
end
%% 
output.filepath = sprintf(strcat('results/%d/'),dx) ; % Specify folder where the results are stored.
output.filename = 'simple_example'; % Specify filenames
output.output_frequency = 1; % Save the state vector after each global time step.
output.output_spec = 1; % 0: Save all state variables, 1: Save only electrical potentials.

%% Define external stimulus 
ext_stim.electrode_size_elements=[5 3]; %size of electrode, given in elements in [x-direction y-direction]
ext_stim.electrode_size=(ext_stim.electrode_size_elements(1,1)*dx)*(ext_stim.electrode_size_elements(1,2)*dy);%size of the square electrode [cm^2]
ext_stim.pulse_width=0.5; %[ms]
ext_stim.current=150;%[mA]
ext_stim.amplitude = (ext_stim.current*10^3)/ext_stim.electrode_size; %[microampere/cm^2]=current density
ext_stim.stim_length = 10; %[ms] time length of the whole stimulation signal
ext_stim.type=0; %  0=monophasic; biphasix=1; constant=2
ext_stim.frequency=50; %[Hz=1/s]
ext_stim.Istim= 0;%calculate_ext_stimulus(ext_stim.amplitude,solvers.dt,t_end/solvers.dt,ext_stim.type,ext_stim.frequency,ext_stim.pulse_width);
%% Apply boundary conditions to the extracellular potential
bc_phi.dirichlet.phi_e_dofs = 1:(num_dx+1)*(num_dy+1); % Set the extracellular potential to zero at the bottom of the sample
bc_phi.dirichlet.myfunction = @(phi_e_dof,timestep) 0;
% gg = reshape(1:num_dx*num_dy,num_dx,num_dy); %specifiy the position of the electrode. In this case the electrode is on top
% gg= [gg(10:10+(ext_stim.electrode_size_elements(1,1)-1), 3:3+(ext_stim.electrode_size_elements(1,2)-1))];
% bc_phi.neumann.elements = length(phi_mesh.elements.phi_e_dofs)-reshape(gg,[],1);
% bc_phi.neumann.surface_vec = strings(length(bc_phi.neumann.elements),1); %initialising surface vector
% bc_phi.neumann.surface_vec(:,:) = 'top'; %declaratio of surface vector
% if (min(bc_phi.neumann.surface_vec=='top')==1)==1
%     bc_phi.neumann.phi_e_dofs = unique(reshape(phi_mesh.elements.phi_e_dofs(5:8,bc_phi.neumann.elements),[],1),'stable');
%     %sortiere doppelte einträge raus unique
% else error('Error: Conditions for surface are only defined for the "top" Surface')
% end;
% bc_phi.neumann.myfunction = @(phi_e_dof,timestep) ext_stim.Istim(1,timestep); % Can be anyself-defined function specifieing the BC value for a given DOF and timestep.
%% Set stimuli at the neuromuscular junctions
for mu_idx=1:num_of_mus
%     bc_vm(mu_idx).nmj.points = [round(num_dx/2,0), round(num_dx*1.5,0)];
%     bc_vm(mu_idx).nmj.times = 0;
%     bc_vm(mu_idx).nmj.amplitude = 10000;%3000;%4750;
%     bc_vm(mu_idx).nmj.dt = 0.1; 
    bc_vm(mu_idx).dirichlet.myfunction=@(timestep,dt) action_potential(timestep,dt);
    bc_vm(mu_idx).dirichlet.points= [round(num_dx/2,0), round(num_dx*1.5,0),round(num_dx*2.5,0),round(num_dx*3.5,0)];
end
%% Run the Simulation
addpath ../cell_model_routines/
addpath ../scenario_routines/

<<<<<<< HEAD:fem_routines/run_test.m

MultiDomain_FEM(num_of_mus,t_end,phi_mesh,vm_mesh,IPP,solvers,output,bc_phi,bc_vm,cell_model,dx,dy,dz)
=======
MultiDomain_FEM(num_of_mus,t_end,phi_mesh,vm_mesh,IPP,solvers,output,bc_phi,bc_vm,cell_model)
>>>>>>> 19473aec368ad647b6eb3084096cd4c8691ea565:fem_routines/examples/nerve_fiber_example.m
