%% Set up script for a finite element multi-domain problem
clear all
%% Set paths
addpath ../basis_functions/
addpath ../boundary_condition_routines/
addpath ../matrices_routines/
addpath ../mesh_routines/
addpath ../simulation_routines/
addpath ../element_routines/
addpath ../utilities/
addpath ../../cell_model_routines/
%% Set the total simulation time
t_end = 20; % [ms] 
%% Specify the requested number of motor units
num_of_mus = 2;
%% Define the FE mesh for the overall spatial domain of interest, i.e. solving for phi_e
%  This yields a cubic domain
length = 1; % cm
width  = 1; % cm
height = 1; % cm
dx     = 0.05; % cm
dy     = 0.1;  % cm
dz     = 0.1;  % cm
phi_mesh = generate_regular_cubic_mesh(length,width,height,dx,dy,dz);
%% Set up a mesh for each motor unit / intracellular domain, i.e. solving for Vm
elements = reshape(1:20*10*10,[20 10 10]);
muscle_elements =  reshape(elements(:,:,:),[],1);% Specify the phi_e elements which contain electrical active cells
% 
for mu_idx = 1:num_of_mus
    dof_mapping = globalDof2muDof(phi_mesh,muscle_elements);
    vm_mesh(mu_idx) = get_mu_mesh(phi_mesh,muscle_elements,dof_mapping);
end
%% Specify the model parameters in each element / at each node / gauss point

IPP = 0; % All parameter are defined per element 

sigma_i = 8.93 * [1 0 0; 0 0 0; 0 0 0]; % Intracellular conductivity 
sigma_e = 6.7 * [1 0 0; 0 0.5 0; 0 0 0.5]; % Extracellular conductivity
sigma_o = 0.4 * eye(3); % Conductivity in fat

Cm  = [1, 1];
Am  = [500, 250];
f_r = [1/3, 2/3];
Am  = [500, 500];
f_r = [1/2, 1/2];

phi_mesh.parameters.sigma = (sigma_i + sigma_e).*ones(3,3,size(phi_mesh.elements.phi_e_dofs,2));
% for idx=(size(muscle_elements,2)+1):size(phi_mesh.parameters.sigma,3)
%     phi_mesh.parameters.sigma(:,:,idx) = sigma_o;
% end

for mu_idx=1:num_of_mus
    vm_mesh(mu_idx).parameters.sigma_i = sigma_i.*ones(3,3,size(vm_mesh(mu_idx).elements.vm_dofs,2));
    vm_mesh(mu_idx).parameters.k_am_cm = 1/(Cm(mu_idx)*Am(mu_idx)).*ones(1,size(vm_mesh(mu_idx).elements.vm_dofs,2));
    vm_mesh(mu_idx).parameters.f_r = f_r(mu_idx).*ones(1,size(vm_mesh(mu_idx).elements.vm_dofs,2));
end

% for elem_idx = 1:20*10*5
%     vm_mesh(2).parameters.f_r(elem_idx) = 0;
% end
%% Set up the membrane models
for mu_idx=1:num_of_mus
    cell_model(mu_idx).ID = 0; % Hodgkin-Huxley 1952 Model
    cell_model(mu_idx).Cm = Cm(mu_idx);
end
%% Specify the time settings and the numerical solvers
solvers.dt = 0.1; % [ms] Global time step
solvers.splitting_scheme = 0; % 0: Gudonov Splitting, 1: Strang Splitting
solvers.fem_time_discretization = 1; % Implicit euler method to solve for the coupled PDE system 
solvers.linear_solver.type = 1; % 0: Matlab mldivide, 1: GMRES
solvers.linear_solver.options = struct('type','crout','droptol',1e-6);
solvers.ode_solver.type = 0; % 0: Heun method, 1: ODE15s
if solvers.ode_solver.type == 0
    solvers.ode_solver.options = 0.01; % [ms] Time Step width for the Heun solver
else
    solvers.ode_solver.options = odeset('RelTol', 1e-06, 'AbsTol', 1e-06, 'MaxStep', 0.1); % Settings for ODE15s
end
%% 
output.filepath = 'results'; % Specify folder where the results are stored.
output.filename = 'simple_example'; % Specify filenames
output.output_frequency = 1; % Save the state vector after each global time step.
output.output_spec = 1; % 0: Save all state variables, 1: Save only electrical potentials.
%% Apply boundary conditions to the extracellular potential
gg = reshape(1:21*11*11,[21 11 11]);
bc_phi.dirichlet.phi_e_dofs = reshape(gg(:,:,11),[],1);%[];%1:21*11; % Set the extracellular potential to zero at the bottom of the sample
bc_phi.dirichlet.myfunction = @(phi_e_dof,timestep) 0;
% bc_phi.neumann.elements = [];
% bc_phi.neumann.surface_vec = [];
% bc_phi.neumann.phi_e_dofs = [];
% bc_phi.neumann.myfunction = @(phi_e_dof,timestep) 0; % Can be any
% self-defined function specifieing the BC value for a given DOF and timestep.
%% Set stimuli at the neuromuscular junctions
for mu_idx=1:num_of_mus
    bc_vm(mu_idx).nmj.points = 21*11*4+21*5+11;
    bc_vm(mu_idx).nmj.times = 1;%1+(mu_idx-1)*10;
    bc_vm(mu_idx).nmj.amplitude = 700;
    bc_vm(mu_idx).nmj.dt = 0.1; 
end
%% Run the Simulation
addpath ../cell_model_routines/
addpath ../scenario_routines/

MultiDomain_FEM(num_of_mus,t_end,phi_mesh,vm_mesh,IPP,solvers,output,bc_phi,bc_vm,cell_model)
