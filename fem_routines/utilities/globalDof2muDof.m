function dof_mapping = globalDof2muDof(phi_mesh,elements)
% Calculates a mapping from phi_e dofs to Vm^i dofs for a MU domain
% which occupies only a part of the overall domain. 
% The first colum of 'dof_mapping' contains the Vm^i dof number the
% second colum the phi_e dof number.

gg = zeros(8*length(elements),1);
idx = 0;
for elem=1:length(elements) 
    gg(idx+1:idx+8) = phi_mesh.elements.phi_e_dofs(:,elements(elem));
    idx = idx + 8;
end
hh = unique(gg);
dof_mapping = zeros(length(hh),2);
dof_mapping(:,1) = 1:length(hh);
dof_mapping(:,2) = hh;
end

