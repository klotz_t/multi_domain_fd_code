function [dt]=set_dt(dx)

% based on following found values for sigma_i=(1/0.05)eye(3)
% polyfit for these values
% dx=0.025 --> dt=0.0140
% dx=0.05 --> dt= 0.0200  
% dx=0.1 --> dt=0.0520    
% dx=0.2 --> dt=0.1250

% based on following found values for sigma_i=(1/0.05)eye(3) for
% length=020cm
% polyfit for these values
% dx=0.025 --> dt=0.020
% dx=0.05 --> dt= 0.0300  
% dx=0.1 --> dt=0.0520    
% dx=0.2 --> dt=0.1250

x=[0.025 0.05 0.1 0.2];
y=[0.02 0.03 0.09 0.19];
p=polyfit(x,y,3);
f = polyval(p,x);
%T = table(x,y,f,y-f,'VariableNames',{'X','Y','Fit','FitError'}); % table
%for evaluation of fit

dt=p(1)*dx^3+p(2)*dx^2+p(3)*dx+p(4);