function [I_stim] = calculate_ext_stimulus(amplitude,dt,num_of_dt,stim_type,frequency,pulse_width)
%% Author: Jessica Walz
% Calculates an external electrical stimulus profile (I_stim) based on
% the specified input arguments.
% - amplitude (of the current_density) [microampere/(cm^2)]
% - dt: time step size in milliseconds
% - num_of_dt: Total number of simulated time steps
% - stimtype_ext: 0 = monophasic; 1 = biphasic; 2 = galvanisation
% - frequency [Hz]
% - pulse_width [ms]

%stim_length has to be at least one periodic time
I_stim = zeros(1,num_of_dt);
periodic_dt=(10^3/frequency)/dt; %[ms/ms=1]
num_of_periods=floor(num_of_dt/periodic_dt);%[]
pw_dt=round(pulse_width/dt,0);%[]
%
for i=1:num_of_dt
    if (stim_type==0) % monophasic stimulus
        for ii=0:num_of_periods
            for t=1+floor(ii*periodic_dt):floor(ii*periodic_dt+pw_dt);
                I_stim(t)=amplitude*dt;
            end;
        end
    elseif (stim_type==1) % biphasic stimulus
        for ii=0:num_of_periods
            for t=1+floor(ii*periodic_dt):floor(ii*periodic_dt+pw_dt);
                I_stim(t)=-0.5*amplitude*dt;
            end;
            for t=1+floor(ii*periodic_dt+pw_dt):floor(ii*periodic_dt+2*pw_dt);
                I_stim(t)=0.5*amplitude*dt;
            end;
        end
    elseif (stim_type==2) % galvanisation
            for t=1:num_of_dt
                I_stim(t)=amplitude*dt;
            end
    end
end
end