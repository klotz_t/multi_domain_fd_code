
filepath='results/'
fr =20;
filesaver=strcat('I50_fr',num2str(fr));
load(strcat(filepath(1,:),'simple_example_info.mat'));
%% plot results for one time frame
nx_total=21;
ny_total=51;
nz_total=51;
nx=21;
ny=11;
nz=11;
n=nx*ny*nz;
Vm=zeros(num_of_mus,nx,ny,nz);
load(sprintf(strcat(filepath,'simple_example_%d.mat'),fr))
% Reshape
for mu_ind=0:num_of_mus-1
    Vm(mu_ind+1,:,:,:)= reshape(xx(1+n*mu_ind:n+n*mu_ind),nx,ny,nz); %Vm contains the Vms of the mus
end;


phi_e = reshape(xx(2*n+1:end),nx_total,ny_total,nz_total); %elements of xx representing the extracellular potentials (one phi_e for eatch grid point)
%% add suplot('transmembran potential of different planes')
Vmplot=figure;
for mu_ind=1:num_of_mus
    subplot(2,num_of_mus,mu_ind);
    contourf(squeeze(Vm(mu_ind,:,5,1:nz))') % Schnittfl�che x-z
    set(gca,'YTick',0:2:nz,'YTickLabel',1:2:nz)
    ylabel('z');xlabel('x (y=10)');title(sprintf('MU %d',mu_ind))
    colorbar;
    subplot(2,num_of_mus,num_of_mus+mu_ind)
    contourf(squeeze(Vm(mu_ind,:,:,1))') % Schnittfl�che y-z
    set(gca,'YTick',0:2:nz,'YTickLabel',1:2:nz);
    ylabel('z');xlabel('y (x=40)')
    colorbar;
end
%%
hold on
colormap(Vmplot,jet(100))
set(gca,'YTick',0:2:nz-1,'YTickLabel',1:2:nz-1)
set(gcf,'units','points','position',[10,10,450,300])
contourf(squeeze(Vm(1,:,round(ny/2),1:nz))') % Schnittfl�che x-z


% pos = get(cb3,'Position');
% cb3.Label.Position = [pos(1)+2 pos(2)]; % to change its position
% set(get(cb3,'ylabel'),'rotation',0)

%colorbar('Ticks',[-100,-75,-50,-25,0,25,50,75]);caxis([-100,75])
%title(sprintf('x-z plane (y=%.1f mm)',ny/2*0.5))
xlabel('x [mm]')
ylabel('z [mm]')
set(gca,'YTick',0:2:nz,'YTickLabel',0.5:1:(nz)*0.5)
set(gca,'XTick',0:8:nx,'XTickLabel',0:4:nx*0.5)
%%
Vmploty=figure;
cb4=colorbar('XLim',[-100 60]);
ylabel(cb4, 'V_m [mV]')
colormap(Vmploty,jet(100))
hold on
%subplot(1,2,2)
contourf(squeeze(Vm(1,round(nx/2,0),:,1:nz))') % Schnittfl�che y-z
set(gcf,'units','points','position',[10,10,450,300])
%colorbar('Ticks',[-100,-75,-50,-25,0,25,50,75]);caxis([-100,75])
set(gca,'YTick',0:2:nz-1,'YTickLabel',1:2:nz-1);
pos = get(cb4,'Position');
%cb4.Label.Position = [pos(1)+2 pos(2)]; % to change its position
%set(get(cb4,'ylabel'),'rotation',0)
%title(sprintf('y-z plane (x=%.1f mm)',nx/2*0.5))
xlabel('y [mm]')
ylabel('z [mm]')
set(gca,'YTick',0:2:nz,'YTickLabel',0.5:1:(nz)*0.5)
set(gca,'XTick',0:4:ny,'XTickLabel',0:2:ny*0.5)
%[ax4,h3]=suplabel(sprintf('Transmembran potentials at t=%.1f ms',input.dt*fr) ,'t');
%set(h3,'FontSize',15)
%%
% saveas(Vmplot,strcat('plots/Vm/',filesaver,'x'),'png');
% saveas(Vmplot,strcat('plots/Vm/',filesaver,'x'));
% saveas(Vmploty,strcat('plots/Vm/',filesaver,'y'),'png');
% saveas(Vmploty,strcat('plots/Vm/',filesaver,'y'));
%%
figure, contourf(squeeze(phi_e(:,1,:))')
title('extracellular potentials in the x-z plane (y=20)')
set(gca,'YTick',0:2:nz-1,'YTickLabel',1:2:nz-1);
colorbar;
xlabel('x')
ylabel('z')

%% plot of Vms at one specific node for the whole simulationtime  
muscleshape=reshape(1:nx*ny*nz,nx,ny,nz);
point=muscleshape(7,1,1) %one point that is close to the surface (z=20) and in the middle of x and y. just an example
for i=1:200-1
    load(sprintf(strcat(filepath,'simple_example_%d.mat'),i))
    Vm_point(i) = Vm(point);
end
%%  
figure;
plot(Vm_point,'r')
%title(strcat('V_m at the grid point x=20, y=10, z=',num2str(nz-2)))
xlabel('time [ms]');
xticks([0:1:20]);
xticklabels({'0','5','10','15','20','25','30','35','40','45','50','55','60','65','70','75','80'});
ylabel('V_m [mV]')
