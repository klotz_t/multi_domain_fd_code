function [index] = define_active_area(length,width,height,dx,dy,dz,active_length,active_width,active_height)

%specify number of grid points in x,y,z-direction
num_dx=round(length/dx,0);
num_dy=round(width/dy,0);
num_dz=round(height/dz,0);

num_active_dx=round(active_length/dx,0);
num_active_dy=round(active_width/dy,0);
num_active_dz=round(active_height/dz,0);

active_area=zeros(num_dx-1,num_dy-1,num_dz-1);

%the active_area is defined in the middle of the x,y and z-direction and
%set to one 
active_area(:,num_dy/2-num_active_dy/2:num_dy/2-num_active_dy/2+num_active_dy-1,num_dz/2-num_active_dz/2:num_dy/2-num_active_dz/2+num_active_dz-1)=1;
index=find(active_area); %

end

