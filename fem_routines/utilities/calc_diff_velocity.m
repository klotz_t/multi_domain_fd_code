clear all;

%% read data for simulations with different discretization in x-direction
dxx=[0.05];%;0.1;0.01;0.001]
time=15;
path='results/%d/';

for j=1:length(dxx)
    load(sprintf(strcat(path,'simple_example_info.mat'),dxx(j,1))) 
    load(sprintf(strcat(path,'simple_example_%d.mat'),dxx(j,1),0))
    dt=solvers.dt;
end;


Aa=zeros(length(xx),round((20/dt),0));
for i=1:time
    load(sprintf(strcat(path,'simple_example_%d.mat'),dxx(j,1),i)) 
    Aa(:,i)=xx;
end;
ma=20/dx;
pos1= bc_vm(1).dirichlet.points(1);
max_start=zeros(1,time);
for k=1:time-10
    if round((max(Aa(1:ma,k))),4)==round(max(Aa(pos1,k)),4)
        max_start(1,k)=k;
    end;
end;
time_pos1=max(max_start);

indeces_max=zeros(1,time);
for t=time_pos1:time
    [value_max index_max]=max(Aa(pos1:ma,t)); % pro Zeitschritt maximalen Wert suchen und Index zwischen pos1 und ma
    values_max(t)=value_max;
    if index_max==1
        index_max=0;
    end
    if index_max==ma-pos1+1
        index_max=0;
    end;
    indeces_max(t)=index_max;
end;

steps=linspace(1,time,time);
finals= [steps;indeces_max;values_max]; % arrray contains per timestep the position where the transmembrane potential is maximal and its value


velocities=zeros(1,ma-pos1);
for t=1:length(steps)
    if finals(2,t)>0
        velocities(finals(2,t))=(dx*10^-2*finals(2,t))/(dt*10^-3*(finals(1,t)-time_pos1)); %[m/s]
    end;
end;
        
figure; plot(velocities);
% velocity=zeros(1,ma);
% for i=pos1:ma %Punkte durchgehen (Zeilen)
%     max_point=zeros(1,time);
%     for k=time_pos1+1:30 %für jeden Zeitschritt maximalen Wert suchen 
%         max_val=round(max(Aa(pos1:ma,k)),2);
%         if max_val==round(max(Aa(i,k)),2) % wenn maximaler Wert an Punkt i steht dann als max_point speichern
%             max_point(1,k)=k;
%         else max_point(1,k)=0;
%         end;
%     end;
%     time_pos2=min(nonzeros(max_point));
%     if isempty(time_pos2)
%         velocity(i)=0;
%     else
%         velocity(i)=(dx*(i-pos1))/((time_pos2-time_pos1)*dt);
%     end;
% end;
% 
% figure;
% plot(velocity(pos1:end));

% find(A[51,:]>23
% 
% finde wert wo bei 51 das Maxima ist aber trotzdem kleiner  23
% 
% finde maxima bei x=56 . heißt dort AP zu dem ZP
% 
% mache das ganze für 
% 0.001
% 0.01
% 0.1
% 1 
% diskretisierung und passe zeischritte entsprechend an