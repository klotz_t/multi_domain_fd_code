function vm_mesh = get_mu_mesh(phi_mesh,elements,dof_mapping)
% Generate a mesh for a intracellular domain given the FE mesh of the
% whole domain (variable 'fe_mesh'), the elements on wich the intracellular domain is
% defined (variable 'elements') and a mapping from the global node numbers
% to the intracellular dofs.

vm_mesh.elements.vm_dofs = zeros(8,length(elements));

gg = zeros(8,size(phi_mesh.elements.phi_e_dofs,2));
for i=1:length(dof_mapping)
    hh = phi_mesh.elements.phi_e_dofs == dof_mapping(i,2);
    gg = gg + hh.*dof_mapping(i,1);
end


for elem=1:length(elements)
    vm_mesh.elements.vm_dofs(:,elem) = gg(:,elements(elem));
    vm_mesh.elements.phi_e_dofs(:,elem) = phi_mesh.elements.phi_e_dofs(:,elements(elem));
    vm_mesh.elements.coordinates(:,elem,:) = phi_mesh.elements.coordinates(:,elements(elem),:);
end

vm_mesh.dofs = size(dof_mapping,1);

end

