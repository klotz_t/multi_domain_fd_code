function [phi_mesh] = generate_regular_cubic_mesh(length,width,height,dx,dy,dz)
% Generates a uniformly spaced finite element mesh for a cube given the 
% overall length, width and height as well as the node spacing dx,dy and
% dz.
nx = round(length/dx,0)+1;
ny = round(width/dy,0)+1;
nz = round(height/dz,0)+1;
nodes = reshape(1:nx*ny*nz,nx,ny,nz);

xx = 0:dx:length;
yy = 0:dy:width;
zz = 0:dz:height;

phi_mesh.elements.phi_e_dofs = zeros(8,(nx-1)*(ny-1)*(nz-1));
phi_mesh.elements.coordinates = zeros(8,(nx-1)*(ny-1)*(nz-1),3);
elem_idx=1;
for z=1:nz-1
    for y=1:ny-1
        for x=1:nx-1
            phi_mesh.elements.phi_e_dofs(1,elem_idx) = nodes(x,y,z);
            phi_mesh.elements.phi_e_dofs(2,elem_idx) = nodes(x+1,y,z);
            phi_mesh.elements.phi_e_dofs(3,elem_idx) = nodes(x+1,y+1,z);
            phi_mesh.elements.phi_e_dofs(4,elem_idx) = nodes(x,y+1,z);
            phi_mesh.elements.phi_e_dofs(5,elem_idx) = nodes(x,y,z+1);
            phi_mesh.elements.phi_e_dofs(6,elem_idx) = nodes(x+1,y,z+1);
            phi_mesh.elements.phi_e_dofs(7,elem_idx) = nodes(x+1,y+1,z+1);
            phi_mesh.elements.phi_e_dofs(8,elem_idx) = nodes(x,y+1,z+1);
            phi_mesh.elements.coordinates(1,elem_idx,:) = [xx(x), yy(y), zz(z)]'; 
            phi_mesh.elements.coordinates(2,elem_idx,:) = [xx(x+1), yy(y), zz(z)]';
            phi_mesh.elements.coordinates(3,elem_idx,:) = [xx(x+1), yy(y+1), zz(z)]';
            phi_mesh.elements.coordinates(4,elem_idx,:) = [xx(x), yy(y+1), zz(z)]';
            phi_mesh.elements.coordinates(5,elem_idx,:) = [xx(x), yy(y), zz(z+1)]';
            phi_mesh.elements.coordinates(6,elem_idx,:) = [xx(x+1), yy(y), zz(z+1)]';
            phi_mesh.elements.coordinates(7,elem_idx,:) = [xx(x+1), yy(y+1), zz(z+1)]';
            phi_mesh.elements.coordinates(8,elem_idx,:) = [xx(x), yy(y+1), zz(z+1)]';
            
            elem_idx = elem_idx + 1;
        end
    end
end

phi_mesh.dofs = nx.*ny.*nz;


end

