function [C] = apply_Neumann_BC_to_phi_e(phi_mesh,elements,surfaces,total_vm_dofs)
% Apply a Neumann BC on the extracellular potential
i = zeros(1,64*length(elements));
j = zeros(1,64*length(elements));
v = zeros(1,64*length(elements));
idx = 0;
for surf_idx=1:length(elements)
    elem_idx = elements(surf_idx);
    [i(idx+1:idx+64), j(idx+1:idx+64), v(idx+1:idx+64)] = ...
        elem_neumann_bc_matrix(phi_mesh.elements.phi_e_dofs(:,elem_idx),...
        squeeze(phi_mesh.elements.coordinates(:,elem_idx,:)),surfaces(surf_idx,:));
    idx = idx + 64;
end
M = sparse(i,j,v,phi_mesh.dofs,phi_mesh.dofs);
C = blkdiag(sparse(total_vm_dofs,total_vm_dofs),M);

end

