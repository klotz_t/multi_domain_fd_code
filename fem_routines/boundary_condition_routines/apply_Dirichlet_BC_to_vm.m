function [M11_bc,K11_bc,K12_bc] = apply_Dirichlet_BC_to_vm(M11,K11,K12,nodes)
% 
M11_bc = M11;
K11_bc = K11;
K12_bc = K12;
for i=1:length(nodes)
    idx = nodes(i);
    M11_bc(idx,:)=0;
    M11_bc(idx,idx)=1;
    K11_bc(idx,:) = 0;
    K12_bc(idx,:) = 0;
end

end

