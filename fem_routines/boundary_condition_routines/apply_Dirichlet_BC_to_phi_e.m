function [K21_bc,K22_bc] = apply_Dirichlet_BC_to_phi_e(K21,K22,nodes)
% 
num_of_mus = size(K21,2);
% 
K22_bc = K22;
K21_bc = K21;
for i=1:length(nodes)
    idx = nodes(i);
    K22_bc(idx,:) = 0;
    K22_bc(idx,idx) = 1;
    for mu_idx=1:num_of_mus
        K21_bc{mu_idx}(idx,:) = 0;
    end
end

end

