function [M11,K11,K12,K21,K22] = assemble_FE_multi_domain_matrices(phi_mesh,vm_mesh,IPP,num_of_mus)
% Assemble the finite element matrices for the multi-domain equations.
%% 
i = zeros(1,64*size(phi_mesh.elements.phi_e_dofs,2));
j = zeros(1,64*size(phi_mesh.elements.phi_e_dofs,2));
v = zeros(1,64*size(phi_mesh.elements.phi_e_dofs,2));
idx = 0;
for elem_idx=1:size(phi_mesh.elements.phi_e_dofs,2)
    [i(idx+1:idx+64), j(idx+1:idx+64), v(idx+1:idx+64)] = ...
        elem_stiffness_matrix_vectorized(1,phi_mesh.parameters.sigma(:,:,elem_idx),...
        phi_mesh.elements.phi_e_dofs(:,elem_idx),squeeze(phi_mesh.elements.coordinates(:,elem_idx,:)),IPP);
    idx = idx + 64;
end
K22 = sparse(i,j,v,phi_mesh.dofs,phi_mesh.dofs);
%%
for mu_idx=1:num_of_mus
    i = zeros(1,64*size(vm_mesh(mu_idx).elements.vm_dofs,2));
    j = zeros(1,64*size(vm_mesh(mu_idx).elements.vm_dofs,2));
    v = zeros(1,64*size(vm_mesh(mu_idx).elements.vm_dofs,2));
    ii = zeros(1,64*size(vm_mesh(mu_idx).elements.vm_dofs,2));
    jj = zeros(1,64*size(vm_mesh(mu_idx).elements.vm_dofs,2));
    vv = zeros(1,64*size(vm_mesh(mu_idx).elements.vm_dofs,2));
    iii = zeros(1,64*size(vm_mesh(mu_idx).elements.vm_dofs,2));
    jjj = zeros(1,64*size(vm_mesh(mu_idx).elements.vm_dofs,2));
    vvv = zeros(1,64*size(vm_mesh(mu_idx).elements.vm_dofs,2));
    iiii = zeros(1,64*size(vm_mesh(mu_idx).elements.vm_dofs,2));
    jjjj = zeros(1,64*size(vm_mesh(mu_idx).elements.vm_dofs,2));
    vvvv = zeros(1,64*size(vm_mesh(mu_idx).elements.vm_dofs,2));
    idx = 0;
    for elem_idx=1:size(vm_mesh(mu_idx).elements.vm_dofs,2)
        [i(idx+1:idx+64), j(idx+1:idx+64), v(idx+1:idx+64)] = elem_mass_matrix(...
            vm_mesh(mu_idx).elements.vm_dofs(:,elem_idx),...
            squeeze(vm_mesh(mu_idx).elements.coordinates(:,elem_idx,:)));
        [ii(idx+1:idx+64), jj(idx+1:idx+64), vv(idx+1:idx+64)] = ...
            elem_stiffness_matrix_vectorized(vm_mesh(mu_idx).parameters.k_am_cm(elem_idx),...
            vm_mesh(mu_idx).parameters.sigma_i(:,:,elem_idx),vm_mesh(mu_idx).elements.vm_dofs(:,elem_idx),...
            squeeze(vm_mesh(mu_idx).elements.coordinates(:,elem_idx,:)),IPP);
        [iii(idx+1:idx+64), jjj(idx+1:idx+64), vvv(idx+1:idx+64)] = ...
            elem_multidomain_stiffness_coupling_matrix_vectorized(vm_mesh(mu_idx).parameters.k_am_cm(elem_idx),...
            vm_mesh(mu_idx).parameters.sigma_i(:,:,elem_idx),vm_mesh(mu_idx).elements.vm_dofs(:,elem_idx),...
            vm_mesh(mu_idx).elements.phi_e_dofs(:,elem_idx),...
            squeeze(vm_mesh(mu_idx).elements.coordinates(:,elem_idx,:)),IPP);
        [iiii(idx+1:idx+64), jjjj(idx+1:idx+64), vvvv(idx+1:idx+64)] = ...
            elem_multidomain_stiffness_coupling_matrix_vectorized(vm_mesh(mu_idx).parameters.f_r(elem_idx),...
            vm_mesh(mu_idx).parameters.sigma_i(:,:,elem_idx),vm_mesh(mu_idx).elements.phi_e_dofs(:,elem_idx),...
            vm_mesh(mu_idx).elements.vm_dofs(:,elem_idx),...
            squeeze(vm_mesh(mu_idx).elements.coordinates(:,elem_idx,:)),IPP);
        idx = idx+64;
    end
    M11{mu_idx} = sparse(i,j,v,vm_mesh(mu_idx).dofs,vm_mesh(mu_idx).dofs);
    K11{mu_idx} = sparse(ii,jj,vv,vm_mesh(mu_idx).dofs,vm_mesh(mu_idx).dofs);
    K12{mu_idx} = sparse(iii,jjj,vvv,vm_mesh(mu_idx).dofs,phi_mesh.dofs);
    K21{mu_idx} = sparse(iiii,jjjj,vvvv,phi_mesh.dofs,vm_mesh(mu_idx).dofs);
end
end

