function [A,B] = get_time_discretisized_multi_domain_equations(M11,K11,K12,K21,K22,dt,teta)
% Apply a temporal discretization scheme to the spatially discretized
% multi-domain equation. teta = 1: Implicit Euler, teta=0.5:
% Cranck-Nicholson
%   Detailed explanation goes here
if nargin == 6
    teta = 1; % Implicit Euler is the default
end

num_of_mus = size(K11,2);

A1 = [];
A2 = [];
A3 = [];
B1 = [];
B2 = [];
B3 = [];

for mu_idx=1:num_of_mus
    A1 = blkdiag(A1,M11{mu_idx}+teta.*dt.*K11{mu_idx}); 
    A2 = vertcat(A2,teta.*dt.*K12{mu_idx});
    A3 = horzcat(A3,teta.*K21{mu_idx});
    
    B1 = blkdiag(B1,M11{mu_idx}+(teta-1).*dt.*K11{mu_idx});
    B2 = vertcat(B2,(teta-1).*dt.*K12{mu_idx});
    B3 = horzcat(B3,(teta-1).*K21{mu_idx});
end
A = [A1,A2;A3,teta.*K22];
B = [B1,B2;B3,(teta-1).*K22];
end

