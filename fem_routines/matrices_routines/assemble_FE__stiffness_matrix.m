function [M] = assemble_FE__stiffness_matrix(fe_mesh,k,sigma,IPP)
% Assemble the finite element stiffness matrix for Int[k*sigma*grad(phi)*grad(w)]dx.
i = zeros(1,64*size(fe_mesh.nodes,2));
j = zeros(1,64*size(fe_mesh.nodes,2));
v = zeros(1,64*size(fe_mesh.nodes,2));
idx = 0;
for elem_idx=1:size(fe_mesh.nodes,2)
    [i(idx+1:idx+64), j(idx+1:idx+64), v(idx+1:idx+64)] = elem_stiffness_matrix_vectorized(k(elem),sigma(elem),fe_mesh.nodes(:,elem_idx),squeeze(fe_mesh.coordinates(:,elem_idx,:)),IPP);
    idx = idx + 64;
end
M = sparse(i,j,v,fe_mesh.dofs,fe_mesh.dofs);
end

