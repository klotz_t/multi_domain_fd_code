function [I_stim] = stimulus(firing_times,amplitude,stim_length,dt,num_of_dt)
% Calculates the stimulation vector for given firing times (in milliseconds)

I_stim = zeros(1,num_of_dt);
num_of_stims = length(firing_times);
num_of_stim_timesteps = round(stim_length/dt,0); 

if (num_of_stims==0)
    I_stim = zeros(1,num_of_dt); % bug fixed 
else
    for i=1:num_of_stims
        stim_start = round(firing_times(i)/dt,0)+1;
        if (stim_start<=num_of_dt)
            for t=stim_start:(stim_start+num_of_stim_timesteps)
                I_stim(t) = amplitude;
            end
        end
    end
end

end