function [Finv] = update_def_grad(var,time)
% Calculates the inverse of the Deformation Gradient Tensor for a
% prescribed dynamic contraction.

if (var{3} == 0) % -> Sinus Shape
    L1 = var{5}(1);
    L2 = var{5}(2);
    amplitude = (L1 - L2)/2;
    period    = var{4};
    lambda = (L1 - amplitude) + cos(2*pi*time/period)*amplitude; 
end
fprintf('Stretch: %d \n', lambda);

if (var{2} == 0) % -> Uniaxial Extension
    Finv = diag([1/lambda, sqrt(lambda), sqrt(lambda)]);
end

end