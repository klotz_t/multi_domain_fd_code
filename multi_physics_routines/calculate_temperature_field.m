function [ ] = calculate_temperature_field(filepath)
% Calculates the temperature field induced by (external) elctrical currents within a
% muscle. Therefore the heat equation is applied,
% i.e. dT/dt = k*laplace_operator(T) + Joule-heating - W*(T-T0). 
% The source terms induced by Joule-heating are calculated from the solution of the multi-domain eqs, i.e. 
% the predicted electrical current fields and the corresponding
% conductivities.
% Further, the last term accounts for blood perfusion.


%%
dt = 0.1; % [ms]
k  = 1.5e-6; % Termal Difussion Coefficient [cm^2/ms] -- from Giering et al 1995
C_h = 3e12; % heat capacity [pJ/(cm^3*K)] 
T0 = 37; % Body Temperature [°C]
W = 0; % Perfusion rafe [1/ms]

load (sprintf('%s/simple_example_info.mat',filepath)); 
num_of_dt = round(input.time_end / input.dt)/input.output_frequency;
mkdir (sprintf('%s/temperature_field/',filepath));
%%
if(isfield(input,'skin'))
    % Do Nothing
else
    input.skin = false;
end
if(isfield(input,'fat'))
    % Do Nothing
else
    input.skin = false;
end
%%
X = grid.X; 
Y = grid.Y;
if(input.skin)  
    Z = grid.Z + grid.Z_fat + grid.Z_skin -2;
elseif(input.fat)
    Z = grid.Z + grid.Z_fat -1;
else
    Z = grid.Z;
end
nodes = 1:X*Y*Z;
num_of_nodes = length(nodes);

A = calculate_temperature_field_eqs_fd_matrix(grid,input,dt,k,W);
b_elem = RHS_eliminate(grid,input);

temperature_field = T0.*ones(num_of_nodes,num_of_dt);
%%
rhs = zeros(num_of_nodes,1);
for i=1:num_of_dt-1
    fprintf('step #   : %d \n', i);
    tic
    load(sprintf('%s/simple_example_%d.mat',filepath,i-1));
    %
    b = calculate_temperature_field_eqs_rhs(xx,temperature_field(:,i),grid,input,dt,C_h,W*T0);
    rhs(:,1) = b_elem.*b; % Apply Neumann type BCs
    rhs(1:X*Y) = T0;    % Apply Dirichlet BC on bottom surface
    
    temperature_field(:,i+1) = A\rhs;
end


save(sprintf('%s/temperature_field/temperature_field.mat',filepath),'temperature_field')



end

function [b] = calculate_temperature_field_eqs_rhs(EF,TF,grid,input,dt,C_h,PT)
% Calculate the elctrical current density in space and map it to the right
% hand side vector of the equation system. (Note that currrently only the
% case of constant volume fractions is implemented).
% EF are the electrical potential field distributions (from the
% multi-domain equations),
% TF is the temperature filed from the previous time step,
% C_h is the heat capacity and
% PT=W*T0 results from the perfusion term.

X = grid.X; 
Y = grid.Y;
if(input.skin)  
    Z = grid.Z + grid.Z_fat + grid.Z_skin -2;
elseif(input.fat)
    Z = grid.Z + grid.Z_fat -1;
else
    Z = grid.Z;
end

dofs = X*Y*Z;
b = zeros(dofs,1);

num_of_muscle_nodes = grid.X*grid.Y*grid.Z;


jx = zeros(grid.X,grid.Y,grid.Z);
jy = zeros(grid.X,grid.Y,grid.Z);
jz = zeros(grid.X,grid.Y,grid.Z);
joule_heating = zeros(grid.X,grid.Y,grid.Z); % in [pJ/(ms*cm^3)]
for i=1:input.num_of_mus+1
    gg = reshape(EF((i-1)*num_of_muscle_nodes+1:i*num_of_muscle_nodes),[grid.X,grid.Y,grid.Z]);
    [gradY,gradX,gradZ] = gradient(gg,grid.dy,grid.dx,grid.dz);
    if(i<=input.num_of_mus)
        sigma = input.sigma_i;
        if(iscell(input.volume_fractions))
            f = input.volume_fractions{i};
        else
            f = input.volume_fractions(i);
        end
    else
        sigma = input.sigma_e + input.sigma_i;
        f = 1;
    end
    % Current is in \micro\ampere\per\square\centi\meter (µA/cm^2)
%     jx = f.*sigma(1,1).*gradX;
%     jy = f.*sigma(2,2).*gradY;
%     jz = f.*sigma(3,3).*gradZ;
    joule_heating = joule_heating + (f.*gradX).^2 .* sigma(1,1) + ...
        (f.*gradY).^2 .* sigma(2,2) + ...
        (f.*gradZ).^2 .* sigma(3,3); 
end
if(input.fat)
   num_of_fat_nodes = grid.X*grid.Y*grid.Z_fat;
   gg = reshape(EF(i*num_of_muscle_nodes+1:i*num_of_muscle_nodes+num_of_fat_nodes),[grid.X,grid.Y,grid.Z_fat]);
   [gradY,gradX,gradZ] = gradient(gg,grid.dy,grid.dx,grid.dz);
   gg = gradX.^2 .* input.sigma_o(1,1) + ...
       gradY.^2 .* input.sigma_o(2,2) + ...
       gradZ.^2 .* input.sigma_o(3,3);
   joule_heating = cat(3,joule_heating,gg(:,:,2:end));
end
if(input.skin)
   gg = reshape(EF(i*num_of_muscle_nodes+num_of_fat_nodes+1:end),[grid.X,grid.Y,grid.Z_skin]);
   [gradY,gradX,gradZ] = gradient(gg,grid.dy,grid.dx,grid.dz);
   gg = gradX.^2 .* input.sigma_s(1,1) + ...
       gradY.^2 .* input.sigma_s(2,2) + ...
       gradZ.^2 .* input.sigma_s(3,3);
   joule_heating = cat(3,joule_heating,gg(:,:,2:end));
end
b = dt./C_h.*reshape(joule_heating,[],1) + TF + PT; 
end

function b = RHS_eliminate(grid,input)
%Eliminate entries from the RHS vector in order to apply the BCs.
X = grid.X; 
Y = grid.Y;
if(input.skin)  
    Z = grid.Z + grid.Z_fat + grid.Z_skin -2;
elseif(input.fat)
    Z = grid.Z + grid.Z_fat -1;
else
    Z = grid.Z;
end

dofs = X*Y*Z;
nodes = 1:X*Y*Z;

nodes_3d = reshape(nodes,X,Y,Z);
num_of_nodes = length(nodes);

left = reshape(nodes_3d(1:1,1:Y,2:Z-1),[],1)';%(X*Y+1):X:(dofs-X*Y);
right = reshape(nodes_3d(X:X,1:Y,2:Z-1),[],1)';%(X*Y+X):X:(dofs-X*Y);
front = reshape(nodes_3d(2:X-1,1:1,2:Z-1),[],1)';
back = reshape(nodes_3d(2:X-1,Y:Y,2:Z-1),[],1)';
bottom = 1:1:(X*Y);
top = reshape(nodes_3d(1:X,1:Y,Z:Z),[],1)';%(dofs-X*Y+1):1:dofs;


b = ones(dofs,1);

% Apply BCs
% Front-Side
for i=1:length(front)
    ind = front(i);
    b(ind,:) = 0;
end
% Back-Side
for i=1:length(back)
    ind = back(i);
    b(ind,:) = 0;
end
% Left-Side
for i=1:length(left)
    ind = left(i);
    b(ind,:) = 0;
end
% Right-Side
for i=1:length(right)
    ind = right(i);
    b(ind,:) = 0;
end
% Top Side
for i=1:length(top)
    ind = top(i);
    b(ind,:) = 0;
end 
% % Bottom Side
% for i=1:length(bottom)
%     ind = bottom(i) + ind_offset;
%     b(ind,:) = 0;
% end
%
end

function [A] = calculate_temperature_field_eqs_fd_matrix(grid,input,dt,k,W)
%Calculate the FD matrix for the 3D heat equation and apply the boundary
%conditions.

X = grid.X; 
Y = grid.Y;
if(input.skin)  
    Z = grid.Z + grid.Z_fat + grid.Z_skin -2;
elseif(input.fat)
    Z = grid.Z + grid.Z_fat -1;
else
    Z = grid.Z;
end
dx = grid.dx;
dy = grid.dy;
dz = grid.dz;

dofs = X*Y*Z;
nodes = 1:X*Y*Z;
nodes_3d = reshape(nodes,X,Y,Z);

%% Get Surface and Innner Nodes (muscle + fat)
inner = reshape(nodes_3d(2:X-1,2:Y-1,2:Z-1),[],1)';
left = reshape(nodes_3d(1:1,1:Y,2:Z-1),[],1)';%(X*Y+1):X:(dofs-X*Y);
right = reshape(nodes_3d(X:X,1:Y,2:Z-1),[],1)';%(X*Y+X):X:(dofs-X*Y);
front = reshape(nodes_3d(2:X-1,1:1,2:Z-1),[],1)';
back = reshape(nodes_3d(2:X-1,Y:Y,2:Z-1),[],1)';
bottom = 1:1:(X*Y);
top = reshape(nodes_3d(1:X,1:Y,Z:Z),[],1)';%(dofs-X*Y+1):1:dofs;

num_of_bottom_nodes = length(bottom);
num_of_top_nodes = length(top);
num_of_left_nodes = length(left);
num_of_right_nodes = length(right);
num_of_back_nodes = length(back);
num_of_front_nodes = length(front);
num_of_inner_nodes = length(inner);


%% Initalise Coefficent Vectors that will generate the FD Matrix
% global_dofs = (n_of_mus+1)*dofs; % + X*Y*Z_fat;
i = zeros(1,dofs*7);
j = zeros(1,dofs*7);
v = zeros(1,dofs*7);

ind_count = 1;

fact_xx = k*dt/dx^2;
fact_yy = k*dt/dy^2;
fact_zz = k*dt/dz^2;
w0 = (1+W+2*fact_xx+2*fact_yy+2*fact_zz);
% diag 
i(ind_count:ind_count+num_of_inner_nodes-1) = inner;
j(ind_count:ind_count+num_of_inner_nodes-1) = inner;
v(ind_count:ind_count+num_of_inner_nodes-1) = w0;
ind_count = ind_count+num_of_inner_nodes;
% u_xx
i(ind_count:ind_count+num_of_inner_nodes-1) = inner;
j(ind_count:ind_count+num_of_inner_nodes-1) = inner + 1;
v(ind_count:ind_count+num_of_inner_nodes-1) = -fact_xx;
ind_count = ind_count+num_of_inner_nodes;
i(ind_count:ind_count+num_of_inner_nodes-1) = inner;
j(ind_count:ind_count+num_of_inner_nodes-1) = inner - 1;
v(ind_count:ind_count+num_of_inner_nodes-1) = -fact_xx;
ind_count = ind_count+num_of_inner_nodes;
% u_yy
i(ind_count:ind_count+num_of_inner_nodes-1) = inner;
j(ind_count:ind_count+num_of_inner_nodes-1) = inner + X;
v(ind_count:ind_count+num_of_inner_nodes-1) = -fact_yy;
ind_count = ind_count+num_of_inner_nodes;
i(ind_count:ind_count+num_of_inner_nodes-1) = inner;
j(ind_count:ind_count+num_of_inner_nodes-1) = inner - X;
v(ind_count:ind_count+num_of_inner_nodes-1) = -fact_yy;
ind_count = ind_count+num_of_inner_nodes;
% u_zz
i(ind_count:ind_count+num_of_inner_nodes-1) = inner;
j(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y;
v(ind_count:ind_count+num_of_inner_nodes-1) = -fact_zz;
ind_count = ind_count+num_of_inner_nodes;
i(ind_count:ind_count+num_of_inner_nodes-1) = inner;
j(ind_count:ind_count+num_of_inner_nodes-1) = inner - X*Y;
v(ind_count:ind_count+num_of_inner_nodes-1) = -fact_zz;
ind_count = ind_count+num_of_inner_nodes;
%% BCs left
% Neumann BC
i(ind_count:ind_count+num_of_left_nodes-1) = left;
j(ind_count:ind_count+num_of_left_nodes-1) = left;
v(ind_count:ind_count+num_of_left_nodes-1) = -1;
ind_count = ind_count+num_of_left_nodes;
i(ind_count:ind_count+num_of_left_nodes-1) = left;
j(ind_count:ind_count+num_of_left_nodes-1) = left + 1;
v(ind_count:ind_count+num_of_left_nodes-1) = 1;
ind_count = ind_count+num_of_left_nodes;
%% BCs right
% Neumann BC
i(ind_count:ind_count+num_of_right_nodes-1) = right;
j(ind_count:ind_count+num_of_right_nodes-1) = right;
v(ind_count:ind_count+num_of_right_nodes-1) = -1;
ind_count = ind_count+num_of_right_nodes;
i(ind_count:ind_count+num_of_right_nodes-1) = right;
j(ind_count:ind_count+num_of_right_nodes-1) = right - 1;
v(ind_count:ind_count+num_of_right_nodes-1) = 1;
ind_count = ind_count+num_of_right_nodes;
%% BCs bottom 
% Dirichlet BC
i(ind_count:ind_count+num_of_bottom_nodes-1) = bottom;
j(ind_count:ind_count+num_of_bottom_nodes-1) = bottom;
v(ind_count:ind_count+num_of_bottom_nodes-1) = 1;
ind_count = ind_count+num_of_bottom_nodes;
% i(ind_count:ind_count+num_of_bottom_nodes-1) = bottom + X*Y*Z*(co_ind-1);
% j(ind_count:ind_count+num_of_bottom_nodes-1) = bottom + X*Y*Z*(co_ind-1) + X*Y;
% v(ind_count:ind_count+num_of_bottom_nodes-1) = 1;
% ind_count = ind_count+num_of_bottom_nodes;
%% Top BC
% Neumann BC
i(ind_count:ind_count+num_of_top_nodes-1) = top;
j(ind_count:ind_count+num_of_top_nodes-1) = top;
v(ind_count:ind_count+num_of_top_nodes-1) = -1;
ind_count = ind_count+num_of_top_nodes;
i(ind_count:ind_count+num_of_top_nodes-1) = top;
j(ind_count:ind_count+num_of_top_nodes-1) = top - X*Y;
v(ind_count:ind_count+num_of_top_nodes-1) = 1;
ind_count = ind_count+num_of_top_nodes;
%% BCs front
% Neumann BC
i(ind_count:ind_count+num_of_front_nodes-1) = front;
j(ind_count:ind_count+num_of_front_nodes-1) = front;
v(ind_count:ind_count+num_of_front_nodes-1) = -1;
ind_count = ind_count+num_of_front_nodes;
i(ind_count:ind_count+num_of_front_nodes-1) = front;
j(ind_count:ind_count+num_of_front_nodes-1) = front + X;
v(ind_count:ind_count+num_of_front_nodes-1) = 1;
ind_count = ind_count+num_of_front_nodes;
%% BCs back
% Neumann BC
i(ind_count:ind_count+num_of_back_nodes-1) = back;
j(ind_count:ind_count+num_of_back_nodes-1) = back;
v(ind_count:ind_count+num_of_back_nodes-1) = -1;
ind_count = ind_count+num_of_back_nodes;
i(ind_count:ind_count+num_of_back_nodes-1) = back;
j(ind_count:ind_count+num_of_back_nodes-1) = back - X;
v(ind_count:ind_count+num_of_back_nodes-1) = 1;
ind_count = ind_count+num_of_back_nodes;


A = sparse(i(1:ind_count-1),j(1:ind_count-1),v(1:ind_count-1),dofs,dofs);

end

