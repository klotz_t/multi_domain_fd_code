function [ ] = calculate_magentic_B_field(filepath,points)
% Calculates the magnetic fields induced by elctrical currents within a
% muscle. Therefore the Biot-Savard Law is applied: 
% B(r) = mu_0/(4*pi)*Int[(J x r')/|r'|^3]dV
% The current density is calculated from  the solution of the multi-domain eqs, i.e. 
% J = \sum(f_r^k \sigma_i^k grad V_m^k) + \sigma_e grad \phi_e
% Further is a vector from the source current to the observation point r.

%% Physical constants --> mu = mu_0/(4*pi)
%mu0 = 4*pi*1e-14; % \volt\second\per\micro\ampere\per\centi\meter   (Vs/µA/cm)
mu = 1e-14; % \volt\second\per\micro\ampere\per\centi\meter
%% Load results of the multi-domain model and set up simulation output
load(sprintf('%s/simple_example_info.mat',filepath)); 
mkdir(sprintf('%s/magnetic_fields/',filepath));

% points = zeros(grid.Y,3);
% points(:,1) = 4.5;
% points(:,2) = 0:grid.dz:grid.width;
% points(:,3) = 2.1;
%% Extract data containing the spatial and temporal discretisation
num_of_dt = round(input.time_end / input.dt)/input.output_frequency;
num_of_nodes = grid.X*grid.Y;
%% Discretization
[Ax,Ay,Az] = get_matrices(grid,points,0);

if input.fat
    [Ax_fat,Ay_fat,Az_fat] = get_matrices(grid,points,1);
end

%%
Bx = zeros(size(points,1),num_of_dt);
By = zeros(size(points,1),num_of_dt);
Bz = zeros(size(points,1),num_of_dt);

for time=1:num_of_dt
    load(sprintf('%s/simple_example_%d.mat',filepath,time-1));
    % Current is in \micro\ampere\per\square\centi\meter (µA/cm^2)
    [jx,jy,jz] = calculate_magentic_field_eqs_rhs(xx,grid,input,0);
%     b = calculate_magentic_field_eqs_rhs_single_domain(xx,grid,input,3);
    Bx(:,time) = mu.*(Az*jy - Ay*jz);
    By(:,time) = mu.*(Ax*jz - Az*jx);
    Bz(:,time) = mu.*(Ay*jx - Ax*jy);
    if input.fat
        [jx,jy,jz] = calculate_magentic_field_eqs_rhs(xx,grid,input,1);
        Bx(:,time) = Bx(:,time) + mu.*(Az_fat*jy - Ay_fat*jz);
        By(:,time) = By(:,time) + mu.*(Ax_fat*jz - Az_fat*jx);
        Bz(:,time) = Bz(:,time) + mu.*(Ay_fat*jx - Ax_fat*jy);
    end
end

%% Store the magnetic flux desnity to file 
save(sprintf('%s/magnetic_fields/B_fields_integral_eq.mat',filepath),'Bx','By','Bz','points','-v7.3')


% Bx = sum(((jy.*(-point(3)+gridZ)) - (jz.*(-point(2)+gridY)))./l_values);
% By = sum(((jz.*(-point(1)+gridX)) - (jx.*(-point(3)+gridZ)))./l_values);
% Bz = sum(((jx.*(-point(2)+gridY)) - (jy.*(-point(1)+gridX)))./l_values);

%
end

function [jx,jy,jz] = calculate_magentic_field_eqs_rhs(xx,grid,input,tissue)
%Calculate the elctrical current density in space and map it to the right
%hand side vector of the equation system. (Note that currrently only the
%case of constant volume fractions is implemented)

nodes = 1:grid.X*grid.Y*grid.Z;
num_of_nodes = length(nodes);
if tissue == 0

    jx = zeros(grid.X,grid.Y,grid.Z);
    jy = zeros(grid.X,grid.Y,grid.Z);
    jz = zeros(grid.X,grid.Y,grid.Z);
    for i=1:input.num_of_mus+1
        gg = reshape(xx((i-1)*num_of_nodes+1:i*num_of_nodes),[grid.X,grid.Y,grid.Z]);
        if(i<=input.num_of_mus)
            gg = gg + reshape(xx((input.num_of_mus)*num_of_nodes+1:(input.num_of_mus+1)*num_of_nodes),[grid.X,grid.Y,grid.Z]);
        end
        [gradY,gradX,gradZ] = gradient(gg,grid.dy,grid.dx,grid.dz);
        if(i<=input.num_of_mus)
            sigma = input.sigma_i;
            if(iscell(input.volume_fractions))
                f = input.volume_fractions{i};
            else
                f = input.volume_fractions(i);
            end
        else
            sigma = input.sigma_e; % + input.sigma_i;
            f = 1;
        end
        % Current is in \micro\ampere\per\square\centi\meter (µA/cm^2)
        jx = jx - (f.*(sigma(1,1).*gradX + sigma(1,2).*gradY + sigma(1,3).*gradZ));
        jy = jy - (f.*(sigma(2,1).*gradX + sigma(2,2).*gradY + sigma(2,3).*gradZ));
        jz = jz - (f.*(sigma(3,1).*gradX + sigma(3,2).*gradY + sigma(3,3).*gradZ));
    end
    jx = reshape(jx,[],1); 
    jy = reshape(jy,[],1);
    jz = reshape(jz,[],1);
elseif tissue == 1
    %num_of_fat_nodes = grid.X*grid.Y*grid.Z_fat;
    gg = reshape(xx((input.num_of_mus+1)*num_of_nodes+1:end),[grid.X,grid.Y,grid.Z_fat]);
    [gradY,gradX,gradZ] = gradient(gg,grid.dy,grid.dx,grid.dz_fat);
    jx = -input.sigma_o(1,1).*gradX;
    jy = -input.sigma_o(2,2).*gradY;
    jz = -input.sigma_o(3,3).*gradZ;
    jx = reshape(jx,[],1); 
    jy = reshape(jy,[],1);
    jz = reshape(jz,[],1);
end


end

function [Ax, Ay, Az] = get_matrices(grid,points,tissue)
    %
    if tissue == 0 % Muscle
        volume = grid.dx*grid.dy*grid.dz;
        weight = reshape(get_weight(grid,tissue),[],1).*volume;
        x_values = linspace(0,grid.length,grid.X);
        y_values = linspace(0,grid.width,grid.Y);
        z_values = linspace(0,grid.height,grid.Z);
    elseif tissue == 1 % Fat
        volume = grid.dx*grid.dy*grid.dz_fat;
        weight = reshape(get_weight(grid,tissue),[],1).*(volume/8);
        x_values = linspace(0,grid.length,grid.X);
        y_values = linspace(0,grid.width,grid.Y);
        z_values = linspace(grid.height,grid.height,grid.Z_fat);
    end
    %
    [yy, xx, zz] = meshgrid(y_values,x_values,z_values);
    xx = reshape(xx,[],1);
    yy = reshape(yy,[],1);
    zz = reshape(zz,[],1);
    % 
    if tissue == 0 
        Ax = zeros(size(points,1),grid.X*grid.Y*grid.Z);
        Ay = zeros(size(points,1),grid.X*grid.Y*grid.Z);
        Az = zeros(size(points,1),grid.X*grid.Y*grid.Z);
    elseif tissue == 1
        Ax = zeros(size(points,1),grid.X*grid.Y*grid.Z_fat);
        Ay = zeros(size(points,1),grid.X*grid.Y*grid.Z_fat);
        Az = zeros(size(points,1),grid.X*grid.Y*grid.Z_fat);
    end
    for idx=1:size(points,1)
        l_values = ((-xx + points(idx,1)).^2 + ...
            (-yy + points(idx,2)).^2 + ...
            (-zz + points(idx,3)).^2).^(1/2).^3;
        Ax(idx,:) = weight.*(-points(idx,1)+xx)./l_values;
        Ay(idx,:) = weight.*(-points(idx,2)+yy)./l_values;
        Az(idx,:) = weight.*(-points(idx,3)+zz)./l_values;
    end
end

function [weight] = get_weight(grid,tissue)
    if tissue == 0
        weight = 8*ones(grid.X,grid.Y,grid.Z);
    elseif tissue == 1
        weight = 8*ones(grid.X,grid.Y,grid.Z_fat);
    end
    weight(1,:,:)     = 4;
    weight(end,:,:)   = 4;
    weight(:,1,:)     = 4;
    weight(:,end,:)   = 4;
    weight(:,:,1)     = 4;
    weight(:,:,end)   = 4;
    weight(1,1,:)     = 2;
    weight(1,end,:)   = 2;
    weight(end,1,:)   = 2;
    weight(end,end,:) = 2;
    weight(1,:,1)     = 2;
    weight(1,:,end)   = 2;
    weight(end,:,1)   = 2;
    weight(end,:,end) = 2;
    weight(:,1,1)     = 2;
    weight(:,end,1)   = 2;
    weight(:,1,end)   = 2;
    weight(:,end,end) = 2;
    weight(1,1,1)     = 1; 
    weight(end,1,1)   = 1;
    weight(1,1,end)   = 1;
    weight(end,1,end) = 1;
    weight(1,end,1)   = 1;
    weight(end,end,1) = 1;
    weight(1,end,end) = 1;
    weight(end,end,end) = 1;
end

