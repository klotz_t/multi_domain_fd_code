function [ ] = calculate_magentic_field_single_domain(filepath,domain_idx,RoI)
% Calculates the magnetic fields induced by elctrical currents within a
% muscle. Therefore the quasi-static version of Amperes-Law (Potential-Formulation) is applied,
% i.e. laplace_op A = - \mu * J . The right hand side is calculated from 
% the solution of the multi-domain eqs, i.e. 
% J = \sum(f_r^k \sigma_i^k grad V_m^k) + \sigma_e grad \phi_e


%% Physical constants
mu0 = 4*pi*1e-15; % \volt\second\per\micro\ampere\per\centi\meter   (Vs/µA/cm)
%% Load results of the multi-domain model and set up simulation output
load (sprintf('%s/simple_example_info.mat',filepath)); 
mkdir (sprintf('%s/magnetic_fields/',filepath));
%% Extract data containing the spatial and temporal discretisation
num_of_dt = round(input.time_end / input.dt)/input.output_frequency;
X = grid.X; 
Y = grid.Y;
if(input.fat)  
    Z = grid.Z + grid.Z_fat;
else
    Z = grid.Z;
end
nodes = 1:X*Y*Z;
num_of_nodes = length(nodes);
%% Set up the FD linear system
A = calculate_magentic_field_eqs_fd_matrix(grid,input);
b_elem = RHS_eliminate(grid,input);
%% Calculate RHS Vector for each time step
rhs = zeros(num_of_nodes*3,num_of_dt-1);
for time=1:num_of_dt-1
    load(sprintf('%s/simple_example_%d.mat',filepath,time-1));
    % Current is in \micro\ampere\per\square\centi\meter (µA/cm^2)
     %b = calculate_magentic_field_eqs_rhs(xx,grid,input);
     if nargin == 3
        b = calculate_magentic_field_eqs_rhs_single_domain(xx,grid,input,domain_idx,RoI); 
     else
        b = calculate_magentic_field_eqs_rhs_single_domain(xx,grid,input,domain_idx);
     end 
    rhs(:,time) = -mu0.*b_elem.*b;
end
%% Solve the linear system for the magnetic vector potential
tic
vector_potential = A\rhs; % The magnetic vector potential is in \volt\second\per\cubic\centi\meter (Vs/cm^3)
toc
%% Calculate the magnetic flux density from the magnetic vector potential
%  Prelocate output array
if input.fat % On the interface the magnetic vector potential is contieous --> remove one dimension
    Bx = zeros(X,Y,Z-1,num_of_dt-1);
    By = zeros(X,Y,Z-1,num_of_dt-1);
    Bz = zeros(X,Y,Z-1,num_of_dt-1);
else
    Bx = zeros(X,Y,Z,num_of_dt-1);
    By = zeros(X,Y,Z,num_of_dt-1);
    Bz = zeros(X,Y,Z,num_of_dt-1);
end
%  
for time=1:num_of_dt-1
    % Restructure the solution vector of the vector potential to calculate
    % the gradient field.
    num_of_muscle_nodes = X*Y*grid.Z;
    vector_potential_muscle = vector_potential(1:3*num_of_muscle_nodes,time);
    Ax = reshape(vector_potential_muscle(1:num_of_muscle_nodes),[X,Y,grid.Z]);
    Ay = reshape(vector_potential_muscle(num_of_muscle_nodes+1:2*num_of_muscle_nodes),[X,Y,grid.Z]);
    Az = reshape(vector_potential_muscle(2*num_of_muscle_nodes+1:3*num_of_muscle_nodes),[X,Y,grid.Z]);
    if(input.fat)  
        vector_potential_fat = vector_potential(3*num_of_muscle_nodes+1:end,time);
        num_of_fat_nodes = X*Y*grid.Z_fat;
        gg = reshape(vector_potential_fat(1:num_of_fat_nodes),[X,Y,grid.Z_fat]);
        Ax = cat(3,Ax,gg(:,:,2:end));
        gg = reshape(vector_potential_fat(num_of_fat_nodes+1:2*num_of_fat_nodes),[X,Y,grid.Z_fat]);
        Ay = cat(3,Ay,gg(:,:,2:end));
        gg = reshape(vector_potential_fat(2*num_of_fat_nodes+1:3*num_of_fat_nodes),[X,Y,grid.Z_fat]);
        Az = cat(3,Az,gg(:,:,2:end));
    end
    [gxy, ~, gxz] = gradient(Ax, grid.dy,grid.dx, grid.dz);
    [~, gyx, gyz] = gradient(Ay, grid.dy,grid.dx, grid.dz);
    [gzy, gzx, ~] = gradient(Az, grid.dy,grid.dx, grid.dz);
    % The resulting magnetic field is in
    % \volt\second\per\square\centi\meter (Vs/cm^2 --> 1e4 Tesla)
    Bx(:,:,:,time) = gzy - gyz;
    By(:,:,:,time) = gxz - gzx;
    Bz(:,:,:,time) = gyx - gxy;
end
%% Store the magnetic flux desnity to file 
save(sprintf('%s/magnetic_fields/B_fields_mu%d.mat',filepath,domain_idx),'Bx','By','Bz')
%
end

function [b] = calculate_magentic_field_eqs_rhs(xx,grid,input)
%Calculate the elctrical current density in space and map it to the right
%hand side vector of the equation system. (Note that currrently only the
%case of constant volume fractions is implemented)

dofs = 3*grid.X*grid.Y*grid.Z;
nodes = 1:grid.X*grid.Y*grid.Z;
num_of_nodes = length(nodes);

b = zeros(dofs,1);
jx = zeros(grid.X,grid.Y,grid.Z);
jy = zeros(grid.X,grid.Y,grid.Z);
jz = zeros(grid.X,grid.Y,grid.Z);
for i=1:input.num_of_mus+1
    gg = reshape(xx((i-1)*num_of_nodes+1:i*num_of_nodes),[grid.X,grid.Y,grid.Z]);
    [gradY,gradX,gradZ] = gradient(gg,grid.dy,grid.dx,grid.dz);
    if(i<=input.num_of_mus)
        sigma = input.sigma_i;
        if(iscell(input.volume_fractions))
            f = input.volume_fractions{i};
        else
            f = input.volume_fractions(i);
        end
    else
        sigma = input.sigma_e + input.sigma_i;
        f = 1;
    end
    % Current is in \micro\ampere\per\square\centi\meter (µA/cm^2)
    jx = jx + f.*sigma(1,1).*gradX;
    jy = jy + f.*sigma(2,2).*gradY;
    jz = jz + f.*sigma(3,3).*gradZ;
    b(1:num_of_nodes) = reshape(jx,[],1); 
    b(num_of_nodes+1:2*num_of_nodes) = reshape(jy,[],1);
    b(2*num_of_nodes+1:3*num_of_nodes) = reshape(jz,[],1);
end
if(input.fat)
    num_of_fat_nodes = grid.X*grid.Y*grid.Z_fat;
    b_fat = zeros(3*num_of_fat_nodes,1); 
    gg = reshape(xx(i*num_of_nodes+1:end),[grid.X,grid.Y,grid.Z_fat]);
    [gradY,gradX,gradZ] = gradient(gg,grid.dy,grid.dx,grid.dz);
    jx = input.sigma_o(1,1).*gradX;
    jy = input.sigma_o(2,2).*gradY;
    jz = input.sigma_o(3,3).*gradZ;
    b_fat(1:num_of_fat_nodes) = reshape(jx,[],1); 
    b_fat(num_of_fat_nodes+1:2*num_of_fat_nodes) = reshape(jy,[],1);
    b_fat(2*num_of_fat_nodes+1:3*num_of_fat_nodes) = reshape(jz,[],1);
    b = vertcat(b,b_fat);
end

end

function [b] = calculate_magentic_field_eqs_rhs_single_domain(xx,grid,input,domain_idx,RoI)
%Calculate the elctrical current density in space and map it to the right
%hand side vector of the equation system. (Note that currrently only the
%case of constant volume fractions is implemented)

dofs = 3*grid.X*grid.Y*grid.Z;
nodes = 1:grid.X*grid.Y*grid.Z;
num_of_nodes = length(nodes);

b = zeros(dofs,1);
jx = zeros(grid.X,grid.Y,grid.Z);
jy = zeros(grid.X,grid.Y,grid.Z);
jz = zeros(grid.X,grid.Y,grid.Z);

if(domain_idx<=input.num_of_mus)
    % Get the intracellular potential, i.e., phi_i = Vm + phi_e
    gg = reshape(xx((domain_idx-1)*num_of_nodes+1:domain_idx*num_of_nodes),[grid.X,grid.Y,grid.Z]) + ...
        reshape(xx(input.num_of_mus*num_of_nodes+1:(input.num_of_mus+1)*num_of_nodes),[grid.X,grid.Y,grid.Z]);
%      hh = zeros(grid.X,grid.Y,grid.Z);
%      hh(:,16,31) = 1;
    if nargin == 5 % only conisders a Region of interesst (RoI)
        gg = RoI.*gg;
    end    
    [gradY,gradX,gradZ] = gradient(gg,grid.dy,grid.dx,grid.dz);
    sigma = input.sigma_i;
    if(iscell(input.volume_fractions))
        f = input.volume_fractions{domain_idx};
    else
        f = input.volume_fractions(domain_idx);
    end
else
    gg = reshape(xx(input.num_of_mus*num_of_nodes+1:(input.num_of_mus+1)*num_of_nodes),[grid.X,grid.Y,grid.Z]);
    if nargin == 5 % only conisders a Region of interesst (RoI)
        gg = RoI.*gg;
    end 
    [gradY,gradX,gradZ] = gradient(gg,grid.dy,grid.dx,grid.dz); 
    sigma = input.sigma_e;
    f = 1;
end
% Current is in \micro\ampere\per\square\centi\meter (µA/cm^2)
jx = jx + f.*sigma(1,1).*(gradX);
jy = jy + f.*sigma(2,2).*(gradY);
jz = jz + f.*sigma(3,3).*(gradZ); 
b(1:num_of_nodes) = reshape(jx,[],1); 
b(num_of_nodes+1:2*num_of_nodes) = reshape(jy,[],1);
b(2*num_of_nodes+1:3*num_of_nodes) = reshape(jz,[],1);


if(input.fat)
    num_of_fat_nodes = grid.X*grid.Y*grid.Z_fat;
    b_fat = zeros(3*num_of_fat_nodes,1); 
    if (domain_idx > input.num_of_mus + 1)
        gg = reshape(xx((input.num_of_mus+1)*num_of_nodes+1:end),[grid.X,grid.Y,grid.Z_fat]);
        [gradY,gradX,gradZ] = gradient(gg,grid.dy,grid.dx,grid.dz);
        jx = input.sigma_o(1,1).*gradX;
        jy = input.sigma_o(2,2).*gradY;
        jz = input.sigma_o(3,3).*gradZ;
        b_fat(1:num_of_fat_nodes) = reshape(jx,[],1); 
        b_fat(num_of_fat_nodes+1:2*num_of_fat_nodes) = reshape(jy,[],1);
        b_fat(2*num_of_fat_nodes+1:3*num_of_fat_nodes) = reshape(jz,[],1);
    end
    b = vertcat(b,b_fat);
end

end

function b = RHS_eliminate(grid,input)
%Eliminate entries from the RHS vector in order to apply the BCs.
X = grid.X; 
Y = grid.Y; 
Z = grid.Z;

dofs = 3*X*Y*Z;
nodes = 1:X*Y*Z;

nodes_3d = reshape(nodes,X,Y,Z);
num_of_nodes = length(nodes);

left = reshape(nodes_3d(1:1,1:Y,2:Z-1),[],1)';%(X*Y+1):X:(dofs-X*Y);
right = reshape(nodes_3d(X:X,1:Y,2:Z-1),[],1)';%(X*Y+X):X:(dofs-X*Y);
front = reshape(nodes_3d(2:X-1,1:1,2:Z-1),[],1)';
back = reshape(nodes_3d(2:X-1,Y:Y,2:Z-1),[],1)';
bottom = 1:1:(X*Y);
top = reshape(nodes_3d(1:X,1:Y,Z:Z),[],1)';%(dofs-X*Y+1):1:dofs;


b = ones(dofs,1);

for dof_ind=1:3
    ind_offset = (dof_ind-1)*num_of_nodes ;
    % Apply BCs
    % Front-Side
    for i=1:length(front)
        ind = front(i) + ind_offset;
        b(ind,:) = 0;
    end
    % Back-Side
    for i=1:length(back)
        ind = back(i) + ind_offset;
        b(ind,:) = 0;
    end
    % Left-Side
    for i=1:length(left)
        ind = left(i) + ind_offset;
        b(ind,:) = 0;
    end
    % Right-Side
    for i=1:length(right)
        ind = right(i) + ind_offset;
        b(ind,:) = 0;
    end
    % Bottom Side
    for i=1:length(bottom)
        ind = bottom(i) + ind_offset;
        b(ind,:) = 0;
    end
    % Top Side
    for i=1:length(top)
        ind = top(i) + ind_offset;
        b(ind,:) = 0;
    end  
end
%
if(input.fat)
    Z = grid.Z_fat;
    dofs = 3*X*Y*Z;
    nodes = 1:X*Y*Z;

    nodes_3d = reshape(nodes,X,Y,Z);
    num_of_nodes = length(nodes);

    left = reshape(nodes_3d(1:1,1:Y,2:Z-1),[],1)';%(X*Y+1):X:(dofs-X*Y);
    right = reshape(nodes_3d(X:X,1:Y,2:Z-1),[],1)';%(X*Y+X):X:(dofs-X*Y);
    front = reshape(nodes_3d(2:X-1,1:1,2:Z-1),[],1)';
    back = reshape(nodes_3d(2:X-1,Y:Y,2:Z-1),[],1)';
    bottom = 1:1:(X*Y);
    top = reshape(nodes_3d(1:X,1:Y,Z:Z),[],1)';%(dofs-X*Y+1):1:dofs;


    b_fat = ones(dofs,1);
    for dof_ind=1:3
        ind_offset = (dof_ind-1)*num_of_nodes ;
        % Apply BCs
        % Front-Side
        for i=1:length(front)
            ind = front(i) + ind_offset;
            b_fat(ind,:) = 0;
        end
        % Back-Side
        for i=1:length(back)
            ind = back(i) + ind_offset;
            b_fat(ind,:) = 0;
        end
        % Left-Side
        for i=1:length(left)
            ind = left(i) + ind_offset;
            b_fat(ind,:) = 0;
        end
        % Right-Side
        for i=1:length(right)
            ind = right(i) + ind_offset;
            b_fat(ind,:) = 0;
        end
        % Bottom Side
        for i=1:length(bottom)
            ind = bottom(i) + ind_offset;
            b_fat(ind,:) = 0;
        end
        % Top Side
        for i=1:length(top)
            ind = top(i) + ind_offset;
            b_fat(ind,:) = 0;
        end  
    end
    b = [b; b_fat];
end

end

function [A] = calculate_magentic_field_eqs_fd_matrix(grid,input)
%Calculate the FD matrix for the 3D Poisson equation and apply the boundary
%conditions.

X = grid.X;
Y = grid.Y;
Z = grid.Z;
% if(input.fat)
%     Z = Z + grid.Z_fat -1;
% end
dx = grid.dx;
dy = grid.dy;
dz = grid.dz;

dofs = 3*X*Y*Z;
nodes = 1:X*Y*Z;
nodes_3d = reshape(nodes,X,Y,Z);

%% Get Surface and Innner Nodes (muscle + fat)
inner = reshape(nodes_3d(2:X-1,2:Y-1,2:Z-1),[],1)';
left = reshape(nodes_3d(1:1,1:Y,2:Z-1),[],1)';%(X*Y+1):X:(dofs-X*Y);
right = reshape(nodes_3d(X:X,1:Y,2:Z-1),[],1)';%(X*Y+X):X:(dofs-X*Y);
front = reshape(nodes_3d(2:X-1,1:1,2:Z-1),[],1)';
back = reshape(nodes_3d(2:X-1,Y:Y,2:Z-1),[],1)';
bottom = 1:1:(X*Y);
top_interface = reshape(nodes_3d(1:X,1:Y,Z:Z),[],1)';%(dofs-X*Y+1):1:dofs;

num_of_bottom_nodes = length(bottom);
num_of_top_interface_nodes = length(top_interface);
num_of_left_nodes = length(left);
num_of_right_nodes = length(right);
num_of_back_nodes = length(back);
num_of_front_nodes = length(front);
num_of_inner_nodes = length(inner);


%% Initalise Coefficent Vectors that will generate the FD Matrix
% global_dofs = (n_of_mus+1)*dofs; % + X*Y*Z_fat;
i = zeros(1,dofs*7);
j = zeros(1,dofs*7);
v = zeros(1,dofs*7);

ind_count = 1;
for co_ind=1:3
    fact_xx = 1/dx^2;
    fact_yy = 1/dy^2;
    fact_zz = 1/dz^2;
    w0 = -2*(fact_xx+fact_yy+fact_zz);
    % diag 
    i(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1);
    v(ind_count:ind_count+num_of_inner_nodes-1) = w0;
    ind_count = ind_count+num_of_inner_nodes;
    % u_xx
    i(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1) + 1;
    v(ind_count:ind_count+num_of_inner_nodes-1) = fact_xx;
    ind_count = ind_count+num_of_inner_nodes;
    i(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1) - 1;
    v(ind_count:ind_count+num_of_inner_nodes-1) = fact_xx;
    ind_count = ind_count+num_of_inner_nodes;
    % u_yy
    i(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1) ;
    j(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1) + X;
    v(ind_count:ind_count+num_of_inner_nodes-1) = fact_yy;
    ind_count = ind_count+num_of_inner_nodes;
    i(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1) - X;
    v(ind_count:ind_count+num_of_inner_nodes-1) = fact_yy;
    ind_count = ind_count+num_of_inner_nodes;
    % u_zz
    i(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1) + X*Y;
    v(ind_count:ind_count+num_of_inner_nodes-1) = fact_zz;
    ind_count = ind_count+num_of_inner_nodes;
    i(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1) - X*Y;
    v(ind_count:ind_count+num_of_inner_nodes-1) = fact_zz;
    ind_count = ind_count+num_of_inner_nodes;
    %% BCs left
    % Note that for the BCs infinite boundary elements are assumed
    i(ind_count:ind_count+num_of_left_nodes-1) = left + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_left_nodes-1) = left + X*Y*Z*(co_ind-1);
    v(ind_count:ind_count+num_of_left_nodes-1) = 3/2;
    ind_count = ind_count+num_of_left_nodes;
    i(ind_count:ind_count+num_of_left_nodes-1) = left + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_left_nodes-1) = left + X*Y*Z*(co_ind-1) + 1;
    v(ind_count:ind_count+num_of_left_nodes-1) = -2;
    ind_count = ind_count+num_of_left_nodes;
    i(ind_count:ind_count+num_of_left_nodes-1) = left + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_left_nodes-1) = left + X*Y*Z*(co_ind-1) + 2;
    v(ind_count:ind_count+num_of_left_nodes-1) = 1/2;
    ind_count = ind_count+num_of_left_nodes;
    %% BCs right
    i(ind_count:ind_count+num_of_right_nodes-1) = right + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_right_nodes-1) = right + X*Y*Z*(co_ind-1);
    v(ind_count:ind_count+num_of_right_nodes-1) = 3/2;
    ind_count = ind_count+num_of_right_nodes;
    i(ind_count:ind_count+num_of_right_nodes-1) = right + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_right_nodes-1) = right + X*Y*Z*(co_ind-1) - 1;
    v(ind_count:ind_count+num_of_right_nodes-1) = -2;
    ind_count = ind_count+num_of_right_nodes;
    i(ind_count:ind_count+num_of_right_nodes-1) = right + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_right_nodes-1) = right + X*Y*Z*(co_ind-1) - 2;
    v(ind_count:ind_count+num_of_right_nodes-1) = 1/2;
    ind_count = ind_count+num_of_right_nodes;
    %% BCs bottom 
    i(ind_count:ind_count+num_of_bottom_nodes-1) = bottom + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_bottom_nodes-1) = bottom + X*Y*Z*(co_ind-1);
    v(ind_count:ind_count+num_of_bottom_nodes-1) = 3/2;
    ind_count = ind_count+num_of_bottom_nodes;
    i(ind_count:ind_count+num_of_bottom_nodes-1) = bottom + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_bottom_nodes-1) = bottom + X*Y*Z*(co_ind-1) + X*Y;
    v(ind_count:ind_count+num_of_bottom_nodes-1) = -2;
    ind_count = ind_count+num_of_bottom_nodes;
    i(ind_count:ind_count+num_of_bottom_nodes-1) = bottom + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_bottom_nodes-1) = bottom + X*Y*Z*(co_ind-1) + 2*X*Y;
    v(ind_count:ind_count+num_of_bottom_nodes-1) = 1/2;
    ind_count = ind_count+num_of_bottom_nodes;
    %% Top BC 
    i(ind_count:ind_count+num_of_top_interface_nodes-1) = top_interface + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_top_interface_nodes-1) = top_interface + X*Y*Z*(co_ind-1);
    v(ind_count:ind_count+num_of_top_interface_nodes-1) = 3/2;
    ind_count = ind_count+num_of_top_interface_nodes;
    i(ind_count:ind_count+num_of_top_interface_nodes-1) = top_interface + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_top_interface_nodes-1) = top_interface + X*Y*Z*(co_ind-1) - X*Y;
    v(ind_count:ind_count+num_of_top_interface_nodes-1) = -2;
    ind_count = ind_count+num_of_top_interface_nodes;
    i(ind_count:ind_count+num_of_top_interface_nodes-1) = top_interface + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_top_interface_nodes-1) = top_interface + X*Y*Z*(co_ind-1) - 2*X*Y;
    v(ind_count:ind_count+num_of_top_interface_nodes-1) = 1/2;
    ind_count = ind_count+num_of_top_interface_nodes;
    %% BCs front
    i(ind_count:ind_count+num_of_front_nodes-1) = front + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_front_nodes-1) = front + X*Y*Z*(co_ind-1);
    v(ind_count:ind_count+num_of_front_nodes-1) = 3/2;
    ind_count = ind_count+num_of_front_nodes;
    i(ind_count:ind_count+num_of_front_nodes-1) = front + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_front_nodes-1) = front + X*Y*Z*(co_ind-1) + X;
    v(ind_count:ind_count+num_of_front_nodes-1) = -2;
    ind_count = ind_count+num_of_front_nodes;
    i(ind_count:ind_count+num_of_front_nodes-1) = front + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_front_nodes-1) = front + X*Y*Z*(co_ind-1) + 2*X;
    v(ind_count:ind_count+num_of_front_nodes-1) = 1/2;
    ind_count = ind_count+num_of_front_nodes;
    %% BCs back
    i(ind_count:ind_count+num_of_back_nodes-1) = back + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_back_nodes-1) = back + X*Y*Z*(co_ind-1);
    v(ind_count:ind_count+num_of_back_nodes-1) = 3/2;
    ind_count = ind_count+num_of_back_nodes;
    i(ind_count:ind_count+num_of_back_nodes-1) = back + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_back_nodes-1) = back + X*Y*Z*(co_ind-1) - X;
    v(ind_count:ind_count+num_of_back_nodes-1) = -2;
    ind_count = ind_count+num_of_back_nodes;
    i(ind_count:ind_count+num_of_back_nodes-1) = back + X*Y*Z*(co_ind-1);
    j(ind_count:ind_count+num_of_back_nodes-1) = back + X*Y*Z*(co_ind-1) - 2*X;
    v(ind_count:ind_count+num_of_back_nodes-1) = 1/2;
    ind_count = ind_count+num_of_back_nodes;
end


A = sparse(i(1:ind_count-1),j(1:ind_count-1),v(1:ind_count-1),dofs,dofs);

if(input.fat)
    X = grid.X;
    Y = grid.Y;
    Z = grid.Z_fat;
    % if(input.fat)
    %     Z = Z + grid.Z_fat -1;
    % end
    dx = grid.dx;
    dy = grid.dy;
    dz = grid.dz_fat;

    muscle_nodes = nodes;
    muscle_dofs  = dofs;
    dofs = 3*X*Y*Z;
    nodes = 1:X*Y*Z;
    nodes_3d = reshape(nodes,X,Y,Z);

    %% Get Surface and Innner Nodes (muscle + fat)
    inner = reshape(nodes_3d(2:X-1,2:Y-1,2:Z-1),[],1)';
    left = reshape(nodes_3d(1:1,1:Y,2:Z-1),[],1)';%(X*Y+1):X:(dofs-X*Y);
    right = reshape(nodes_3d(X:X,1:Y,2:Z-1),[],1)';%(X*Y+X):X:(dofs-X*Y);
    front = reshape(nodes_3d(2:X-1,1:1,2:Z-1),[],1)';
    back = reshape(nodes_3d(2:X-1,Y:Y,2:Z-1),[],1)';
    bottom_interface = 1:1:(X*Y);
    top = reshape(nodes_3d(1:X,1:Y,Z:Z),[],1)';%(dofs-X*Y+1):1:dofs;

    num_of_bottom_nodes = length(bottom_interface);
    num_of_top_nodes = length(top);
    num_of_left_nodes = length(left);
    num_of_right_nodes = length(right);
    num_of_back_nodes = length(back);
    num_of_front_nodes = length(front);
    num_of_inner_nodes = length(inner);


    %% Initalise Coefficent Vectors that will generate the FD Matrix
    % global_dofs = (n_of_mus+1)*dofs; % + X*Y*Z_fat;
    i = zeros(1,dofs*7);
    j = zeros(1,dofs*7);
    v = zeros(1,dofs*7);

    ind_count = 1;
    for co_ind=1:3
        fact_xx = 1/dx^2;
        fact_yy = 1/dy^2;
        fact_zz = 1/dz^2;
        w0 = -2*(fact_xx+fact_yy+fact_zz);
        % diag 
        i(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1);
        j(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1);
        v(ind_count:ind_count+num_of_inner_nodes-1) = w0;
        ind_count = ind_count+num_of_inner_nodes;
        % u_xx
        i(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1);
        j(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1) + 1;
        v(ind_count:ind_count+num_of_inner_nodes-1) = fact_xx;
        ind_count = ind_count+num_of_inner_nodes;
        i(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1);
        j(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1) - 1;
        v(ind_count:ind_count+num_of_inner_nodes-1) = fact_xx;
        ind_count = ind_count+num_of_inner_nodes;
        % u_yy
        i(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1) ;
        j(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1) + X;
        v(ind_count:ind_count+num_of_inner_nodes-1) = fact_yy;
        ind_count = ind_count+num_of_inner_nodes;
        i(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1);
        j(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1) - X;
        v(ind_count:ind_count+num_of_inner_nodes-1) = fact_yy;
        ind_count = ind_count+num_of_inner_nodes;
        % u_zz
        i(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1);
        j(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1) + X*Y;
        v(ind_count:ind_count+num_of_inner_nodes-1) = fact_zz;
        ind_count = ind_count+num_of_inner_nodes;
        i(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1);
        j(ind_count:ind_count+num_of_inner_nodes-1) = inner + X*Y*Z*(co_ind-1) - X*Y;
        v(ind_count:ind_count+num_of_inner_nodes-1) = fact_zz;
        ind_count = ind_count+num_of_inner_nodes;
        %% BCs left
        % Note that for the BCs infinite boundary elements are assumed
        i(ind_count:ind_count+num_of_left_nodes-1) = left + X*Y*Z*(co_ind-1);
        j(ind_count:ind_count+num_of_left_nodes-1) = left + X*Y*Z*(co_ind-1);
        v(ind_count:ind_count+num_of_left_nodes-1) = 3/2;
        ind_count = ind_count+num_of_left_nodes;
        i(ind_count:ind_count+num_of_left_nodes-1) = left + X*Y*Z*(co_ind-1);
        j(ind_count:ind_count+num_of_left_nodes-1) = left + X*Y*Z*(co_ind-1) + 1;
        v(ind_count:ind_count+num_of_left_nodes-1) = -2;
        ind_count = ind_count+num_of_left_nodes;
        i(ind_count:ind_count+num_of_left_nodes-1) = left + X*Y*Z*(co_ind-1);
        j(ind_count:ind_count+num_of_left_nodes-1) = left + X*Y*Z*(co_ind-1) + 2;
        v(ind_count:ind_count+num_of_left_nodes-1) = 1/2;
        ind_count = ind_count+num_of_left_nodes;
        %% BCs right
        i(ind_count:ind_count+num_of_right_nodes-1) = right + X*Y*Z*(co_ind-1);
        j(ind_count:ind_count+num_of_right_nodes-1) = right + X*Y*Z*(co_ind-1);
        v(ind_count:ind_count+num_of_right_nodes-1) = 3/2;
        ind_count = ind_count+num_of_right_nodes;
        i(ind_count:ind_count+num_of_right_nodes-1) = right + X*Y*Z*(co_ind-1);
        j(ind_count:ind_count+num_of_right_nodes-1) = right + X*Y*Z*(co_ind-1) - 1;
        v(ind_count:ind_count+num_of_right_nodes-1) = -2;
        ind_count = ind_count+num_of_right_nodes;
        i(ind_count:ind_count+num_of_right_nodes-1) = right + X*Y*Z*(co_ind-1);
        j(ind_count:ind_count+num_of_right_nodes-1) = right + X*Y*Z*(co_ind-1) - 2;
        v(ind_count:ind_count+num_of_right_nodes-1) = 1/2;
        ind_count = ind_count+num_of_right_nodes;
        %% BCs bottom 
        i(ind_count:ind_count+num_of_bottom_nodes-1) = bottom_interface + X*Y*Z*(co_ind-1);
        j(ind_count:ind_count+num_of_bottom_nodes-1) = bottom_interface + X*Y*Z*(co_ind-1);
        v(ind_count:ind_count+num_of_bottom_nodes-1) = 1;
        ind_count = ind_count+num_of_bottom_nodes;
        %% Top BC 
        i(ind_count:ind_count+num_of_top_nodes-1) = top + X*Y*Z*(co_ind-1);
        j(ind_count:ind_count+num_of_top_nodes-1) = top + X*Y*Z*(co_ind-1);
        v(ind_count:ind_count+num_of_top_nodes-1) = 3/2;
        ind_count = ind_count+num_of_top_nodes;
        i(ind_count:ind_count+num_of_top_nodes-1) = top + X*Y*Z*(co_ind-1);
        j(ind_count:ind_count+num_of_top_nodes-1) = top + X*Y*Z*(co_ind-1) - X*Y;
        v(ind_count:ind_count+num_of_top_nodes-1) = -2;
        ind_count = ind_count+num_of_top_nodes;
        i(ind_count:ind_count+num_of_top_nodes-1) = top + X*Y*Z*(co_ind-1);
        j(ind_count:ind_count+num_of_top_nodes-1) = top + X*Y*Z*(co_ind-1) - 2*X*Y;
        v(ind_count:ind_count+num_of_top_nodes-1) = 1/2;
        ind_count = ind_count+num_of_top_nodes;
        %% BCs front
        i(ind_count:ind_count+num_of_front_nodes-1) = front + X*Y*Z*(co_ind-1);
        j(ind_count:ind_count+num_of_front_nodes-1) = front + X*Y*Z*(co_ind-1);
        v(ind_count:ind_count+num_of_front_nodes-1) = 3/2;
        ind_count = ind_count+num_of_front_nodes;
        i(ind_count:ind_count+num_of_front_nodes-1) = front + X*Y*Z*(co_ind-1);
        j(ind_count:ind_count+num_of_front_nodes-1) = front + X*Y*Z*(co_ind-1) + X;
        v(ind_count:ind_count+num_of_front_nodes-1) = -2;
        ind_count = ind_count+num_of_front_nodes;
        i(ind_count:ind_count+num_of_front_nodes-1) = front + X*Y*Z*(co_ind-1);
        j(ind_count:ind_count+num_of_front_nodes-1) = front + X*Y*Z*(co_ind-1) + 2*X;
        v(ind_count:ind_count+num_of_front_nodes-1) = 1/2;
        ind_count = ind_count+num_of_front_nodes;
        %% BCs back
        i(ind_count:ind_count+num_of_back_nodes-1) = back + X*Y*Z*(co_ind-1);
        j(ind_count:ind_count+num_of_back_nodes-1) = back + X*Y*Z*(co_ind-1);
        v(ind_count:ind_count+num_of_back_nodes-1) = 3/2;
        ind_count = ind_count+num_of_back_nodes;
        i(ind_count:ind_count+num_of_back_nodes-1) = back + X*Y*Z*(co_ind-1);
        j(ind_count:ind_count+num_of_back_nodes-1) = back + X*Y*Z*(co_ind-1) - X;
        v(ind_count:ind_count+num_of_back_nodes-1) = -2;
        ind_count = ind_count+num_of_back_nodes;
        i(ind_count:ind_count+num_of_back_nodes-1) = back + X*Y*Z*(co_ind-1);
        j(ind_count:ind_count+num_of_back_nodes-1) = back + X*Y*Z*(co_ind-1) - 2*X;
        v(ind_count:ind_count+num_of_back_nodes-1) = 1/2;
        ind_count = ind_count+num_of_back_nodes;
    end
    B = sparse(i(1:ind_count-1),j(1:ind_count-1),v(1:ind_count-1),dofs,dofs);
    %
    C21 = sparse(bottom_interface,top_interface,-1,dofs,muscle_dofs) + ... 
        sparse(bottom_interface+dofs/3,top_interface+muscle_dofs/3,-1,dofs,muscle_dofs) + ...
        sparse(bottom_interface+2*dofs/3,top_interface+2*muscle_dofs/3,-1,dofs,muscle_dofs);
    %
    C12 = sparse(top_interface,bottom_interface,3/2,muscle_dofs,dofs) + ...
        sparse(top_interface,bottom_interface + X*Y ,-2,muscle_dofs,dofs) + ...
        sparse(top_interface,bottom_interface + 2*X*Y,+1/2,muscle_dofs,dofs) + ...
        sparse(top_interface + muscle_dofs/3,bottom_interface + dofs/3,+3/2,muscle_dofs,dofs) + ...
        sparse(top_interface + muscle_dofs/3,bottom_interface + dofs/3 + X*Y ,-2,muscle_dofs,dofs) + ...
        sparse(top_interface + muscle_dofs/3,bottom_interface + dofs/3 + 2*X*Y,+1/2,muscle_dofs,dofs) + ...
        sparse(top_interface + 2*muscle_dofs/3,bottom_interface + 2*dofs/3,+3/2,muscle_dofs,dofs) + ...
        sparse(top_interface + 2*muscle_dofs/3,bottom_interface + 2*dofs/3 + X*Y ,-2,muscle_dofs,dofs) + ...
        sparse(top_interface + 2*muscle_dofs/3,bottom_interface + 2*dofs/3 + 2*X*Y,+1/2,muscle_dofs,dofs);
    %
    A = [A,C12;C21,B];
end

end

