%% Run Simulation
clear all
addpath simulation_routines/
addpath scenario_routines/
addpath linear_solver_routines/
addpath cell_model_routines/
addpath utilities/

%% First Generate Input Parameters for Multi-Domain Model

%% Time Settings
% Time-Step to solve Diffusion Eq. in millisecond
input.dt = 0.1; % [ms]
%  Simulation Time in milliseconds
input.time_end = 10; % [ms]
%% Define the finite differences grid
% Length/Width/Height of the muscle cube in centimeters
input.length = 4.0; % [cm]
input.width = 2; % [cm]
input.height = 2; % [cm]
% Number of grid points in x/y/z-direction
input.nx = 20*input.length + 1;  
input.ny = 20*input.width + 1; 
input.nz = 20*input.height +1;
%% Define material parameters
% Number of motor units
input.num_of_mus = 3; 
% Volume fractions of the MUs (for each MU)
for mu_idx=1:input.num_of_mus
    input.volume_fractions{mu_idx}=1/input.num_of_mus;
end
% Surface to volume ratio [1/cm] (for each MU)
%input.A_m = [500, 250]; 
input.axondiameter=10e-4; %Axon diamter [cm]; fiber diameter is 0.66*axondiameter (Kim vs. Sweeney 0.6)
input.internodallength=0.2 %length between two nodes (myelinised part) [cm]
input.nodalgaplength=1.5e-4; %[cm] (see Sweeney)
input.lnlm=input.nodalgaplength/input.internodallength; % ratio of intranodal length to nodal gap length [-]
input.A_m = linspace(500,250,input.num_of_mus)*input.lnlm; %multiply A_m with ratio lnlm as the surface area is only th non-myelinated part
% membrane capacity [microF/cm^2] (for each MU)
input.C_m = ones(1,input.num_of_mus).*2.5; 
% Fibre Rotation angle arround the z-Axes
input.fibre_rotation_z = 0; 
% Conductivity Tensor in the fibre [mS/cm]
input.sigma_i = (1/0.1)* [1 0 0; 0 0 0; 0 0 0];% 8.93 * [1 0 0; 0 0 0; 0 0 0];
% Conductivity Tensor in the extra-cellular space [mS/cm]
input.sigma_e = (1/0.3)* [1 0 0; 0 0.5 0; 0 0 0.5];%6.7 * [1 0 0; 0 0.5 0; 0 0 0.5]; 
%% Define if an additional fat/skin layer on top of the muscle tissue should be included
input.fat = true;
% Height of the fat/skin tissue in centimeters
input.height_fat = 0.2; % [cm]
% Number of grid points in z-direction (Fat/Skin Domain)
input.nz_fat = 6; 
% Conductivity Tensor in the fat tissue [mS/cm]
input.sigma_o = 0.4*eye(3); 
input.skin = false;
% Height of the fat/skin tissue in centimeters
input.height_skin = 0.3; % [cm]
% Number of grid points in z-direction (Fat/Skin Domain)
input.nz_skin = 5; 
% Conductivity Tensor in the fat tissue [mS/cm]
input.sigma_s = 0.2*eye(3); 
%%% Define if an additional nerve lays in the fat layer and its position
%conductivity in the fiber and ext-space of the nerve fiber depends on the
%fibers diameter. Other author set resistance(=1/conductivity)=0.7. see
%conductivity in the membrane (axoplasma resistance) neglected in bidomain
%model
%other values Kim_09
%Conductivity Tensor in the fibre [mS/cm] (see Rattay and McNeal: 100Ohm*cm)
%input.sigma_i = (1/0.1)* [1 0 0; 0 0 0; 0 0 0];
% Conductivity Tensor in the extra-cellular space [mS/cm] (see Rattay and
% McNeal: 300 Ohm*cm) vs Kim takes 24 Ohm*cm 
%input.sigma_e = (1/0.3) * eye(3)
%% Chose Membrane Model
input.model_id = 5; % 0: Hodgkin-Huxley (1952) 1: Shorten (2007)
%% Define position of the neuromuscular junction, firing times and stimulus
% Amplitude of the applied electrical stimulus
% input.stimulus_amplitude = 2000; % Shorten
input.stimulus_amplitude = 700;  % Hodkin Huxley
% Length of the applied stimulus in milliseconds
input.stimulus_length = 0.1; % [ms] 
% position of the neuromuscular junctions
input.junction_points = cell(input.num_of_mus,1);
N = input.nx*input.ny*input.nz;
grid_points = reshape(1:N,input.nx,input.ny,input.nz);
% input.junction_points{1} = [];
% input.junction_points{2} = [];

input.active = ones(N*input.num_of_mus,1);
%% firing times
input.firing_times = cell(input.num_of_mus,1);

%input.firing_times{1} = [10];%[20 80 140 200 260 320 380 440]; % [ms]

%% Non-Isometric Contraction 
input.dynamic{1} = 0; % 1: True 0: False 
input.dynamic{2} = 0; % 0: uniaixal extension 1: pure shear 
input.dynamic{3} = 0; % Shape 0: sin 
input.dynamic{4} = 500; % period in ms
input.dynamic{5} = [1.2 1]; % Length 1 and Length 2 
%% Output Generation
input.output_frequency = 1; 
input.DefGrad = eye(3);%diag([1.05, 1.05^(-0.5), 1.05^(-0.5)]);%eye(3);

%% Apply external stimulus
input.ext_stim.electrode_size=0.1*0.1%size of the square electrode [cm^2]
input.ext_stim.pulse_width=0.2; %[ms]
input.ext_stim.current=10;%[mA]
input.ext_stim.amplitude = (input.ext_stim.current*10^3)/input.ext_stim.electrode_size; %[microampere/cm^2]=current density
input.ext_stim.stim_length = 10; %[ms]
input.ext_stim.type=1; %  0=monophasic; biphasix=1; constant=2
input.ext_stim.frequency=50; %[Hz=1/s]
% input.ext_stim.points = [356 436 516 596 676 756 836 916 996 357 437 517 597 677 757 837 917 997 358 438 518 598 678 758 838 918 998 359 439 519 599 679 759 839 919 999 360 440 520 600 680 760 840 920 1000 361 441 521 601 681 761 841 921 1001 362 442 522 602 682 762 842 922 1002 363 443 523 603 683 763 843 923 1003 364 444 524 604 684 764 844 924 1004];
gg = reshape(1:input.nx*input.ny,input.nx,input.ny);
gg= [gg(20:22,5:15);gg(60:62,5:15)];
input.ext_stim.points = reshape(gg,[],1);
%9*9=81 stimulation points. is a square of 9p x 9p
% -->equals 0.45cm x 0.45cm
%% Now Run the simulation 
mkdir results
input.ode_solver=1;
MultiDomain_implicit_3D_parallel(input)



