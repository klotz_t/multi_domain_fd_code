%-------------------------------------------------%
%                                                 %
% ----------- The MultiDomain equations --------- %
%                                                 %
% ---- Institute for Modelling and Simulation --- %
% ---------- of Biomechanical Systems ----------- %
%                                                 % 
% ----------- University of Stuttgart ----------- % 
%                                                 %  
%                   Thomas Klotz                  %
%-------------------------------------------------%
%%
function [] = MultiDomain_implicit_3D_parallel(input)
%
fprintf('\n Start Programm \n')
%% Time Settings
fprintf('\n Seeting up time settings \n')
% time step size for PDE
if (isfield(input,'dt'))
    dt = input.dt; % [ms]
else
    dt = 0.1; % [ms]
end
% time step for the ODEs
if (isfield(input,'dt_ode'))
    dt_ode = input.dt_ode;  % [ms]
else 
    dt_ode = 0.01; % [ms]
end
% stop time
t_end = input.time_end; % [ms]
% number of time steps for dynamic PDE solver (k)
num_of_dt = round(t_end / dt);
% Output Frequency 
output_frequency = input.output_frequency;
%% Setting up the computational domain
fprintf('\n Setting up the finite-difference grid \n')
% length of the domain [cm]
grid.length = input.length;
grid.width = input.width;
grid.height = input.height;
% number of grid points 
grid.X = input.nx; % Number of Grid Points in x-direction 
grid.Y = input.ny; % Number of Grid Points in y-direction 
grid.Z = input.nz; % Number of Grid Points in z-direction 
n = grid.X*grid.Y*grid.Z; % Total number of discretization points in muscle region.
% grid spacing
grid.dx = grid.length / (grid.X-1);
grid.dy = grid.width / (grid.Y-1);
grid.dz = grid.height / (grid.Z-1);
% get the surface nodes
surface_nodes = get_surface_nodes(grid.X,grid.Y,grid.Z);
% specify if fat tissue should be included
include_fat = input.fat;
if (include_fat)
    grid.Z_fat = input.nz_fat;
    grid.height_fat = input.height_fat;
    grid.dz_fat = grid.height_fat / (grid.Z_fat-1);
    n_fat = grid.X*grid.Y*grid.Z_fat;
end
% specify if skin tissue should be included (on top of fat)
include_skin = input.skin;
if (include_skin)
    grid.Z_skin = input.nz_skin;
    grid.height_skin = input.height_skin;
    grid.dz_skin = grid.height_skin / (grid.Z_skin-1);
    n_skin=grid.X*grid.Y*grid.Z_skin;
end
%% Node numbering
% Bottom:                      Middle:
% (X-1)*Y+1      ...  X*Y        X*(2Y-1)+1    ...        ...  2*X*Y
%  |             ...   |            |           |         ...     |
% (X-1)*i+1      ...  i*X           |           |         ...     |  
%  |     |   |   ...   |            |           |         ...     |          
% X+1  X+2  X+3  ...  2*X        X*(Y+1)+1   X*(Y+1)+2    ...  X*(Y+2)
%  1    2    3   ...   X         X*Y+1       X*Y+2        ...  X*Y+X

%% Set Model Parameters
fprintf('\n Define material parameters \n')
% Number of MUs
p.Number_of_MUs = input.num_of_mus;
% Volume fraction of the MUs
p.f_k = input.volume_fractions;
% surface area to volume ration
p.A_m = input.A_m;    % [1/cm] % -> fibre-radius = [50 150] micro-meters
% membrane capacitance
p.C_m = input.C_m;      % [microF/cm^2]
% Fibre rotation arround the z-Axe
if (isfield(input,'fibre_rotation_z'))
    fibre_angle_z = input.fibre_rotation_z;%pi/8;
else
    fibre_angle_z = 0;
end
if (isfield(input,'fibre_rotation_y'))
    fibre_angle_z = input.fibre_rotation_y;%pi/8;
else
    fibre_angle_y = 0;
end
% Rotation matrix for conductivity tensor
a_0 = [cos(fibre_angle_y)*cos(fibre_angle_z);...
    cos(fibre_angle_y)*sin(fibre_angle_z); ...
    sin(fibre_angle_y)];
% conductivity
p.sigma_i_0 = input.sigma_i(1,1) * (a_0*a_0') + input.sigma_i(2,2) * (eye(3) - a_0*a_0'); % mS/cm - D
p.sigma_e_0 = input.sigma_e(1,1) * (a_0*a_0') + input.sigma_e(2,2) * (eye(3) - a_0*a_0'); % mS/cm - D
F_inv = inv(input.DefGrad);
p.sigma_i = F_inv*p.sigma_i_0*F_inv';
p.sigma_e = F_inv*p.sigma_e_0*F_inv';

if (include_fat)
    p.sigma_o_0 = input.sigma_o;
    p.sigma_o = F_inv*p.sigma_o_0*F_inv';
end
if (include_skin)
    p.sigma_s_0 = input.sigma_s;
    p.sigma_s = F_inv*p.sigma_s_0*F_inv';
end

%% Muscle Stimulation
fprintf('\n Define junction points and stimulation times \n')
amplitude = input.stimulus_amplitude;%1000;
stim_length = input.stimulus_length;

%I_stim = zeros(n*,num_of_dt);
%I_stim = zeros(n,num_of_dt,p.Number_of_MUs);

% Prescribe the firing times from the motor nerves
for mu_ind=1:p.Number_of_MUs
    I_stim{mu_ind} = sparse(n,num_of_dt);
    for jun_ind=1:length(input.junction_points{mu_ind})
        point = input.junction_points{mu_ind}(jun_ind)-(mu_ind-1)*n;
        if(length(input.firing_times{mu_ind}) > 0)
            I_stim{mu_ind}(point,:) = stimulus(input.firing_times{mu_ind},amplitude,stim_length,dt,num_of_dt); 
        end
    end
end
% I_stim = reshape(I_stim,[length(I_stim)/num_of_processes num_of_dt num_of_processes]);

% Apply an external electrical stimulus
if (isfield(input,'ext_stim'))
    ext_stim = calculate_ext_stimulus(input.ext_stim.amplitude,input.dt,num_of_dt,...
        input.ext_stim.type,input.ext_stim.frequency,input.ext_stim.pulse_width);
end

%% Initalise Vector of unknowns
fprintf('\n Setting up vector of unknowns \n')
if (include_skin)
    % The Vecors uf unknowns [Vm_1,Vm_2,...,Vm_n,Phi_e,Phi_o,Phi_s]
    xx = zeros((p.Number_of_MUs+1)*n + grid.X*grid.Y*grid.Z_fat + grid.X*grid.Y*grid.Z_skin,1);
elseif (include_fat)
    % The Vecors uf unknowns [Vm_1,Vm_2,...,Vm_n,Phi_e,Phi_o]
    xx = zeros((p.Number_of_MUs+1)*n + grid.X*grid.Y*grid.Z_fat,1);
else
    % The Vecors uf unknowns [Vm_1,Vm_2,...,Vm_n,Phi_e]
    xx = zeros((p.Number_of_MUs+1)*n,1);
end
% % Initalise the State Variables 
% xx(1:p.Number_of_MUs*n) = -75;
% Global Number of Degress of Freedom
global_dofs = length(xx); 
%% Setting up the cellular model 
fprintf('\n Setting up the membrane models \n')
%
if (isfield(input,'model_id'))
    model_id = input.model_id;
else
    model_id = 0;
end

% Initialise constants and state variables for cellular model
for mu_ind=1:p.Number_of_MUs
    if (isfield(input.cell_model_specs,'parameters'))
        CONSTANTS{mu_ind} = input.cell_model_specs.parameters(mu_ind,:);
    else
        gg = initConsts(model_id); 
        CONSTANTS{mu_ind} = gg(1,:);
    end    
    %if model_id == 1
    %    gg = initConsts(model_id); 
    %    CONSTANTS{mu_ind} = gg(1,:); 
    %    if (isfield(input.cell_model_specs,'fatigue'))
    %        CONSTANTS{mu_ind}(:,57) = input.cell_model_specs.fatigue;
    %    end
    %else
    %    CONSTANTS{mu_ind} = initConsts(model_id);
    %end 
end

% Map the MU capacity C_m to the vector of unknowns 
for mu_ind=1:p.Number_of_MUs
    Cm_p{mu_ind} = p.C_m(mu_ind)*ones(n,1);
end

% Initalise cell models at each point and for each MU
[INIT_STATES] = initStates(model_id);
for mu_ind=1:p.Number_of_MUs
    for point_ind=1:n
        ALL_STATES{mu_ind}(point_ind,:) = INIT_STATES;
    end
end
% Initalise the State Variables 
xx(1:p.Number_of_MUs*n) = INIT_STATES(:,1);

% Set up ODE solver
if (isfield(input,'ode_solver'))
    ode_solver = input.ode_solver;
else
    ode_solver = 0;
end

if ode_solver == 1
    options = odeset('RelTol', 1e-06, 'AbsTol', 1e-06, 'MaxStep', 0.1);
end
%% Setting up the discrete Finite-Difference Matrix System
fprintf('\n Setting up the finite difference matrix system \n')
% Setting Up the FD-Matrix system to solve A\b=x

% Initalise RHS Vector b;
b = zeros(global_dofs,1);
% Setting up a Vector to eliminate entrys on the right hand side Vector b -> For Neumann BC
b_elem = RHS_eliminate(global_dofs,grid,surface_nodes,p);
% Linear solver option 
if (isfield(input,'linear_solver'))
    solvertype = input.linear_solver;
else
    solvertype = 0;
end
% Chose the BC at the bottom of the tissue sample
if (isfield(input,'BC'))
    BC = input.BC;
else
    BC = 0;
end

if (solvertype == 0) % Matlab mldivide solver
%     M = build_multidomain_fd_matrix(dt,grid,p,global_dofs,include_fat,include_skin);
    [M,~,~,~,~,~,~] = build_finite_differences_block_matrices(dt,grid,p,include_fat,include_skin,BC);
elseif (solvertype == 1) % GMRES solver with ilu as preconditioner
%     M = build_multidomain_fd_matrix(dt,grid,p,global_dofs,include_fat,include_skin);
    [M,~,~,~,~,~,~] = build_finite_differences_block_matrices(dt,grid,p,include_fat,include_skin,BC);
    % incomplete LU-Factorisation of the System Matrix A (preconditioner for gmres solver) 
    [L,U] = ilu(M,struct('type','crout','droptol',1e-6));
end

%% Setting up Output 
t = 0;
fprintf('\n Generate general output \n')
output_count = 0;
delta_t = dt*output_frequency;
% save('results/simple_example_info.mat','delta_t','p','grid','surface_nodes','input','I_stim');
if (isfield(input,'output_spec'))
    output_spec = input.output_spec;
else
    output_spec = 0;
end

if (isfield(input,'filepath'))
    file_name = sprintf('%s/simple_example_%d.mat', input.filepath,output_count);
else
    file_name = sprintf('results/simple_example_%d.mat', output_count);
end

switch output_spec
    case 0 % Full state vector
        save(file_name,'xx','ALL_STATES','t');%,'rhs')
    case 1 % Only the electrical potential fields
        save(file_name,'xx');
    case 2 % Only surface emg potential
        surface_pot = xx((global_dofs - grid.X*grid.Y + 1):1:length(xx));
        emg = reshape(surface_pot,[grid.X, grid.Y]);
        save(file_name,'emg')
    case 3 % Also store the RHS vector
        save(file_name,'xx','ALL_STATES')
 end

%% Save the simulation parameters
if (isfield(input,'filepath'))
        file_name = sprintf('%s/simple_example_info.mat', input.filepath);
    else
        file_name = 'results/simple_example_info.mat';
    end
if (isfield(input,'ext_stim'))
    save(file_name,'delta_t','p','grid','surface_nodes','input','I_stim','ext_stim');
else
    save(file_name,'delta_t','p','grid','surface_nodes','input','I_stim');
end
%%
tic
%
% -------------------------- solution process ----------------------------%
%
fprintf('\n Start solving the multi-domain equations \n')
for time = 1:num_of_dt-1
    %
    t = time*dt;
    fprintf('step #   : %d \t', time);
    fprintf('time [ms]: %f \n', (time-1)*dt);
    %%
    % cellular model
    %
    tspan = [(time-1)*dt, time*dt];
    %
    % integrate the cellular model at each discretisation point and for every MU 
    %   
    tic
    switch ode_solver
        case 0
            parfor mu_ind=1:p.Number_of_MUs
                t_ode = tspan(1);
                while (t_ode < tspan(2))
                    k1 = computeRatesAndAlgebraic(t_ode, ALL_STATES{mu_ind},...
                        CONSTANTS{mu_ind},I_stim{mu_ind}(:,time),Cm_p{mu_ind},model_id)';
                    k2 = computeRatesAndAlgebraic(t_ode+dt_ode, ALL_STATES{mu_ind} + dt_ode.*k1,...
                        CONSTANTS{mu_ind},I_stim{mu_ind}(:,time),Cm_p{mu_ind},model_id)'; 
                    ALL_STATES{mu_ind} = ALL_STATES{mu_ind} + dt_ode/2.*(k1 + k2);
                    t_ode = t_ode + dt_ode;
                end
            end
        case 1
            for mu_ind=1:p.Number_of_MUs
                tmp = zeros(n,length(ALL_STATES{mu_ind}(1,:)));
                parfor i = 1:n
                    LAST_STATES = ALL_STATES{mu_ind}(i,:);
                    % Solve model with ODE solver
                    [~, STATES] = ode15s(@(TIME, STATES)computeRatesAndAlgebraic(...
                        TIME, STATES, CONSTANTS{mu_ind},I_stim{mu_ind}(i,time),Cm_p{mu_ind}(i),model_id), tspan, LAST_STATES, options);
                    % update ALL_STATES
%                     ALL_STATES{mu_ind}(i,:) = STATES(end,:);
                    tmp(i,:) = STATES(end,:);
                end
                ALL_STATES{mu_ind} = tmp;
            end
    end    
    toc
    %b = zeros(global_dofs,1);
    %b(1:n*p.Number_of_MUs) = reshape(ALL_STATES(:,1,:),[],1);
    b = [];
    for mu_ind=1:p.Number_of_MUs
        b =vertcat(b,ALL_STATES{mu_ind}(:,1));
    end
    if (include_fat)
        b = vertcat(b,zeros(n+n_fat,1)); 
        
    elseif (include_skin)
        b = vertcat(b,zeros(n+n_fat+n_skin,1));    
    else
        b = vertcat(b,zeros(n,1));
    end
    % Apply BCs to the right hand side vector
    rhs = b.*b_elem;
    % Modify the RHS vector in the case of an external electrical current 
    if (isfield(input,'ext_stim'))
       for i=1:length(input.ext_stim.points)
           if (include_skin)
               % Map the surface node number to the global DOF
               ind = input.ext_stim.points(i) + n*(p.Number_of_MUs+1)+grid.X*grid.Y*grid.Z_fat+grid.X*grid.Y*(grid.Z_skin-1);
           elseif (include_fat)
               % Map the surface node number to the global DOF
               ind = input.ext_stim.points(i) + n*(p.Number_of_MUs+1)+grid.X*grid.Y*(grid.Z_fat-1);
           else
               % Map the surface node number to the global DOF
               ind = input.ext_stim.points(i) + n*p.Number_of_MUs+grid.X*grid.Y*(grid.Z-1);
           end
           % Apply BC
           rhs(ind) = ext_stim(time);%*input.ext_stim.weights(i); 
       end
    end
    
    if (isfield(input,'dynamic'))
       if (input.dynamic{1} == 1)
          % Get the actual Deformation Gradient from prescribed scenario 
          F_inv = update_def_grad(input.dynamic,(time-1)*dt);
          % Update the conductivity Tensors
          p.sigma_i = F_inv*p.sigma_i_0*F_inv';
          p.sigma_e = F_inv*p.sigma_e_0*F_inv';
          if (include_fat)
              p.sigma_o = F_inv*p.sigma_o_0*F_inv';
          end
          if (include_skin)
              p.sigma_s = F_inv*p.sigma_s_0*F_inv';
          end
          % Update the FD matrix
%           M = build_multidomain_fd_matrix(dt,grid,p,global_dofs,include_fat,include_skin);
          [M,~,~,~,~,~,~] = build_finite_differences_block_matrices(dt,grid,p,include_fat,include_skin,BC);
          if (solvertype == 1)
              % incomplete LU-Factorisation of the System Matrix A (preconditioner for gmres solver) 
              [L,U] = ilu(M,struct('type','crout','droptol',1e-6));
          end
       end
    end

    %% Solve the Problem
    tic   
    switch solvertype
        case 0 % Matlab mldivide solver
            xx = M\rhs; 
        case 1 % GMRES Solver          
            xx = gmres(M,rhs,20,1e-12,20,L,U,xx);
        otherwise
            disp('Error: No valid linear solver specification')
            return
    end
    toc
    % Uptate Vm for the Cell-Modells
    gg = reshape(xx(1:n*p.Number_of_MUs),[n p.Number_of_MUs]);
    for mu_ind=1:p.Number_of_MUs
        ALL_STATES{mu_ind}(:,1) = gg(:,mu_ind);       
    end
    %ALL_STATES(:,1,:) = reshape(xx(1:n*p.Number_of_MUs),[n*p.Number_of_MUs/num_of_processes num_of_processes]);

    % Save the Solution
    if (mod(time,output_frequency)==0)
        output_count = output_count + 1;
        if (isfield(input,'filepath'))
            file_name = sprintf('%s/simple_example_%d.mat', input.filepath,output_count);
        else
            file_name = sprintf('results/simple_example_%d.mat', output_count);
        end
        switch output_spec
            case 0 % Full state vector
                save(file_name,'xx','ALL_STATES','t');%,'rhs')
            case 1 % Only the electrical potential fields
                save(file_name,'xx');
            case 2 % Only surface emg potential
                surface_pot = xx((global_dofs - grid.X*grid.Y + 1):1:length(xx));
                emg = reshape(surface_pot,[grid.X, grid.Y]);
                save(file_name,'emg')
            case 3 % Also store the RHS vector
                save(file_name,'xx','ALL_STATES','rhs')
         end
    end
    %
end    % time
toc
fprintf('\n The simulation has been succesfully completed :) \n')
%
end     % function
