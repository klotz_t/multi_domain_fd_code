%% Run Simulation
% clear all
addpath ../../simulation_routines/
addpath ../../scenario_routines/
addpath ../../linear_solver_routines/
addpath ../../cell_model_routines/
addpath ../../utilities/
addpath ../../multi_physics_routines/
%% First Generate input{MU_idx} Parameters for Multi-Domain Model
%muscle_idx = 1;
for muscle_idx=3:3
    rng((muscle_idx-1)*1000);	
    load(sprintf('input/muscle_%d.mat',muscle_idx))
    d_fat = [0 0.5]; %cm
    for fat_idx = 1:length(d_fat)
        input = cell(150,1);
        parfor MU_idx=1:150
            %% Time Settings
            % Time-Step to solve Diffusion Eq. in millisecond
            input{MU_idx}.dt = 0.1; % [ms]
            input{MU_idx}.dt_ode = input{MU_idx}.dt/10;
            %  Simulation Time in milliseconds
            input{MU_idx}.time_end = 60; % [ms]
            %% Define the finite differences grid
            % Length/Width/Height of the muscle cube in centimeters
            input{MU_idx}.length = 8; % [cm]
            input{MU_idx}.width = 4; % [cm]
            input{MU_idx}.height = 4; % [cm]
            % Number of grid points in x/y/z-direction
            input{MU_idx}.nx = input{MU_idx}.length*20+1;  
            input{MU_idx}.ny = input{MU_idx}.width*10+1; 
            input{MU_idx}.nz = input{MU_idx}.height*10+1;
            %% Define material parameters
            % Number of motor units
            input{MU_idx}.num_of_mus = 1; 
            % Volume fractions of the MUs (for each MU)
            %f_r = linspace(1,10,input{MU_idx}.num_of_mus)/sum(linspace(1,10,input{MU_idx}.num_of_mus));
            % load('input{MU_idx}/distribution_30_mus.mat')
            for i=1:input{MU_idx}.num_of_mus
                gg = zeros(input{MU_idx}.nx,input{MU_idx}.ny,input{MU_idx}.nz);
                for j=1:input{MU_idx}.nx
                    gg(j,:,:) = squeeze(f_r(:,:,MU_idx));
                end
                input{MU_idx}.volume_fractions{i} = gg;
            end
            % Surface to volume ratio [1/cm] (for each MU)
            %input{MU_idx}.A_m = 250*ones(1,input{MU_idx}.num_of_mus);
            values = linspace(500,250,150);
            input{MU_idx}.A_m = values(MU_idx);
            % membrane capacity [microF/cm^2] (for each MU)
            % input{MU_idx}.C_m = [1, 1]; 
            input{MU_idx}.C_m = 1.*ones(1,input{MU_idx}.num_of_mus); 
            % Fibre Rotation angle arround the z-Axes
            input{MU_idx}.fibre_rotation_z = 0; 
            % Conductivity Tensor in the fibre [mS/cm]
            input{MU_idx}.sigma_i = 8.93 * [1 0 0; 0 0 0; 0 0 0]; 
            % Conductivity Tensor in the extra-cellular space [mS/cm]
            input{MU_idx}.sigma_e = 6.7 * [1 0 0; 0 0.2 0; 0 0 0.2]; 
            %% Define if an additional fat/skin layer on top of the muscle tissue should be included
            if d_fat(fat_idx) > 0 
	    	input{MU_idx}.fat = true;
           	 % Height of the fat/skin tissue in centimeters
           	 input{MU_idx}.height_fat = d_fat(fat_idx);%0.3; % [cm]
           	 % Number of grid points in z-direction (Fat/Skin Domain)
            	 input{MU_idx}.nz_fat = d_fat(fat_idx)*10+1;%4;
	    else 
	         input{MU_idx}.fat = false;
            end		 
            % Conductivity Tensor in the fat tissue [mS/cm]
            input{MU_idx}.sigma_o = 0.4*eye(3); 
            input{MU_idx}.skin = false;
            % Height of the fat/skin tissue in centimeters
            input{MU_idx}.height_skin = 0.125; % [cm]
            % Number of grid points in z-direction (Fat/Skin Domain)
            input{MU_idx}.nz_skin = 5; 
            % Conductivity Tensor in the fat tissue [mS/cm]
            input{MU_idx}.sigma_s = 0.2*eye(3); 
            %% Chose Membrane Model
            input{MU_idx}.model_id = 0; % 0: Hodgkin-Huxley (1952) 1: Shorten (2007)
            input{MU_idx}.cell_model_specs.fatigue=0; % 0: knock out membrane fatigue 1: with membrane fatigue
            %% Define position of the neuromuscular junction, firing times and stimulus
            % Amplitude of the applied electrical stimulus
            %input{MU_idx}.stimulus_amplitude = 3000; % Shorten
            input{MU_idx}.stimulus_amplitude = 700;  % Hodkin Huxley
            % Length of the applied stimulus in milliseconds
            input{MU_idx}.stimulus_length = 0.1; % [ms] 
            % position of the neuromuscular junctions
            input{MU_idx}.junction_points = cell(input{MU_idx}.num_of_mus,1);
            N = input{MU_idx}.nx*input{MU_idx}.ny*input{MU_idx}.nz;
            grid_points = reshape(1:N,input{MU_idx}.nx,input{MU_idx}.ny,input{MU_idx}.nz);
            stim_plane_points = reshape(grid_points(30+randi([-5 5]),:,:),[],1);
            % rng(0,'twister');
            % temp = randi([-10 10],1,input{MU_idx}.num_of_mus)
            for i=1:input{MU_idx}.num_of_mus
                %stim_plane_points = reshape(grid_points(mu_dist.x_nm_junctions(MU_idx),:,:),[],1);
                input{MU_idx}.junction_points{i} = stim_plane_points + (i-1)*N;
            end

            input{MU_idx}.active = ones(N*input{MU_idx}.num_of_mus,1);
            %% firing times
            input{MU_idx}.firing_times = cell(input{MU_idx}.num_of_mus,1);
            input{MU_idx}.firing_times{1} = [0.1];
            %% Non-Isometric Contraction 
            input{MU_idx}.dynamic{1} = 0; % 1: True 0: False 
            input{MU_idx}.dynamic{2} = 0; % 0: uniaixal extension 1: pure shear 
            input{MU_idx}.dynamic{3} = 0; % Shape 0: sin 
            input{MU_idx}.dynamic{4} = 500; % period in ms
            input{MU_idx}.dynamic{5} = [1.2 1]; % Length 1 and Length 2 
            %% Output Generation
            input{MU_idx}.output_frequency = 5; 
            input{MU_idx}.DefGrad = eye(3);%diag([1.2, 1.2^(-0.5), 1.2^(-0.5)]);
            input{MU_idx}.output_spec = 1; % Save all potentials 
            %% Solver Specs
            input{MU_idx}.linear_solver = 1;
       	    input{MU_idx}.ode_solver = 0;
            input{MU_idx}.filepath   = sprintf('muscle_%d_f%d/MUAP_%d/results',muscle_idx,d_fat(fat_idx)*10,MU_idx);
            %% Now Run the simulation 
            mkdir(input{MU_idx}.filepath)
            if ~isfile(sprintf('muscle_%d_f%d/MUAP_%d/results/simple_example_599.mat',muscle_idx,d_fat(fat_idx),MU_idx))
	    	fprintf('MU #   : %d \t', MU_idx);
		MultiDomain_implicit_3D_parallel(input{MU_idx})
            	get_EMG(input{MU_idx}.filepath)
            end
	    if ~isfile(sprintf('muscle_%d_f%d/MUAP_%d/results/magnetic_fields/B_fields.mat',muscle_idx,d_fat(fat_idx),MU_idx))	             	calculate_magentic_field(input{MU_idx}.filepath);
            end 		    
            %mkdir(sprintf('muscle_%d/MUAP_%d',muscle_idx,MU_idx))
            %movefile('results',sprintf('muscle_%d/MUAP_%d',muscle_idx,MU_idx))
        end
    end
end


