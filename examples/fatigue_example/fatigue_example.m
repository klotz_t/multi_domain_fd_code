%% Run Simulation
clear all
%% First Generate Input Parameters for Multi-Domain Model

%% Time Settings
% Time-Step to solve Diffusion Eq. in millisecond
input.dt = 0.1; % [ms]
input.dt_ode = input.dt/100;
%  Simulation Time in milliseconds
input.time_end = 10000; % [ms]
%% Define the finite differences grid
% Length/Width/Height of the muscle cube in centimeters
input.length = 4.0; % [cm]
input.width = 2.0; % [cm]
input.height = 2.0; % [cm]
% Number of grid points in x/y/z-direction
input.nx = 30*input.length + 1;  
input.ny = 10*input.width + 1; 
input.nz = 10*input.height + 1;
%% Define material parameters
% Number of motor units
input.num_of_mus = 30; 
% Volume fractions of the MUs (for each MU)
%f_r = linspace(1,10,input.num_of_mus)/sum(linspace(1,10,input.num_of_mus));
load('input/distribution_30_mus.mat')
for i=1:input.num_of_mus
    gg = zeros(input.nx,input.ny,input.nz);
    for j=1:input.nx
      gg(j,:,:) = f_r(:,:,i);
    end
    input.volume_fractions{i} = gg;
end
    % Surface to volume ratio [1/cm] (for each MU)
%input.A_m = 250*ones(1,input.num_of_mus);
input.A_m = linspace(500,250,input.num_of_mus);
% membrane capacity [microF/cm^2] (for each MU)
% input.C_m = [1, 1]; 
input.C_m = ones(1,input.num_of_mus); 
% Fibre Rotation angle arround the z-Axes
input.fibre_rotation_z = 0; 
% Conductivity Tensor in the fibre [mS/cm]
input.sigma_i = 8.93 * [1 0 0; 0 0 0; 0 0 0]; 
% Conductivity Tensor in the extra-cellular space [mS/cm]
input.sigma_e = 6.7 * [1 0 0; 0 0.5 0; 0 0 0.5]; 
%% Define if an additional fat/skin layer on top of the muscle tissue should be included
input.fat = true;
% Height of the fat/skin tissue in centimeters
input.height_fat = 0.5; % [cm]
% Number of grid points in z-direction (Fat/Skin Domain)
input.nz_fat = 6; 
% Conductivity Tensor in the fat tissue [mS/cm]
input.sigma_o = 0.4*eye(3); 
input.skin = false;
% Height of the fat/skin tissue in centimeters
input.height_skin = 0.125; % [cm]
% Number of grid points in z-direction (Fat/Skin Domain)
input.nz_skin = 5; 
% Conductivity Tensor in the fat tissue [mS/cm]
input.sigma_s = 0.2*eye(3); 
%% Chose Membrane Model
input.model_id = 0; % 0: Hodgkin-Huxley (1952) 1: Shorten (2007)
input.cell_model_specs.fatigue=0; % 0: knock out membrane fatigue 1: with membrane fatigue
%% Define position of the neuromuscular junction, firing times and stimulus
% Amplitude of the applied electrical stimulus
input.stimulus_amplitude = 3000; % Shorten
%input.stimulus_amplitude = 700;  % Hodkin Huxley
% Length of the applied stimulus in milliseconds
input.stimulus_length = 0.1; % [ms] 
% position of the neuromuscular junctions
input.junction_points = cell(input.num_of_mus,1);
N = input.nx*input.ny*input.nz;
grid_points = reshape(1:N,input.nx,input.ny,input.nz);
%stim_plane_points = reshape(grid_points(90,:,:),[],1);
rng(0,'twister');
temp = randi([-10 10],1,input.num_of_mus)
for i=1:input.num_of_mus
    stim_plane_points = reshape(grid_points(90+temp(i),:,:),[],1);
    input.junction_points{i} = stim_plane_points + (i-1)*N;
end

input.active = ones(N*input.num_of_mus,1);
%% firing times
input.firing_times = cell(input.num_of_mus,1);
load('input/firing_times.mat')
for i=1:input.num_of_mus
  input.firing_times{i} = round(firing_times{i},3);
end
%input.firing_times{1} = [0.1 21 41 61 81];%1:10:30;%[1 21 39 40 61 80 104 122 145 166 184];
%input.firing_times{2} = [5 20 35 50];%[15 45 72 101 134 162 190];
%input.firing_times{3} = [10];
%input.firing_times{4} = [15];
%input.firing_times{5} = [20];
%input.firing_times{6} = [25];
%input.firing_times{7} = [30];
%input.firing_times{8} = [35];
%input.firing_times{9} = [40];
%input.firing_times{10} = [45];

%% Non-Isometric Contraction 
input.dynamic{1} = 0; % 1: True 0: False 
input.dynamic{2} = 0; % 0: uniaixal extension 1: pure shear 
input.dynamic{3} = 0; % Shape 0: sin 
input.dynamic{4} = 500; % period in ms
input.dynamic{5} = [1.2 1]; % Length 1 and Length 2 
%% Output Generation
input.output_frequency = 1; 
input.DefGrad = eye(3);%diag([1.2, 1.2^(-0.5), 1.2^(-0.5)]);
input.output_spec = 1; 

%% Apply external stimulus
% input.ext_stim.times = [1]; % [ms]
% input.ext_stim.amplitude = 40;
% input.ext_stim.stim_length = 5;
% input.ext_stim.points = [87811 87812 87813 87811+20 87812+20 87813+20 87811-20 87812-20 87813-20];
input.linear_solver = 4;
input.ode_solver = 0;
%% Now Run the simulation 
addpath ../../simulation_routines/
addpath ../../scenario_routines/
addpath ../../linear_solver_routines/
addpath ../../cell_model_routines/
mkdir results
MultiDomain_implicit_3D_parallel(input)


