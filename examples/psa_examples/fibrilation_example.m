%% Run Simulation
% clear all
addpath ../../simulation_routines/
addpath ../../scenario_routines/
addpath ../../linear_solver_routines/
addpath ../../cell_model_routines/
addpath ../../utilities/
addpath ../../multi_physics_routines/
%% First Generate input{MU_idx} Parameters for Multi-Domain Model
L = 4;
W = 2;
H = 2;
n_f  = 20;%30
n_xf = 10;


rng(0)
nMU      = 100;     
Am_values = 250 + (600-250)*rand(nMU,1);
gg = reshape(1:(n_f*L+1)*(n_xf*W+1)*(n_xf*H+1),(n_f*L+1),(n_xf*W+1),(n_xf*H+1));
jx_values = round(20 + (60-20) * rand(nMU,1),0);
jy_values = round(1 + (20-1) * rand(nMU,1),0);
jz_values = round(1 + (20-1) * rand(nMU,1),0);

rve_area = (1/n_xf)^2;
fibre_area = pi*(2./Am_values).^2;
fr_values = fibre_area./rve_area;

input = cell(nMU,1);
parfor MU_idx=1:nMU
    %% Time Settings
    % Time-Step to solve Diffusion Eq. in millisecond
    input{MU_idx}.dt = 0.1; % [ms] % 0.01
    input{MU_idx}.dt_ode = input{MU_idx}.dt/10;
    %  Simulation Time in milliseconds
    input{MU_idx}.time_end = 50; % [ms]
    %% Define the finite differences grid
    % Length/Width/Height of the muscle cube in centimeters
    input{MU_idx}.length = L; % [cm]
    input{MU_idx}.width = W; % [cm]
    input{MU_idx}.height = H; % [cm]
    % Number of grid points in x/y/z-direction
    input{MU_idx}.nx = n_f*input{MU_idx}.length + 1;  
    input{MU_idx}.ny = n_xf*input{MU_idx}.width + 1;
    input{MU_idx}.nz = n_xf*input{MU_idx}.height + 1;
    %% Define material parameters
    % Number of motor units
    input{MU_idx}.num_of_mus = 1; 
    % Volume fractions of the MUs (for each MU)
    %f_r = linspace(1,10,input{MU_idx}.num_of_mus)/sum(linspace(1,10,input{MU_idx}.num_of_mus));
    % load('input{MU_idx}/distribution_30_mus.mat')
    for i=1:input{MU_idx}.num_of_mus
        gg = fr_values(MU_idx)*ones(input{MU_idx}.nx,input{MU_idx}.ny,input{MU_idx}.nz);
        input{MU_idx}.volume_fractions{i} = gg;
    end
        % Surface to volume ratio [1/cm] (for each MU)
    input{MU_idx}.A_m = Am_values(MU_idx)*ones(1,input{MU_idx}.num_of_mus);
    %input{MU_idx}.A_m = linspace(para.Am(1),para.Am(2),input{MU_idx}.num_of_mus);
    % membrane capacity [microF/cm^2] (for each MU)
    % input{MU_idx}.C_m = [1, 1]; 
    input{MU_idx}.C_m = 1.*ones(1,input{MU_idx}.num_of_mus); 
    % Fibre Rotation angle arround the z-Axes
    input{MU_idx}.fibre_rotation_z = 0; 
    % Conductivity Tensor in the fibre [mS/cm]
    input{MU_idx}.sigma_i = 8.93 * [1 0 0; 0 0 0; 0 0 0]; 
    % Conductivity Tensor in the extra-cellular space [mS/cm]
    input{MU_idx}.sigma_e = 6.7 * [1 0 0; 0 0.25 0; 0 0 0.25]; 
    %% Define if an additional fat/skin layer on top of the muscle tissue should be included
    input{MU_idx}.fat = true;
    % Height of the fat/skin tissue in centimeters
    input{MU_idx}.height_fat = 0.5; % [cm]
    % Number of grid points in z-direction (Fat/Skin Domain)
    input{MU_idx}.nz_fat = 6; 
    % Conductivity Tensor in the fat tissue [mS/cm]
    input{MU_idx}.sigma_o = 0.4*eye(3);
    input{MU_idx}.skin = false;
    % Height of the fat/skin tissue in centimeters
    input{MU_idx}.height_skin = 0.125; % [cm]
    % Number of grid points in z-direction (Fat/Skin Domain)
    input{MU_idx}.nz_skin = 5; 
    % Conductivity Tensor in the fat tissue [mS/cm]
    input{MU_idx}.sigma_s = 0.2*eye(3); 
    %% Chose Membrane Model
    input{MU_idx}.model_id = 0;%7; % 0: Hodgkin-Huxley (1952) 1: Shorten (2007)
    input{MU_idx}.cell_model_specs.parameters = initConsts(0);
    %% Define position of the neuromuscular junction, firing times and stimulus
    % Amplitude of the applied electrical stimulus
    %input{MU_idx}.stimulus_amplitude = 2000; % Shorten
    input{MU_idx}.stimulus_amplitude = 700;  % Hodkin Huxley
    % Length of the applied stimulus in milliseconds
    input{MU_idx}.stimulus_length = 0.1; % [ms] 
    % position of the neuromuscular junctions
    input{MU_idx}.junction_points = cell(input{MU_idx}.num_of_mus,1);
    N = input{MU_idx}.nx*input{MU_idx}.ny*input{MU_idx}.nz;
    grid_points = reshape(1:N,input{MU_idx}.nx,input{MU_idx}.ny,input{MU_idx}.nz);
    %stim_plane_points = reshape(grid_points(90,:,:),[],1);
    % rng(0,'twister');
    % temp = randi([-10 10],1,input{MU_idx}.num_of_mus)
    for i=1:input{MU_idx}.num_of_mus
        stim_plane_points = reshape(grid_points(jx_values(MU_idx),jy_values(MU_idx),jz_values(MU_idx)),[],1);
        input{MU_idx}.junction_points{i} = stim_plane_points + (i-1)*N;
    end
    input{MU_idx}.active = ones(N*input{MU_idx}.num_of_mus,1);
    %% firing times
    input{MU_idx}.firing_times = cell(input{MU_idx}.num_of_mus,1);
    input{MU_idx}.firing_times{1} = [1];
    %% Output Generation
    input{MU_idx}.output_frequency = 1; 
    input{MU_idx}.DefGrad = eye(3);
    input{MU_idx}.output_spec = 1; 
    input{MU_idx}.linear_solver = 1;
    input{MU_idx}.ode_solver = 0;
    input{MU_idx}.filepath   = sprintf('fibirilation_response2/fibre_%d/results',MU_idx);
    %% Boundary Condition
    input{MU_idx}.BC = 0;
    %input{MU_idx}.BC = 1; % zero Neumann BC condition on bottom with respect to the extracellular potential (0: zero Dirichlet)
    %% Now Run the simulation 
    mkdir(input{MU_idx}.filepath)
    MultiDomain_implicit_3D_parallel(input{MU_idx})
    get_EMG(sprintf('fibirilation_response2/fibre_%d/results',MU_idx))
    get_im_EMG([2 1],sprintf('fibirilation_response2/fibre_%d/results',MU_idx))
    calculate_magentic_field(sprintf('fibirilation_response2/fibre_%d/results',MU_idx))
    %get_EMG(input{MU_idx}.filepath)
    %mkdir(sprintf('muscle_%d/MUAP_%d',muscle_idx,MU_idx))
    %movefile('results',sprintf('muscle_%d/MUAP_%d',muscle_idx,MU_idx))
end




