%% Define Baseline Parameters
pBaseline.sigma_i        = 8.93 * [1 0 0; 0 0 0; 0 0 0];    % Intracellular conductivity 
pBaseline.sigma_e_fibre  = 6.7;                             % Extracellular conductivity in the muscle fibre direction
pBaseline.sigma_e_aniso  = 0.5;                             % Anisotropy of extracellular conductivity
pBaseline.sigma_o        = 0.4 * eye(3);                    % Conductivity in fat  
pBaseline.Cm             = 1;                          % Membrane capacitance [smallest MU, largest MU]  
pBaseline.Am             = 350;                      % Fiber surface-to-volume ratio [smallest MU, largest MU]  
pBaseline.length         = 4;                               % muscle length in cm
pBaseline.width          = 1.5;                               % muscle width in cm
pBaseline.height         = 1.5;                               % muscle height in cm
pBaseline.height_fat     = 0.5;                             % height of fat tissue in cm
pBaseline.territory_centre_y     = 0.75;                   % 
pBaseline.territory_centre_z     = 0.75;                   % 
pBaseline.territory_radius = 0.2;
pBaseline.territory_load = 0.25;
pBaseline.na_defect = 0.0105;                       % Baseline chloride defect

%% Create a population of virtual subjects
NumberOfSubjects  = 20;
NumberOfParamters = 15; % to be determined
CoV               = 0.1*ones(NumberOfSubjects,NumberOfParamters);
CoV(:,11:12)        = 0.2;
% CoV(:,15)           = 0.20;
noise = 1 + randn(NumberOfSubjects,NumberOfParamters) .* CoV;
% CoV2              = 0.20; 
% noise_defect = 1 + randn(NumberOfSubjects,1) .* CoV2;

for subj_idx=1:NumberOfSubjects
    pSubject(subj_idx).sigma_i            = pBaseline.sigma_i.*noise(subj_idx,1);
    pSubject(subj_idx).sigma_e_fibre      = pBaseline.sigma_e_fibre.*noise(subj_idx,2);
    pSubject(subj_idx).sigma_e_aniso      = pBaseline.sigma_e_aniso.*noise(subj_idx,3);
    pSubject(subj_idx).sigma_o            = pBaseline.sigma_o.*noise(subj_idx,4);
    pSubject(subj_idx).length             = pBaseline.length.*noise(subj_idx,5);
    pSubject(subj_idx).width              = pBaseline.width.*noise(subj_idx,6);
    pSubject(subj_idx).height             = pBaseline.height.*noise(subj_idx,7);
    pSubject(subj_idx).height_fat         = pBaseline.height_fat.*noise(subj_idx,8);
    pSubject(subj_idx).Cm                 = pBaseline.Cm.*noise(subj_idx,9);
    pSubject(subj_idx).Am                 = pBaseline.Am.*noise(subj_idx,10);
    pSubject(subj_idx).territory_centre_y = pBaseline.territory_centre_y.*noise(subj_idx,11);
    pSubject(subj_idx).territory_centre_z = pBaseline.territory_centre_y.*noise(subj_idx,12);
    pSubject(subj_idx).territory_radius   = pBaseline.territory_radius.*noise(subj_idx,13);
    pSubject(subj_idx).territory_load     = pBaseline.territory_load.*noise(subj_idx,14);
    pSubject(subj_idx).na_defect          = pBaseline.na_defect.*noise(subj_idx,15);
end




%% Set up simulation simulation
addpath ../../simulation_routines/
addpath ../../scenario_routines/
addpath ../../linear_solver_routines/
addpath ../../cell_model_routines/
addpath ../../utilities/
addpath ../../multi_physics_routines/

dx = 1/30;
dy = 0.1;
dz = 0.1;

for subj_idx=1:NumberOfSubjects
    %% First Generate Input Parameters for Multi-Domain Model
    %% Time Settings
    % Time-Step to solve Diffusion Eq. in millisecond
    input.dt = 0.01; % [ms]
    input.dt_ode = input.dt/10;
    %  Simulation Time in milliseconds
    input.time_end = 150; % [ms]
    %% Define the finite differences grid
    % Length/Width/Height of the muscle cube in centimeters
    input.length = round(pSubject(subj_idx).length,1); % [cm]
    input.width = round(pSubject(subj_idx).width,1); % [cm]
    input.height = round(pSubject(subj_idx).height,1); % [cm]
    % Number of grid points in x/y/z-direction
    input.nx = round(30*input.length + 1,0);  
    input.ny = round(10*input.width + 1,0); 
    input.nz = round(10*input.height + 1,0);
    %% Define material parameters
    % Number of motor units
    input.num_of_mus = 1; 
    % Volume fractions of the MUs (for each MU)
    for i=1:input.num_of_mus
        gg = pSubject(subj_idx).territory_load .* ones(input.nx,input.ny,input.nz);
        input.volume_fractions{i} = gg;
    end
    % Surface to volume ratio [1/cm] (for each MU)
    input.A_m = pSubject(subj_idx).Am*ones(1,input.num_of_mus);
    %input.A_m = linspace(500,250,input.num_of_mus);
    % membrane capacity [microF/cm^2] (for each MU)
    % input.C_m = [1, 1]; 
    input.C_m = pSubject(subj_idx).Cm.*ones(1,input.num_of_mus); 
    % Fibre Rotation angle arround the z-Axes
    input.fibre_rotation_z = 0; 
    % Conductivity Tensor in the fibre [mS/cm]
    input.sigma_i = pSubject(subj_idx).sigma_i * [1 0 0; 0 0 0; 0 0 0]; 
    % Conductivity Tensor in the extra-cellular space [mS/cm]
    input.sigma_e = pSubject(subj_idx).sigma_e_fibre * ...
        [1 0 0; 0 pSubject(subj_idx).sigma_e_aniso 0; 0 0 pSubject(subj_idx).sigma_e_aniso]; 
    %% Define if an additional fat/skin layer on top of the muscle tissue should be included
    input.fat = true;
    % Height of the fat/skin tissue in centimeters
    input.height_fat = round(pSubject(subj_idx).height_fat,1); % [cm]
    % Number of grid points in z-direction (Fat/Skin Domain)
    input.nz_fat = round(10*input.height_fat + 1,0);
    %input.nz_fat = input.height_fat/dz+1; 
    % Conductivity Tensor in the fat tissue [mS/cm]
    input.sigma_o = pSubject(subj_idx).sigma_o*eye(3); 
    input.skin = false;
    % Height of the fat/skin tissue in centimeters
    input.height_skin = 0.125; % [cm]
    % Number of grid points in z-direction (Fat/Skin Domain)
    input.nz_skin = 5; 
    % Conductivity Tensor in the fat tissue [mS/cm]
    input.sigma_s = 0.2*eye(3); 
    %% Chose Membrane Model
    input.model_id = 7; % 0: Hodgkin-Huxley (1952) 1: Shorten (2007)
    %input.cell_model_specs.fatigue=1; % 0: knock out membrane fatigue 1: with membrane fatigue
    %input.cell_model_specs.parameter_set = 0;
    input.cell_model_specs.parameters = initConsts(7);
    %input.cell_model_specs.parameters(:,38) = pSubject(subj_idx).cloride_conductance; % g_Cl_bar
    input.cell_model_specs.parameters(:,56) = pSubject(subj_idx).na_defect; % f (Na+ h-Gate defect variable)
    %% Define position of the neuromuscular junction, firing times and stimulus
    % Amplitude of the applied electrical stimulus
    input.stimulus_amplitude = 2000; % Shorten
    %input.stimulus_amplitude = 700;  % Hodkin Huxley
    % Length of the applied stimulus in milliseconds
    input.stimulus_length = 0.1; % [ms] 
    % position of the neuromuscular junctions
    input.junction_points = cell(input.num_of_mus,1);
    N = input.nx*input.ny*input.nz;
    grid_points = reshape(1:N,input.nx,input.ny,input.nz);
    
    temp1 = round(pBaseline.territory_centre_y,1) / dy + 1;
    temp2 = round(pBaseline.territory_radius,1) / dy;
    yvals = (temp1-temp2):1:(temp1+temp2);

    temp1 = round(pBaseline.territory_centre_z,1) / dz + 1;
    temp2 = round(pBaseline.territory_radius,1) / dz;
    zvals = (temp1-temp2):1:(temp1+temp2);
    input.junction_points{1} = grid_points(31,yvals,zvals);
    
    input.active = ones(N*input.num_of_mus,1);
    %% firing times
    input.firing_times = cell(input.num_of_mus,1);
    input.firing_times{1} = [1 11 21 31 41 51 61 71 81];
    
    %% Non-Isometric Contraction 
    % input.dynamic{1} = 0; % 1: True 0: False 
    % input.dynamic{2} = 0; % 0: uniaixal extension 1: pure shear 
    % input.dynamic{3} = 0; % Shape 0: sin 
    % input.dynamic{4} = 500; % period in ms
    % input.dynamic{5} = [1.2 1]; % Length 1 and Length 2 
    %% Output Generation
    input.output_frequency = 10; 
    input.DefGrad = eye(3);%diag([1.2, 1.2^(-0.5), 1.2^(-0.5)]);
    input.output_spec = 1; 
    
    %% Apply external stimulus
    % input.ext_stim.times = [1]; % [ms]
    % input.ext_stim.amplitude = 40;
    % input.ext_stim.stim_length = 5;
    % input.ext_stim.points = [87811 87812 87813 87811+20 87812+20 87813+20 87811-20 87812-20 87813-20];
    input.linear_solver = 1;
    input.ode_solver = 0;
    %% Boundary Condition
    input.BC = 0;
    %input.BC = 1; % zero Neumann BC condition on bottom with respect to the extracellular potential (0: zero Dirichlet)
    %% Now Run the simulation 
    addpath ../../simulation_routines/
    addpath ../../scenario_routines/
    addpath ../../linear_solver_routines/
    addpath ../../cell_model_routines/
    addpath ../../utilities/
    addpath ../../multi_physics_routines/
    %% 
    mkdir results
    MultiDomain_implicit_3D_parallel(input)
    get_EMG('results')
    get_im_EMG([2, 0.5],'results',0,0)
    get_im_EMG([2.5, 0.75],'results',0,1)
    get_im_EMG([3, 1.0],'results',0,2)
%     calculate_magentic_field('results')
    movefile('results',sprintf('results_myotonia_na_subject_%d',subj_idx))
end
