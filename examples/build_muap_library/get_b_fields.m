addpath ../../multi_physics_routines

for muscle_idx=1:1
    %if muscle_idx == 1
    %    pathname  = 'high_mx_deep';
    %elseif muscle_idx == 2
    %    pathname  = 'high_mx_superf';
    %elseif muscle_idx == 3 
    %    pathname = 'low_mx_deep';
    %elseif muscle_idx == 4 
    %    pathname = 'low_mx_superf';
    %elseif muscle_idx == 5 
    %    pathname = 'rand';
    %end
  mkdir(sprintf('magnetic_MUAP_library/muscle_%d/',muscle_idx));
  %parfor mu_idx=1:150
  %  calculate_magentic_field(sprintf('muscle_%d/MUAP_%d/results/',muscle_idx,mu_idx));
  %end
  for mu_idx=1:150
    %load(sprintf('%s/MUAP_%d/results/simple_example_info.mat',pathname,mu_idx));
    %%load(sprintf('%s/MUAP_%d/results/simple_example_0.mat',pathname,mu_idx));
    %calculate_magentic_field(sprintf('%s/MUAP_%d/results/',pathname,mu_idx));
    Bx = [];
    By = [];
    Bz = [];
    load(sprintf('muscle_%d/MUAP_%d/results/magnetic_fields/B_fields.mat',muscle_idx,mu_idx));
    sBx = squeeze(Bx(:,:,end,:));
    sBy = squeeze(By(:,:,end,:));
    sBz = squeeze(Bz(:,:,end,:));
    save(sprintf('magnetic_MUAP_library/muscle_%d/MUAP_%d.mat',muscle_idx,mu_idx),'sBx','sBy','sBz')
    %delete(sprintf('%s/MUAP_%d/results/magnetic_fields/B_fields.mat',pathname,mu_idx));
  end
end
