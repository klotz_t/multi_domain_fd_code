%% Run Simulation
% clear all
addpath ../../simulation_routines/
addpath ../../scenario_routines/
addpath ../../linear_solver_routines/
addpath ../../cell_model_routines/
addpath ../../utilities/
%% First Generate input{MU_idx} Parameters for Multi-Domain Model
%muscle_idx = 1;
for muscle_idx=1:1
    load(sprintf('input/muscle_%d.mat',muscle_idx))
    input = cell(150,1);
    parfor MU_idx=1:150
        %% Time Settings
        % Time-Step to solve Diffusion Eq. in millisecond
        input{MU_idx}.dt = 0.1; % [ms]
        input{MU_idx}.dt_ode = input{MU_idx}.dt/10;
        %  Simulation Time in milliseconds
        input{MU_idx}.time_end = 60; % [ms]
        %% Define the finite differences grid
        % Length/Width/Height of the muscle cube in centimeters
        input{MU_idx}.length = geo.length; % [cm]
        input{MU_idx}.width = geo.width; % [cm]
        input{MU_idx}.height = geo.height; % [cm]
        % Number of grid points in x/y/z-direction
        input{MU_idx}.nx = input{MU_idx}.length*20+1;  
        input{MU_idx}.ny = geo.grid_y; 
        input{MU_idx}.nz = geo.grid_z;
        %% Define material parameters
        % Number of motor units
        input{MU_idx}.num_of_mus = 1; 
        % Volume fractions of the MUs (for each MU)
        %f_r = linspace(1,10,input{MU_idx}.num_of_mus)/sum(linspace(1,10,input{MU_idx}.num_of_mus));
        % load('input{MU_idx}/distribution_30_mus.mat')
        for i=1:input{MU_idx}.num_of_mus
            gg = zeros(input{MU_idx}.nx,input{MU_idx}.ny,input{MU_idx}.nz);
            for j=1:input{MU_idx}.nx
                gg(j,:,:) = squeeze(mu_dist.f_r(:,:,MU_idx));
            end
            input{MU_idx}.volume_fractions{i} = gg;
        end
            % Surface to volume ratio [1/cm] (for each MU)
        %input{MU_idx}.A_m = 250*ones(1,input{MU_idx}.num_of_mus);
        input{MU_idx}.A_m = linspace(para.Am(1),para.Am(2),input{MU_idx}.num_of_mus);
        % membrane capacity [microF/cm^2] (for each MU)
        % input{MU_idx}.C_m = [1, 1]; 
        input{MU_idx}.C_m = para.Cm.*ones(1,input{MU_idx}.num_of_mus); 
        % Fibre Rotation angle arround the z-Axes
        input{MU_idx}.fibre_rotation_z = 0; 
        % Conductivity Tensor in the fibre [mS/cm]
        input{MU_idx}.sigma_i = para.sigma_i; 
        % Conductivity Tensor in the extra-cellular space [mS/cm]
        input{MU_idx}.sigma_e = para.sigma_e; 
        %% Define if an additional fat/skin layer on top of the muscle tissue should be included
        input{MU_idx}.fat = true;
        % Height of the fat/skin tissue in centimeters
        input{MU_idx}.height_fat = geo.height_fat; % [cm]
        % Number of grid points in z-direction (Fat/Skin Domain)
        input{MU_idx}.nz_fat = geo.grid_z_fat; 
        % Conductivity Tensor in the fat tissue [mS/cm]
        input{MU_idx}.sigma_o = para.sigma_o; 
        input{MU_idx}.skin = false;
        % Height of the fat/skin tissue in centimeters
        input{MU_idx}.height_skin = 0.125; % [cm]
        % Number of grid points in z-direction (Fat/Skin Domain)
        input{MU_idx}.nz_skin = 5; 
        % Conductivity Tensor in the fat tissue [mS/cm]
        input{MU_idx}.sigma_s = 0.2*eye(3); 
        %% Chose Membrane Model
        input{MU_idx}.model_id = 0; % 0: Hodgkin-Huxley (1952) 1: Shorten (2007)
        input{MU_idx}.cell_model_specs.fatigue=0; % 0: knock out membrane fatigue 1: with membrane fatigue
        %% Define position of the neuromuscular junction, firing times and stimulus
        % Amplitude of the applied electrical stimulus
        %input{MU_idx}.stimulus_amplitude = 3000; % Shorten
        input{MU_idx}.stimulus_amplitude = 700;  % Hodkin Huxley
        % Length of the applied stimulus in milliseconds
        input{MU_idx}.stimulus_length = 0.1; % [ms] 
        % position of the neuromuscular junctions
        input{MU_idx}.junction_points = cell(input{MU_idx}.num_of_mus,1);
        N = input{MU_idx}.nx*input{MU_idx}.ny*input{MU_idx}.nz;
        grid_points = reshape(1:N,input{MU_idx}.nx,input{MU_idx}.ny,input{MU_idx}.nz);
        %stim_plane_points = reshape(grid_points(90,:,:),[],1);
        % rng(0,'twister');
        % temp = randi([-10 10],1,input{MU_idx}.num_of_mus)
        for i=1:input{MU_idx}.num_of_mus
            stim_plane_points = reshape(grid_points(mu_dist.x_nm_junctions(MU_idx),:,:),[],1);
            input{MU_idx}.junction_points{i} = stim_plane_points + (i-1)*N;
        end

        input{MU_idx}.active = ones(N*input{MU_idx}.num_of_mus,1);
        %% firing times
        input{MU_idx}.firing_times = cell(input{MU_idx}.num_of_mus,1);
        input{MU_idx}.firing_times{1} = [1];
        %input{MU_idx}.firing_times{1} = [0.1 21 41 61 81];%1:10:30;%[1 21 39 40 61 80 104 122 145 166 184];
        %input{MU_idx}.firing_times{2} = [5 20 35 50];%[15 45 72 101 134 162 190];
        %input{MU_idx}.firing_times{3} = [10];
        %input{MU_idx}.firing_times{4} = [15];
        %input{MU_idx}.firing_times{5} = [20];
        %input{MU_idx}.firing_times{6} = [25];
        %input{MU_idx}.firing_times{7} = [30];
        %input{MU_idx}.firing_times{8} = [35];
        %input{MU_idx}.firing_times{9} = [40];
        %input{MU_idx}.firing_times{10} = [45];

        %% Non-Isometric Contraction 
        input{MU_idx}.dynamic{1} = 0; % 1: True 0: False 
        input{MU_idx}.dynamic{2} = 0; % 0: uniaixal extension 1: pure shear 
        input{MU_idx}.dynamic{3} = 0; % Shape 0: sin 
        input{MU_idx}.dynamic{4} = 500; % period in ms
        input{MU_idx}.dynamic{5} = [1.2 1]; % Length 1 and Length 2 
        %% Output Generation
        input{MU_idx}.output_frequency = 1; 
        input{MU_idx}.DefGrad = eye(3);%diag([1.2, 1.2^(-0.5), 1.2^(-0.5)]);
        input{MU_idx}.output_spec = 1; % Only save the surface potential 

        %% Apply external stimulus
        % input{MU_idx}.ext_stim.times = [1]; % [ms]
        % input{MU_idx}.ext_stim.amplitude = 40;
        % input{MU_idx}.ext_stim.stim_length = 5;
        % input{MU_idx}.ext_stim.points = [87811 87812 87813 87811+20 87812+20 87813+20 87811-20 87812-20 87813-20];
        input{MU_idx}.linear_solver = 1;
        input{MU_idx}.ode_solver = 0;
        input{MU_idx}.filepath   = sprintf('muscle_%d/MUAP_%d/results',muscle_idx,MU_idx);
        %% Now Run the simulation 
%         addpath ../../simulation_routines/
%         addpath ../../scenario_routines/
%         addpath ../../linear_solver_routines/
%         addpath ../../cell_model_routines/
%         addpath ../../utilities/
        mkdir(input{MU_idx}.filepath)
        MultiDomain_implicit_3D_parallel(input{MU_idx})
        get_EMG(input{MU_idx}.filepath)
        %mkdir(sprintf('muscle_%d/MUAP_%d',muscle_idx,MU_idx))
        %movefile('results',sprintf('muscle_%d/MUAP_%d',muscle_idx,MU_idx))
    end
end


