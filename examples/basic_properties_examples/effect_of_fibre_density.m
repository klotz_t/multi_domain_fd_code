%% Run Simulation
rng(0)
clear all
%%
x_values = 1:0.25:4;
y_values = 0:0.25:1.5;
[X,Y] = meshgrid(x_values, y_values);
X = reshape(X',[],1);
Y = reshape(Y',[],1);

points = zeros(length(X),3);
points(:,1) = X;
points(:,2) = Y;
points(:,3) = 2.5;
%% First Generate Input Parameters for Multi-Domain Model
depth = 0.5; % [cm]
dz = 0.05;
%% Time Settings
% Time-Step to solve Diffusion Eq. in millisecond
input.dt = 0.1; % [ms]
input.dt_ode = input.dt/10;
%  Simulation Time in milliseconds
input.time_end = 30; % [ms]
%% Define the finite differences grid
% Length/Width/Height of the muscle cube in centimeters
input.length = 4.0; % [cm]
input.width = 1.5; % [cm]
input.height = 2.0; % [cm]
% Number of grid points in x/y/z-direction
input.nx = 20*input.length + 1;  
input.ny = 20*input.width + 1; 
input.nz = 20*input.height + 1;
%% Define material parameters
% Number of motor units
input.num_of_mus = 5; 
% Volume fractions of the MUs (for each MU)
for i=1:input.num_of_mus
    gg = 0.1*ones(input.nx,input.ny,input.nz);
    input.volume_fractions{i} = gg;
end
% Surface to volume ratio [1/cm] (for each MU)
%input.A_m = 500*ones(1,input.num_of_mus);
input.A_m = linspace(250,500,input.num_of_mus);
% membrane capacity [microF/cm^2] (for each MU)
% input.C_m = [1, 1]; 
input.C_m = ones(1,input.num_of_mus); 
% Fibre Rotation angle arround the z-Axes
%input.fibre_rotation_z = 0; 
% Conductivity Tensor in the fibre [mS/cm]
input.sigma_i = 8.93 * [1 0 0; 0 0 0; 0 0 0]; 
% Conductivity Tensor in the extra-cellular space [mS/cm]
input.sigma_e = 6.7 * [1 0 0; 0 0.25 0; 0 0 0.25]; 
%% Define if an additional fat/skin layer on top of the muscle tissue should be included
input.fat = true;
% Height of the fat/skin tissue in centimeters
input.height_fat = 0.2; % [cm]
% Number of grid points in z-direction (Fat/Skin Domain)
input.nz_fat = 5; 
% Conductivity Tensor in the fat tissue [mS/cm]
input.sigma_o = 0.4*eye(3); 
input.skin = false;
% Height of the fat/skin tissue in centimeters
input.height_skin = 0.125; % [cm]
% Number of grid points in z-direction (Fat/Skin Domain)
input.nz_skin = 5; 
% Conductivity Tensor in the fat tissue [mS/cm]
input.sigma_s = 0.2*eye(3); 
%% Chose Membrane Model
input.model_id = 0; % 0: Hodgkin-Huxley (1952) 1: Shorten (2007)
input.cell_model_specs.fatigue=1; % 0: knock out membrane fatigue 1: with membrane fatigue
input.cell_model_specs.parameter_set = 0;
%% Define position of the neuromuscular junction, firing times and stimulus
% Amplitude of the applied electrical stimulus
% input.stimulus_amplitude = 3000; % Shorten
input.stimulus_amplitude = 700;  % Hodkin Huxley
% Length of the applied stimulus in milliseconds
input.stimulus_length = 0.1; % [ms] 
% position of the neuromuscular junctions
input.junction_points = cell(input.num_of_mus,1);
N = input.nx*input.ny*input.nz;
grid_points = reshape(1:N,input.nx,input.ny,input.nz);
for mu_idx=1:input.num_of_mus
    nmj = zeros(1,input.ny*input.nz);
    idx = 1;
    for z_idx=1:input.nz
        for y_idx=1:input.ny
            nmj(idx) = grid_points(21+randi([-10 10]),y_idx,z_idx);
            idx = idx + 1;
        end
    end
    input.junction_points{mu_idx} = nmj + (mu_idx-1)*N;
end
    
%input.junction_points{2} = reshape(grid_points(21,:,:),[],1) + N;

input.active = ones(N*input.num_of_mus,1);
%% firing times
input.firing_times = cell(input.num_of_mus,1);
%input.firing_times{1} = [0.1];
%input.firing_times{2} = [30];


%% Non-Isometric Contraction 
% input.dynamic{1} = 0; % 1: True 0: False 
% input.dynamic{2} = 0; % 0: uniaixal extension 1: pure shear 
% input.dynamic{3} = 0; % Shape 0: sin 
% input.dynamic{4} = 500; % period in ms
% input.dynamic{5} = [1.2 1]; % Length 1 and Length 2 
%% Output Generation
input.output_frequency = 1; 
input.DefGrad = eye(3);%diag([1.2, 1.2^(-0.5), 1.2^(-0.5)]);
input.output_spec = 1; 

%% Apply external stimulus
% input.ext_stim.times = [1]; % [ms]
% input.ext_stim.amplitude = 40;
% input.ext_stim.stim_length = 5;
% input.ext_stim.points = [87811 87812 87813 87811+20 87812+20 87813+20 87811-20 87812-20 87813-20];
input.linear_solver = 1;
input.ode_solver = 0;
%% Boundary Condition
input.BC = 1; % 1: zero Neumann BC condition on bottom with respect to the extracellular potential (0: zero Dirichlet)
%% Now Run the simulation 
addpath ../../simulation_routines/
addpath ../../scenario_routines/
addpath ../../linear_solver_routines/
addpath ../../cell_model_routines/
addpath ../../utilities/
addpath ../../multi_physics_routines/
% %% 
% depth = 0.1; % [cm]
% depth_idx = input.nz - round(depth/dz);
% input.junction_points{1} = grid_points(61,16,depth_idx);
% mkdir results
% MultiDomain_implicit_3D_parallel(input)
% get_EMG('results')
% calculate_magentic_field('results')
% movefile results results_d1mm
% %%
% depth = 0.3; % [cm]
% depth_idx = input.nz - round(depth/dz);
% input.junction_points{1} = grid_points(21,16,depth_idx);
% mkdir results
% MultiDomain_implicit_3D_parallel(input)
% get_EMG('results')
% calculate_magentic_field('results')
% movefile results results_d3mm
%%
for fr=1:5 
%    gg = ones(input.nx,input.ny,input.nz);
%     input.volume_fractions{1} = gg.*(20*fr)./100;
%     input.volume_fractions{2} = gg.*(100-20*fr)./100;
    if fr == 1
        input.firing_times{1} = 0.1;
        input.firing_times{2} = 30;
        input.firing_times{3} = 30;
        input.firing_times{4} = 30;
        input.firing_times{5} = 30;
    elseif fr == 2
        input.firing_times{1} = 0.1;
        input.firing_times{2} = 0.1;
        input.firing_times{3} = 30;
        input.firing_times{4} = 30;
        input.firing_times{5} = 30;
    elseif fr == 3
        input.firing_times{1} = 0.1;
        input.firing_times{2} = 0.1;
        input.firing_times{3} = 0.1;
        input.firing_times{4} = 30;
        input.firing_times{5} = 30;
    elseif fr == 4
        input.firing_times{1} = 0.1;
        input.firing_times{2} = 0.1;
        input.firing_times{3} = 0.1;
        input.firing_times{4} = 0.1;
        input.firing_times{5} = 30;
    elseif fr == 5
        input.firing_times{1} = 0.1;
        input.firing_times{2} = 0.1;
        input.firing_times{3} = 0.1;
        input.firing_times{4} = 0.1;
        input.firing_times{5} = 0.1;
    end
    mkdir results
    MultiDomain_implicit_3D_parallel(input)
    get_EMG('results')
    %calculate_magentic_field('results')
    calculate_magentic_B_field('results',points)
    movefile('results',sprintf('results_recruitment_%d_percent',fr*20));
end

for fr=2:5 
%    gg = ones(input.nx,input.ny,input.nz);
%     input.volume_fractions{1} = gg.*(20*fr)./100;
%     input.volume_fractions{2} = gg.*(100-20*fr)./100;
    if fr == 1
        input.firing_times{1} = 0.1;
        input.firing_times{2} = 30;
        input.firing_times{3} = 30;
        input.firing_times{4} = 30;
        input.firing_times{5} = 30;
    elseif fr == 2
        input.firing_times{1} = 30;
        input.firing_times{2} = 0.1;
        input.firing_times{3} = 30;
        input.firing_times{4} = 30;
        input.firing_times{5} = 30;
    elseif fr == 3
        input.firing_times{1} = 30;
        input.firing_times{2} = 30;
        input.firing_times{3} = 0.1;
        input.firing_times{4} = 30;
        input.firing_times{5} = 30;
    elseif fr == 4
        input.firing_times{1} = 30;
        input.firing_times{2} = 30;
        input.firing_times{3} = 30;
        input.firing_times{4} = 0.1;
        input.firing_times{5} = 30;
    elseif fr == 5
        input.firing_times{1} = 30;
        input.firing_times{2} = 30;
        input.firing_times{3} = 30;
        input.firing_times{4} = 30;
        input.firing_times{5} = 0.1;
    end
    mkdir results
    MultiDomain_implicit_3D_parallel(input)
    get_EMG('results')
    %calculate_magentic_field('results')
    calculate_magentic_B_field('results',points)
    movefile('results',sprintf('results_recruitment_mu_%d',fr));
end

