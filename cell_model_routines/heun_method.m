function [y] = heun_method(y0,p,I_stim,Cm,model_id,tspan,dt)
% Applies a Heun method to solve for the membrane models specified by the
% computeRatesAndAlbebraic function and the 'model_id'.
% y0: Initial conditions
% p : Vector of the model 
% I_stim: Stimulation current
% Cm: Vector for membrane capacity
% model_id: specifies the RHS from the list of availible membrane models(see computeRatesAndAlbebraic function)
% tspan: time intervall
% dt: Time step size
y = y0;
t_ode = tspan(1);
while (t_ode < tspan(2))
    k1 = computeRatesAndAlgebraic(t_ode,y,p,I_stim,Cm,model_id)';
    k2 = computeRatesAndAlgebraic(t_ode+dt,y + dt.*k1,p,I_stim,Cm,model_id)'; 
    y = y + dt/2.*(k1 + k2);
    t_ode = t_ode + dt;
end

end

