%% Test Cell Model Routines
% Tests the response of the membrane model specified by 'model_id' with an 
% current pulse with amplitude 'I_stim' and a duration of 'dt_stim'.

model_id  = 5; % 0: Hodgkin-Huxley, 1: Shorten, 2: Shorten (fatige knockout), ...
               % 3: Shorten (ECC), 4: Adrian-Chandler-Hodgkin, 5: CRRS  
solver_id = 1; % 0: Improved Euler Method, 1: ode15s solver

y0      = initStates(model_id); % get initial condition
p       = initConsts(model_id); % get the model paramteres
Cm      = 2.5;                    % set membrane capacitance [microS/cm^2]
I_stim  = 1750;                 % set amplitude for an injected current pulse [microA]
dt_stim = 0.1;                  % set duration of current pulse
t_end   = 1;                   % simulation time


if solver_id == 1
    options = odeset('RelTol', 1e-06, 'AbsTol', 1e-06, 'MaxStep', 0.1);
else
    dt_ode = 0.01;
end

if solver_id == 1
    [t1, y1] = ode15s(@(t,y)computeRatesAndAlgebraic(...
                        t, y, p,I_stim,Cm,model_id), [0 dt_stim], y0, options);
    [t2, y2] = ode15s(@(t,y)computeRatesAndAlgebraic(...
                        t, y, p,0,Cm,model_id), [dt_stim t_end], y1(end,:), options);
    t = vertcat(t1,t2);
    y = vertcat(y1,y2);
else
    y(1,:) = y0;
    idx = 1;
    t_ode = 0;
    t(1) = 0;
    while (t_ode < t_end)
        if t_ode<dt_stim
            I = I_stim;
        else
            I = 0;
        end
        k1 = computeRatesAndAlgebraic(t_ode, y(idx,:),p,I,Cm,model_id)';
        k2 = computeRatesAndAlgebraic(t_ode+dt_ode, y(idx,:) + dt_ode.*k1,p,I,Cm,model_id)'; 
        y(idx+1,:) = y(idx,:) + dt_ode/2.*(k1 + k2);
        t_ode = t_ode + dt_ode;
        t(idx+1) = t_ode;
        idx = idx + 1;
    end
end
figure,plot(t,y(:,1))