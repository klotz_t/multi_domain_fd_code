# README #

This Matlab code was developed to test and apply the multi-domain model 
which was originally proposed in [1] and which can be used to simulate the electro-physiological behaviour of skeletal muscles. 
The code includes a finite difference and a finite element implementation of the multi-domain model of skeletal muscle electrophyiology. 
With the solution of the electric field problem at hand, the magnetic field induced by the activity of the muscle can be computet. 
The corresponding methods are published in [2,3]. 

Developed by:

Thomas Klotz, University of Stuttgart, Institute for Modeling and Simulation of Biomechanical Systems.

Contributors:

Jessica Walz, University of Stuttgart, Institute of Mechanics, Structural Analysis and Dynamics.   

# References  

[1] Klotz T., Gizzi, L., Yavuz, U. and Röhrle, O. (2020).
    Modelling the electrical activity of skeletal muscle tissue using a 
    multi‑domain approach. Biomechanics and Modeling in Mechanobiology. 
    
[2] Klotz, T., Gizzi, L., Röhrle, O. (2022). Investigating 
    the spatial resolution of EMG and MMG based on a systemic  multi-scale 
    model. Biomechanics and Modeling in Mechanobiology.
    
[3] Klotz, T., Lehmann, L., Negro, F. and Röhrle, O. (2023). 
    High-density magnetomyography is superior to high-density surface electromyography 
    for motor unit decomposition: a simulation study.    